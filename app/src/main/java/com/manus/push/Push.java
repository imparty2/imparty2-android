/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manus.push;

import com.manus.push.http.HTTP;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.NotYetConnectedException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Manus
 */
public class Push implements Client.DataHandler {

    public interface OnPush {

        public void onPush(String data);

        public void onLog(String data);
    }

    private String host;
    private WebSocket client;
    private String regid;
    private int sleepOnDisconnection = 5000;
    private OnPush push;
    private boolean running = false;

    public Push(String host) {
        this.host = host;

    }

    public void setSleepOnDisconnection(int sleepOnDisconnection) {
        this.sleepOnDisconnection = sleepOnDisconnection;
    }

    public void setPush(OnPush push) {
        this.push = push;
    }

    public String register() {
        String res = HTTP.http_get("http://" + host + "/register");
        if(res != null)
        {
        	
	        JSONObject obj = new JSONObject(res);
	        int port = obj.getInt("port");
	        String reg = obj.getString("regid");
	        return port + ":" + reg;
        }
        return null;
    }

    public boolean connect(String reg) {
        this.regid = reg;
        running = true;

        return indeterminate_connection();
    }

    private boolean indeterminate_connection() {
        final String sport = regid.substring(0, regid.indexOf(":"));
        final int port = Integer.parseInt(sport);
        if (client != null) {

            if (push != null) {
                push.onLog("Clossing existent connection");
            }

            client.close();
        }
       
        URI uri = URI.create("ws://" + host + ":" + port);
        client = new WebSocket(uri);
        
        

        if (push != null) {
            push.onLog("Creating new connection");
        }

        client.setHandler(this);
        try {
            new Thread(client).start();
            return true;
        } catch (NotYetConnectedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void close() {
        running = false;
        client.close();
    }

    @Override
    public void onReceiveString(String data) {
        if (push != null) {
            push.onLog("new data: " + data);
            push.onPush(data);
        }
    }

    @Override
    public void onDisconnection() {
        if (push != null) {
            push.onLog("Push disconnected");
        }
        if (running) {

            try {
                Thread.sleep(sleepOnDisconnection);
            } catch (InterruptedException ex) {
                Logger.getLogger(Push.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
            while (!indeterminate_connection()) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Push.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }

            }
        }
    }

    @Override
    public void onConnect() {
        // TODO Auto-generated method stub
        if (push != null) {
            push.onLog("Push connected");
        }
        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                JSONObject obj = new JSONObject();
                obj.put("regid", regid);
                client.send(obj.toString());

                if (push != null) {
                    push.onLog("Connection running reg: " + regid);
                }

            }
        }).start();
    }

    public boolean isRunning() {
        // TODO Auto-generated method stub
        return running;
    }
}
