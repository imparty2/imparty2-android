package com.manus.push;

import java.net.URI;

import com.manus.push.Push.OnPush;

public class Test2 {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Push p = new Push("jpush.imparty.org");
        p.setPush(new OnPush() {

            @Override
            public void onPush(String data) {
                // TODO Auto-generated method stub
                System.out.println("Data: " + data);
            }

            @Override
            public void onLog(String data) {
                System.out.println("Log: " + data);

            }
        });
        String reg = p.register();
        System.out.println("Reg: " + reg);
        p.connect(reg);

    }

}
