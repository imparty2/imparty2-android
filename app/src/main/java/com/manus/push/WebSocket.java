package com.manus.push;

import java.net.URI;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import com.manus.push.Client.DataHandler;

public class WebSocket extends WebSocketClient {

    DataHandler handler;

    public void setHandler(DataHandler handler) {
        this.handler = handler;
    }
    
	public WebSocket(URI serverURI) {
		super(serverURI);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onOpen(ServerHandshake handshakedata) {
		// TODO Auto-generated method stub
		System.out.println("Open!");
		if(handler != null)
		{
			handler.onConnect();
		}
	}

	@Override
	public void onMessage(String message) {
		// TODO Auto-generated method stub
		if(handler != null)
		{
			handler.onReceiveString(message);
		}
	}

	@Override
	public void onClose(int code, String reason, boolean remote) {
		// TODO Auto-generated method stub
		if(handler != null)
		{
			handler.onDisconnection();
		}
	}

	@Override
	public void onError(Exception ex) {
		// TODO Auto-generated method stub
		ex.printStackTrace();
	}

	public void ping() {
		// TODO Auto-generated method stub
		this.send("ping");
	}

}
