/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manus.push;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Manus
 */
public class Client {
    
    public static final int MAX_PACKET_LENG = 1024 * 10;
    Socket socket;

    public interface DataHandler
    {
        public void onReceiveString(String data);
        public void onConnect();
        public void onDisconnection();
    }
    DataHandler handler;

    public void setHandler(DataHandler handler) {
        this.handler = handler;
    }
    
    public boolean send(String json)
    {
        try {
            socket.getOutputStream().write(json.getBytes());
            return true;
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean connect(String ip, int port)
    {
        SocketAddress addr = new InetSocketAddress(ip, port);
        try {
            socket = new Socket();
            socket.connect(addr);
            recv();
            if(handler != null) handler.onConnect();
            return true;
        } catch (IOException ex) {
            //Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
        
    }
    
    private void recv()
    {
        new Thread(new Runnable() {

            @Override
            public void run() {
                InputStream in = null;
                try {
                    int leng = 0;
                    byte[] buffer = new byte[MAX_PACKET_LENG];
                    in = socket.getInputStream();
                    while((leng = in.read(buffer)) > 0)
                    {
                        if(handler != null)
                        {
                            handler.onReceiveString(new String(buffer, 0, leng));
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    try {
                        in.close();
                    } catch (IOException ex) {
                        Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                try {
                    socket.close();
                } catch (IOException ex) {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(handler != null)
                {
                    handler.onDisconnection();
                }
                
                
            }
        }).start();
    }
    
    void close() {
        try {
            socket.close();
            if(handler != null) handler.onDisconnection();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
