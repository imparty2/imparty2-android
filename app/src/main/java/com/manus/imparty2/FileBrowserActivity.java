package com.manus.imparty2;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.manus.imparty2.adapter.FilesAdapter;

public class FileBrowserActivity extends Activity implements OnItemClickListener {

	ListView listView;
	FilesAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_browser);
		listView = (ListView) findViewById(R.id.listView1);
		listView.setOnItemClickListener(this);
		load(Environment.getExternalStorageDirectory());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.file_browser, menu);
		return true;
	}

	public void load(File path) {
		File[] files = path.listFiles();

		List<File> output = new ArrayList<File>();

		for (File file : files) {
			output.add(file);
		}
		Collections.sort(output, new SortFileName());
		Collections.sort(output, new SortFolder());
		if(path.getParent() != null)
		{
			output.add(0, path.getParentFile());
		}
		adapter = new FilesAdapter(this, R.layout.item_file, output, path.getParentFile());
		listView.setAdapter(adapter);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		File f = adapter.getItem(arg2);
		if (f.isDirectory()) {
			load(f);
		} else {
			Intent result = new Intent();
			result.putExtra("file", f.getAbsolutePath());
			setResult(RESULT_OK, result);
			finish();
		}

	}

	// sorts based on the files name
	public class SortFileName implements Comparator<File> {
		@Override
		public int compare(File f1, File f2) {
			return f1.getName().compareTo(f2.getName());
		}
	}

	// sorts based on a file or folder. folders will be listed first
	public class SortFolder implements Comparator<File> {
		@Override
		public int compare(File f1, File f2) {
			if ((f1.isDirectory() && f2.isDirectory()) || (!f1.isDirectory() && !f2.isDirectory()))
				return 0;
			else if (f1.isDirectory() && !f2.isDirectory())
				return -1;
			else
				return 1;
		}
	}

}
