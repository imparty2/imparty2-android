package com.manus.imparty2;

import com.manus.imparty2.TextEditorDialog.OnTextListenner;
import com.manus.imparty2.client.Login;
import com.manus.imparty2.core.ImageLoader;
import com.manus.imparty2.service.MessagingService;
import com.manus.imparty2.service.MessagingService.MessageReceiver;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileActivity extends Activity implements MessageReceiver, OnClickListener {

	ImageLoader loader = new ImageLoader();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		MessagingService.attachListenner(this);

		((ImageButton) findViewById(R.id.imageButton1)).setOnClickListener(this);
		((ImageButton) findViewById(R.id.imageButton2)).setOnClickListener(this);
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.profile, menu);
		return true;
	}

	@Override
	public boolean onMessage(Bundle data) {
		// TODO Auto-generated method stub
		if(data.getInt("key") == MessagingService.BUNDLE_KEY_SERVICE_BOOT)
		{
			if(MessagingService.getInstance().isLogin())
			{
				displayInfo();
			}
		}
		else if(data.getInt("key") == MessagingService.BUNDLE_KEY_LOGIN)
		{
			displayInfo();
		}
		return false;
	}
	
	public void displayInfo()
	{
		Login info = MessagingService.getInstance().loginInfo;
		((TextView) findViewById(R.id.textView1)).setText(info.public_name);
		((TextView) findViewById(R.id.textView2)).setText(info.status);
		loader.setImage(info.avatar, (ImageView) findViewById(R.id.imageView1));
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.imageButton1)
		{
			TextEditorDialog dialog = new TextEditorDialog();
			dialog.show(this, new OnTextListenner() {
				
				@Override
				public void onText(String text) {
					// TODO Auto-generated method stub
					MessagingService.getInstance().editProfileName(text);
				}
			});
		}
		else if(v.getId() == R.id.imageButton2)
		{

			TextEditorDialog dialog = new TextEditorDialog();
			dialog.show(this, new OnTextListenner() {
				
				@Override
				public void onText(String text) {
					// TODO Auto-generated method stub
					MessagingService.getInstance().editProfileStatus(text);
				}
			});
		}
	}

}
