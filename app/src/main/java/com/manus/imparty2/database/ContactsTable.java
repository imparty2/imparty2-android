package com.manus.imparty2.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.manus.imparty2.Application;
import com.manus.imparty2.client.Account;
import com.manus.imparty2.core.Contact;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.ContactsContract.PhoneLookup;

public class ContactsTable extends DatabaseTable {

	private static final String TABLE_NAME = "contacts";
	private static final HashMap<String, String> colums = new HashMap<String, String>();
	static {
		colums.put("_id", "TEXT PRIMARY KEY");
		colums.put("username", "TEXT");
		colums.put("uid", "TEXT");
		colums.put("name", "TEXT");
		colums.put("avatar", "TEXT");
		colums.put("provider", "TEXT");
		colums.put("account", "TEXT");
		colums.put("presence", "TEXT");
		colums.put("status", "TEXT");
		colums.put("lastseen", "INTEGER");
		colums.put("joined", "INTEGER");
	}

	public ContactsTable() {
		// TODO Auto-generated constructor stub
	}

	@Override
	String getTableName() {
		return TABLE_NAME;
	}

	@Override
	void create(SQLiteDatabase db) {

		build_table(db, colums);
	}

	@Override
	void migrate(SQLiteDatabase db, int toVersion) {
		DatabaseManager.dropTable(db, TABLE_NAME);
		create(db);

	}

	@Override
	void clear() {
		// TODO Auto-generated method stub

	}

	public Contact query(String uid) {
		String sql = "SELECT * FROM " + TABLE_NAME + " WHERE uid='" + uid + "'";
		Cursor c = getDatabaseManager().db().rawQuery(sql, null);

		if (c.moveToNext()) {
			Contact _contact = build(c);
			c.close();
			return _contact;
		}
		c.close();
		return null;
	}

	public void sync(List<Contact> contacts, String account) {
		if (contacts == null)
			return;
		for (Contact contact : contacts) {
			ContentValues values = new ContentValues();
			values.put("uid", contact.uid);
			values.put("username", contact.username);
			values.put("avatar", contact.avatar);
			values.put("name", contact.display_name);
			values.put("provider", contact.provider);
			values.put("joined", contact.joined);
			values.put("account", account);
			if (contact.uid.equals("-1")) {
				getDatabaseManager().db().insert(TABLE_NAME, null, values);
			} else {

				if (getDatabaseManager().db().update(TABLE_NAME, values, "uid='" + contact.uid + "'", null) == 0) {
					getDatabaseManager().db().insert(TABLE_NAME, null, values);
				}
			}
		}
	}

	public void insert(String uid, String username, String name, String avatar) {
		ContentValues values = new ContentValues();
		values.put("uid", uid);
		values.put("username", username);
		values.put("avatar", avatar);
		values.put("name", name);
		values.put("provider", "");
		values.put("joined", 1);
		values.put("account", "");
		getDatabaseManager().db().insert(TABLE_NAME, null, values);

	}
	public void drop_contacts(String account, String provider) {
		getDatabaseManager().db().delete(TABLE_NAME, "account='" + account + "' AND provider = '" + provider + "'", null);
	}
	public void drop_contacts(String account) {
		getDatabaseManager().db().delete(TABLE_NAME, "account = '" + account + "'", null);
	}

	public List<Contact> list(List<Account> accounts, boolean joined) {

		List<Contact> ret = new ArrayList<Contact>();
		for (Account account : accounts) {
			String sql = "SELECT * FROM " + TABLE_NAME + " WHERE account = '" + account.username + "' " + (joined ? " AND joined = 1" : "") + " ORDER BY abs(1-joined) ASC";
			Cursor c = getDatabaseManager().db().rawQuery(sql, null);
			while (c.moveToNext()) {
				ret.add(build(c));
			}
			c.close();
		}

		return ret;
	}

	private Contact build(Cursor c) {
		Contact _contact = new Contact();
		_contact.uid = c.getString(c.getColumnIndex("uid"));
		_contact.username = c.getString(c.getColumnIndex("username"));
		_contact.avatar = c.getString(c.getColumnIndex("avatar"));
		_contact.display_name = c.getString(c.getColumnIndex("name"));
		_contact.provider = c.getString(c.getColumnIndex("provider"));
		if (_contact.provider != null && _contact.provider.equals("phonebook")) {
			String tmp = queryInternalContact(c.getString(c.getColumnIndex("username")));
			if (tmp.equals("")) {
				_contact.display_name = c.getString(c.getColumnIndex("name"));

			} else {

				_contact.display_name = tmp;
			}
		}
		_contact.joined = c.getInt(c.getColumnIndex("joined"));
		_contact.account = c.getString(c.getColumnIndex("account"));
		_contact.lastseen = c.getLong(c.getColumnIndex("lastseen"));
		_contact.presence = c.getString(c.getColumnIndex("presence"));
		_contact.status = c.getString(c.getColumnIndex("status"));
		return _contact;
	}

	public void update_info(String username, String name, String avatar, String presence, long lastseen, String status) {
		// TODO Auto-generated method stub

		ContentValues values = new ContentValues();
		values.put("avatar", avatar);
		values.put("name", name);
		values.put("presence", presence);
		values.put("joined", 1);
		values.put("lastseen", lastseen);
		values.put("status", status);
		if (query(username) != null) {

			getDatabaseManager().db().update(TABLE_NAME, values, "uid='" + username + "'", null);

		} else {
			values.put("uid", username);
			values.put("username", username);
			getDatabaseManager().db().insert(TABLE_NAME, null, values);
		}
	}

	public String queryInternalContact(String phone) {
		Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone));
		ContentResolver resolver = Application.getInstance().getContentResolver();
		Cursor c = resolver.query(uri, new String[]{PhoneLookup.DISPLAY_NAME}, "", null, "");
		String name = "";
		if (c.moveToNext()) {
			name = c.getString(0);
		}
		c.close();
		return name;

	}

}
