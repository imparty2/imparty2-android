/**
 * Copyright (c) 2013, Redsolution LTD. All rights reserved.
 * 
 * This file is part of Xabber project; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License, Version 3.
 * 
 * Xabber is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License,
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */
package com.manus.imparty2.database;

import java.util.HashMap;

import android.database.sqlite.SQLiteDatabase;

/**
 * Interface for registered database tables.
 * 
 * @author alexander.ivanov
 * 
 */
public abstract class DatabaseTable {

	public DatabaseTable() {
		DatabaseManager.getInstance().addTable(this);
	}
	
	protected DatabaseManager getDatabaseManager() {
		return DatabaseManager.getInstance();
	}
	
	protected void build_table(SQLiteDatabase db, HashMap<String, String> colums) {
		String sql = "";
		for (String key : colums.keySet()) {
			String value = colums.get(key);
			sql += (sql.equals("")?"":" , ");
			sql += key + " " + value;
			
		}
		sql = "CREATE TABLE " + getTableName() + " (" + sql + ");";
		DatabaseManager.execSQL(db, sql);
	}
	abstract String getTableName();
	/**
	 * Called on create database.
	 * 
	 * @param db
	 */
	abstract void create(SQLiteDatabase db);

	/**
	 * Called on database migration.
	 * 
	 * @param db
	 * @param toVersion
	 */
	abstract void migrate(SQLiteDatabase db, int toVersion);

	/**
	 * Called on clear database request.
	 */
	abstract void clear();

}
