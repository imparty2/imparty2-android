package com.manus.imparty2.database;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.manus.imparty2.client.Account;
import com.manus.imparty2.core.Contact;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class AccountTable extends DatabaseTable {

	public static final String TABLE_NAME = "accounts";
	public static final LinkedHashMap<String, String> columns = new LinkedHashMap<String, String>();
	static
	{
		columns.put("_id", "TEXT PRIMARY KEY");
		columns.put("username", "TEXT UNIQUE");
		columns.put("provider", "TEXT");
		columns.put("name", "TEXT");
		columns.put("avatar", "TEXT");
		columns.put("sync", "INTEGER");
	}
	@Override
	String getTableName() {
		// TODO Auto-generated method stub
		return TABLE_NAME;
	}

	@Override
	void create(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		build_table(db, columns);
	}

	@Override
	void migrate(SQLiteDatabase db, int toVersion) {
		// TODO Auto-generated method stub
		DatabaseManager.dropTable(db, TABLE_NAME);
		build_table(db, columns);
	}

	@Override
	void clear() {
		// TODO Auto-generated method stub
		
	}

	public List<Account> list_accounts()
	{
		List<Account> ret = new ArrayList<Account>();
		String sql = "SELECT * FROM " + TABLE_NAME;
		Cursor c = getDatabaseManager().db().rawQuery(sql, null);
		while(c.moveToNext())
		{
			ret.add(build(c));
		}
		c.close();
		return ret;
	}
	public List<Account> list_sync_accounts()
	{
		List<Account> ret = new ArrayList<Account>();
		String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync = 1";
		Cursor c = getDatabaseManager().db().rawQuery(sql, null);
		while(c.moveToNext())
		{
			ret.add(build(c));
		}
		c.close();
		return ret;
	}
	
	private Account build(Cursor c)
	{
		Account _account = new Account();
		_account.username = c.getString(c.getColumnIndex("username"));;
		_account.avatar = c.getString(c.getColumnIndex("avatar"));
		_account.name = c.getString(c.getColumnIndex("name"));
		_account.provider = c.getString(c.getColumnIndex("provider"));
		_account.sync = c.getInt(c.getColumnIndex("sync"))==1;
		return _account;
	}
	
	public void save_account(Account account)
	{

		ContentValues values = new ContentValues();
		values.put("username", account.username);
		values.put("avatar", account.avatar);
		values.put("name", account.name);
		values.put("provider", account.provider);
		values.put("sync", 1);
		getDatabaseManager().db().insert(TABLE_NAME, null, values);
	}
	
	public void sync_account(Account account, boolean sync)
	{
		ContentValues values = new ContentValues();
		values.put("sync", sync?1:0);
		getDatabaseManager().db().update(TABLE_NAME, values, "username = '" + account.username + "' AND provider='" + account.provider + "'", null);
		
	}

	public void save_account(List<Account> accounts) {
		// TODO Auto-generated method stub
		
		List<Account> acc = list_accounts();
		for (Account account : acc) {
			boolean find = false;
			for (Account account2 : accounts) {
				if(account2.username.equals(account.username))
				{
					find = true;
					break;
				}
			}
			if(!find)
			{
				getDatabaseManager().db().delete(TABLE_NAME, "username = '" + account.username + "' AND provider='" + account.provider + "'", null);
			}
		}
		for (Account account : accounts) {
			if(!exitst(account))
				save_account(account);
		}
	}
	public boolean exitst(Account account)
	{
		boolean res = false;
		Cursor c = getDatabaseManager().db().rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE username = '" + account.username + "' AND provider='" + account.provider + "'", null);
		if(c.moveToNext())
		{
			res = true;
		}
		c.close();
		return res;
	}

	public boolean is_sync(Account account) {
		// TODO Auto-generated method stub
		boolean res = false;
		Cursor c = getDatabaseManager().db().rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE username = '" + account.username + "' AND provider='" + account.provider + "'", null);
		if(c.moveToNext())
		{
			res = c.getInt(c.getColumnIndex("sync"))==1?true:false;
		}
		c.close();
		return res;
	}

}
