package com.manus.imparty2.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import com.manus.imparty2.core.Chat;
import com.manus.imparty2.core.Contact;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ChatTable extends DatabaseTable {

	public static final String TABLE_NAME = "chat";
	public static final LinkedHashMap<String, String> columns = new LinkedHashMap<String, String>();
	static
	{
		columns.put("_id", "TEXT PRIMARY KEY");
		columns.put("contact", "TEXT");
		columns.put("content", "TEXT");
		columns.put("display", "TEXT");
		columns.put("hashcode", "TEXT");
		columns.put("uid", "TEXT");
		columns.put("time", "INTEGER");
		columns.put("status", "INTEGER");
	}
	
	@Override
	String getTableName() {
		// TODO Auto-generated method stub
		return TABLE_NAME;
	}

	@Override
	void create(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		build_table(db, columns);
	}

	@Override
	void migrate(SQLiteDatabase db, int toVersion) {
		// TODO Auto-generated method stub
		DatabaseManager.dropTable(db, TABLE_NAME);
		build_table(db, columns);
		
	}

	@Override
	void clear() {
		// TODO Auto-generated method stub
		
	}
	public List<Chat> list_recents()
	{
		List<Chat> ret = new ArrayList<Chat>();
		String sql = "SELECT * FROM " + ChatTable.TABLE_NAME + " GROUP BY contact ORDER BY time DESC";
		Cursor c = getDatabaseManager().db().rawQuery(sql, null);
		while(c.moveToNext())
		{
			ret.add(build(c));
		}

		c.close();
		return ret;
	}
	
	public int count_unread_messages(String contact)
	{
		String sql = "SELECT count(*) FROM " + ChatTable.TABLE_NAME + " WHERE contact = '" + contact + "' AND status = " + Chat.STATUS_RECV_FROM_CONTACT;
		Cursor c = getDatabaseManager().db().rawQuery(sql, null);
		int count = 0;
		if(c.moveToNext())
		{
			count = c.getInt(0);
		}

		c.close();
		return count;
	}

	public Chat insert(String contact, String content, long time, String _id, int status, String display, String uid)
	{

		ContentValues values = new ContentValues();
		values.put("_id", _id);
		values.put("contact", contact);
		values.put("content", content);
		values.put("time", time);
		values.put("status", status);
		values.put("display", display);
		values.put("uid", uid);
		if(getDatabaseManager().db().insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE) == 0)
			return null;

		
		Chat chat = new Chat();
		chat._id = _id;
		chat.contact = contact;
		chat.message = content;
		chat.time = time;
		chat.status = status;
		chat.uid = uid;
		return chat;
	}
	public Chat insert(String contact, String content, long time, String _id, int status)
	{
		return insert( contact,  content,  time,  _id,  status, "", "");
	}
	public Chat insert_incoming(Chat chat)
	{
		return insert( chat.contact,  chat.message,  chat.time,  chat._id,  Chat.STATUS_RECV_FROM_CONTACT, "", "");
	}


	public List<Chat> query(String uid)
	{
		List<Chat> ret = new ArrayList<Chat>();
		String sql = "SELECT * FROM " + TABLE_NAME + " WHERE contact='" + uid + "' ORDER BY time";
		Cursor c = getDatabaseManager().db().rawQuery(sql, null);
		while(c.moveToNext())
		{
			ret.add(build(c));
		}
		c.close();
		return ret;
	}

	private Chat build(Cursor c) {
		// TODO Auto-generated method stub
		Chat _chat = new Chat();
		_chat._id = c.getString(c.getColumnIndex("_id"));
		_chat.contact = c.getString(c.getColumnIndex("contact"));
		_chat.message = c.getString(c.getColumnIndex("content"));
		_chat.time = c.getLong(c.getColumnIndex("time"));
		_chat.status = c.getInt(c.getColumnIndex("status"));
		_chat.uid = c.getString(c.getColumnIndex("uid"));
		_chat.display = c.getString(c.getColumnIndex("display"));
		return _chat;
	}

	public Chat select(String id) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM " + TABLE_NAME + " WHERE _id='" + id + "'";
		Cursor c = getDatabaseManager().db().rawQuery(sql, null);
		if(c.moveToNext())
		{
			Chat _c = build(c);
			c.close();
			return _c;
		}
		c.close();
		return null;
	}
	
	public List<Chat> select_messages(String where)
	{
		List<Chat> list = new ArrayList<Chat>();
		String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + where;
		Cursor c = getDatabaseManager().db().rawQuery(sql, null);
		while(c.moveToNext())
		{
			list.add( build(c) );
		}
		c.close();
		return list;
	}

	public void update_status(String id, int status) {
		// TODO Auto-generated method stub
		ContentValues values = new ContentValues();
		values.put("status", status);
		getDatabaseManager().db().update(TABLE_NAME, values, "_id='" + id + "'", null);
	}

	public boolean mark_read(String username) {
		ContentValues values = new ContentValues();
		values.put("status", Chat.STATUS_READ);
		return getDatabaseManager().db().update(TABLE_NAME, values, "status = '" + Chat.STATUS_RECV_FROM_CONTACT + "' AND contact = '" + username + "'", null) >= 1;
	}
	

	public int getUnread()
	{
		Cursor c = getDatabaseManager().db().rawQuery("SELECT count(*) FROM " + TABLE_NAME + " WHERE status='" + Chat.STATUS_RECV_FROM_CONTACT + "'", null);
		if(c.moveToNext())
		{
			int i = c.getInt(0);
			c.close();
			return i;
		}
		c.close();
		return 0;
	}

	public int getUnreadUsers()
	{
		Cursor c = getDatabaseManager().db().rawQuery("SELECT DISTINCT contact FROM " + TABLE_NAME + " WHERE status='" + Chat.STATUS_RECV_FROM_CONTACT + "'", null);
		if(c.moveToNext())
		{
			int i = c.getCount();
			c.close();
			return i;
		}
		c.close();
		return 0;
	}
	public List<Chat> getUnreadChats()
	{
		List<Chat> chats = new ArrayList<Chat>();
		Cursor c = getDatabaseManager().db().rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE status='" + Chat.STATUS_RECV_FROM_CONTACT + "'", null);
		while(c.moveToNext())
		{
			chats.add( build(c) );
		}
		c.close();
		return chats;
	}

	public void delete(String uid) {
		getDatabaseManager().db().delete(TABLE_NAME, "contact = '" + uid + "'", null);
	}

	public void delete_id(String _id) {
		// TODO Auto-generated method stub
		getDatabaseManager().db().delete(TABLE_NAME, "_id = '" + _id + "'", null);
		
	}

}
