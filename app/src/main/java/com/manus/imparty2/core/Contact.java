package com.manus.imparty2.core;

import org.json.JSONException;
import org.json.JSONObject;

public class Contact {

	public String display_name;
	public String avatar;
	public String uid;
	public String username, provider, account;
	public String status;
	public int joined = 1;
	public long lastseen;
	public String presence;
	public String other = "";
	
	public void parse(JSONObject json) throws JSONException {

		this.display_name = json.getString("name");
		this.avatar = json.getString("avatar");
		this.provider = json.getString("provider");
		this.status = json.getString("status");
		this.username = json.getString("username");
		this.uid = json.optString("uid", "-1");
		this.joined = json.optInt("joined_service", 0);
		
	}
	
}
