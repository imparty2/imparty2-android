package com.manus.imparty2.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.HashMap;

import com.manus.imparty2.R;
import com.manus.imparty2.Utils;
import com.manus.imparty2.client.HttpUtils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.widget.ImageView;

public class ImageLoader {
	
	public static class ImageCache
	{
		private static HashMap<String, Bitmap> cache = new HashMap<String, Bitmap>();
		public static File CACHE;
		static {
			if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
			{
				CACHE = new File(Environment.getExternalStorageDirectory(), "Android/data/imparty/cache");
			}
			else
			{
				CACHE = new File(Environment.getExternalStorageDirectory(), "Android/data/imparty/cache");
			}
			new File(Environment.getExternalStorageDirectory(), "Android").mkdirs();
			new File(Environment.getExternalStorageDirectory(), "Android/data").mkdirs();
			new File(Environment.getExternalStorageDirectory(), "Android/data/imparty").mkdirs();
			new File(Environment.getExternalStorageDirectory(), "Android/data/imparty/cache").mkdirs();
		};
		
		public static File getFile(String str)
		{
			String md5 = Utils.md5(str);
			File dir = new File(CACHE, md5.substring(0, 2));
			dir.mkdirs();
			return new File(dir, md5);
		}
		public static void save(Bitmap bmp, String file)
		{
			FileOutputStream out;
			try {
				out = new FileOutputStream(getFile(file));
				bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
				out.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public static Bitmap load(String file)
		{
			return decodeFile(getFile(file));
		}
		public static void put(String key, Bitmap data)
		{
			cache.put(key, data);
		}
		public static Bitmap get(String key)
		{
			return cache.get(key);
		}
	}
	
	
	private Handler handler = new Handler();

	public void setImage(final String url, final ImageView image)
	{
		if((ImageCache.get(url)) != null)
		{

			image.setImageBitmap(ImageCache.get(url));
		}
		else
		{
			FakeImage fake = new FakeImage();
			image.setImageDrawable(fake);
			image.setImageResource(R.drawable.ic_default_loading);
			fake.execute(url, image);
			
		}
	}
	public void setImageAB(final String url, final ActionBar image)
	{
		if((ImageCache.get(url)) != null)
		{

			image.setIcon(new BitmapDrawable(ImageCache.get(url)));
		}
		else
		{
			//image.setImageResource(R.drawable.ic_default_loading);
			new AsynImageLoaderActionBar(image).execute(url);
			
		}
	}


	private static Bitmap decodeFile(File f) {
		try {
			// decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Bitmap getImage(String url)
	{
		
		File internal_file = ImageCache.getFile(url);
		Bitmap res;
		if((res = ImageCache.get(url)) == null)
		{
			
			if(internal_file.exists())
			{
				res = decodeFile(internal_file);
			}
			else
			{
				if(HttpUtils.http_download(url.replace("https://", "http://"), internal_file.getAbsolutePath()))
				{
					
		            res = decodeFile(internal_file);
				}
			}
			ImageCache.put(url, res);
		}
		return res;
	}
	
	class FakeImage extends ColorDrawable
	{
		AsynImageLoader loader;
		public void execute(String url, ImageView view)
		{
			loader = new AsynImageLoader(view);
			loader.execute(url);
		}
	}

	public class AsynImageLoader extends AsyncTask<String, Void, Bitmap>
	{
		WeakReference<ImageView> view; 
		public AsynImageLoader(ImageView view) {
			// TODO Auto-generated constructor stub
			this.view = new WeakReference<ImageView>(view);
		}
		@Override
		protected Bitmap doInBackground(String... params) {
			// TODO Auto-generated method stub
			return getImage(params[0]);
		}
		
		@Override
		protected void onPostExecute(Bitmap result) {
			// TODO Auto-generated method stub
			if(result != null && view.get() != null)
				view.get().setImageBitmap(result);
		}
		
	}
	public class AsynImageLoaderActionBar extends AsyncTask<String, Void, Drawable>
	{

		ActionBar view;
		public AsynImageLoaderActionBar(ActionBar view) {
			// TODO Auto-generated constructor stub
			this.view = view;
		}
		@Override
		protected Drawable doInBackground(String... params) {
			// TODO Auto-generated method stub
			Bitmap bmp = getImage(params[0]);
			if(bmp != null)
				return new BitmapDrawable(bmp);
			return null;
		}
		
		@Override
		protected void onPostExecute(Drawable result) {
			// TODO Auto-generated method stub
			if(result != null)
				view.setIcon(result);
		}
		
	}
	  

}
