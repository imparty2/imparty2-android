package com.manus.imparty2.core;

import java.io.File;
import java.io.FileDescriptor;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.manus.imparty2.Utils;

import android.os.Environment;

public class MediaProvider {

	public static final File BASE_FOLDER = new File(Environment.getExternalStorageDirectory(), "IMParty");
	static
	{
		BASE_FOLDER.mkdir();
	}

	public static final File IMAGE_FOLDER = new File(BASE_FOLDER, "IMParty Images");
	static
	{
		IMAGE_FOLDER.mkdir();
	}

	public static final File AUDIO_FOLDER = new File(BASE_FOLDER, "IMParty Audio");
	static
	{
		AUDIO_FOLDER.mkdir();
	}
	public static final File VIDEO_FOLDER = new File(BASE_FOLDER, "IMParty Video");
	static
	{
		VIDEO_FOLDER.mkdir();
	}
	public static final File FILES_FOLDER = new File(BASE_FOLDER, "IMParty Files");
	static
	{
		FILES_FOLDER.mkdir();
	}
	
	public static File get_media_file(Message message)
	{
		if(message.type.equals("image"))
		{
			File f = new File(message.file);
			if(f.exists()) return f;
			return new File(IMAGE_FOLDER,"IMPImage-ext-" +  Utils.md5(message.file));
		}
		else if(message.type.equals("audio"))
		{
			File f = new File(message.file);
			if(f.exists()) return f;
			return new File(AUDIO_FOLDER, "IMPAudio-ext-" + Utils.md5(message.file));
		}
		else if(message.type.equals("video"))
		{
			File f = new File(message.file);
			if(f.exists()) return f;
			return new File(VIDEO_FOLDER, "IMPAudio-ext-" + Utils.md5(message.file));
		}
		else if(message.type.equals("file"))
		{
			File f = new File(message.file);
			if(f.exists()) return f;
			return new File(FILES_FOLDER, message.data);
		
		}
		return new File("");
	}

	public static File get_image_file() {
		// TODO Auto-generated method stub
		return new File(IMAGE_FOLDER, "IMPImage-" + getTime() + ".jpg");
		
	}

	public static File get_audio_file() {
		// TODO Auto-generated method stub
		return new File(AUDIO_FOLDER, "IMPAudio-" + getTime() + ".acc");
		
	}
	public static File get_video_file() {
		// TODO Auto-generated method stub
		return new File(VIDEO_FOLDER, "IMPVideo-" + getTime() + ".mp4");
		
	}
	
	public static String getTime()
	{
		 return new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
         
	}
}
