package com.manus.imparty2.core;

import org.json.JSONException;
import org.json.JSONObject;

import com.manus.imparty2.Utils;

import android.content.Context;
import android.graphics.Bitmap;

public class Message {
	
	public String data;
	public String type;
	public String file;
	public Long size;
	public Message(String message) {
		// TODO Auto-generated constructor stub
		try {
			JSONObject obj = new JSONObject(message);
			type = obj.getString("t");
			data = obj.optString("d", "");
			file = obj.optString("f", "");
			size = obj.optLong("s", 0);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			data = message;
			type = "chat";
		}
	}
	

	public static String getGroupMessage(Context context, Message message) {
		// TODO Auto-generated method stub
		if(message.data.equals("user_add"))
		{
			return message.file + " ha entrado en el grupo";
		}
		else if(message.data.equals("user_remove"))
		{
			return message.file + " ha salido del grupo";
		}
		else if(message.data.equals("group_title"))
		{
			return "Cambio el asunto a: " + message.file;
		}
		else if(message.data.equals("group_avatar"))
		{
			return "Cambio el icono del grupo";
			
		}
		
		return "";
	}
//	
//	public static String build_image_message(String image)
//	{
//		String data = Utils.get_image_preview_base64(image);
//		String type = "image";
//		
//		try {
//			return new JSONObject().put("d", data).put("y", type).toString();
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
//	}

}
