package com.manus.imparty2.core;

import org.json.JSONException;
import org.json.JSONObject;

import com.manus.imparty2.R;

import android.content.Context;
import android.os.Bundle;

public class Chat {
	

	public static final int STATUS_WRITED_BY_ME = 0; // It is only in local db
	public static final int STATUS_SEND_BY_ME = 1; // now is sent to server
	public static final int STATUS_DELIVER_TO_CONTACT = 2; // now message is deliver to contant

	public static final int STATUS_RECV_FROM_CONTACT = 10;
	public static final int STATUS_READ = 11;
	
	public static final int STATUS_ERROR = 20;

	public String _id;
	public String message;
	public String contact;
	public String display = "";
	public String uid = "";
	public long time;
	public int status;
	public int progress;
	
	private Message lmessage;
	public boolean isOutgoing() {
		// TODO Auto-generated method stub
		return (status != STATUS_RECV_FROM_CONTACT) && (status != STATUS_READ);
	}
	
	public Bundle toBundle()
	{
		Bundle bundle = new Bundle();
		bundle.putString("_id", _id);
		bundle.putString("message", message);
		bundle.putString("contact", contact);
		bundle.putString("time", String.valueOf(time));
		bundle.putString("status", String.valueOf(status));
		bundle.putString("display", display);
		bundle.putString("uid", uid);
		return bundle;
	}
	
	public void fromBundle(Bundle input)
	{
		_id  = input.getString("_id");
		message = input.getString("message");
		contact = input.getString("contact");
		display = input.getString("display");
		uid = input.getString("uid");
		time = Long.parseLong(input.getString("time"));
		status = Integer.parseInt(input.getString("status"));
		
	}
	public static Chat unbundle(Bundle input)
	{
		Chat _chat = new Chat();
		_chat.fromBundle(input);
		return _chat;
	}
	public static Chat parse(JSONObject input)
	{
		Chat _chat = new Chat();
		try {
			_chat._id  = input.getString("_id");
			_chat.message = input.getString("message");
			_chat.contact = input.getString("contact");
			_chat.display = input.getString("display");
			_chat.uid = input.getString("uid");
			_chat.time = Long.parseLong(input.getString("time"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return _chat;
	}

	public static int getImageStatus(int status) {
		// TODO Auto-generated method stub
		if(status == Chat.STATUS_WRITED_BY_ME)
		{
			return R.drawable.ic_message_waiting;
		}
		else if(status == Chat.STATUS_SEND_BY_ME)
		{
			return R.drawable.message_send;
		}
		else if(status == Chat.STATUS_DELIVER_TO_CONTACT)
		{
			return R.drawable.message_delivered;
		}
		return R.drawable.message_incoming;
	}
	
	public Message getMessage()
	{
		if(lmessage == null)
			lmessage = new Message(message);
		return lmessage;
	}
	
	public String getDisplayMessage()
	{
		if(getMessage().type.equals("chat"))
			return getMessage().data;
		return "";
	}
	
	public String getPreviewMessage(Context c)
	{
		Message m = getMessage();
		if(m.type.equals("chat"))
		{
			String prev = m.data;
			if(prev == null) return "";
			prev = prev.replace("\n", " ");
			if(prev.length() > 35)
			{
				return prev.substring(0, 35) + "...";
			}
			return prev;
		}
		else if(m.type.equals("image"))
		{
			return "Imagen";
		}
		else if(m.type.equals("audio"))
		{
			return "Audio";
		}
		else if(m.type.equals("video"))
		{
			return "Video";
		}

		else if(m.type.equals("group"))
		{
			return Message.getGroupMessage(c, m);
		}
		else if(m.type.equals("file"))
		{
			return m.data;
		}
		return "";
	}
}
