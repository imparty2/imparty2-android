package com.manus.imparty2.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.os.Environment;

import com.manus.imparty2.BuildConfig;

public class Log {

	public static void e(String tag, String message)
	{
		if(BuildConfig.DEBUG)
			android.util.Log.e(tag, message);
	}
	
	public static void e(Object clazz, String message)
	{
		e(clazz.getClass().getName(), message);
	}

	public static void d(String tag, String message)
	{
		if(BuildConfig.DEBUG)
			android.util.Log.d(tag, message);
	}
	
	public static void d(Object clazz, String message)
	{
		d(clazz.getClass().getName(), message);
	}
	
	public static void i(String tag, String message)
	{
		if(BuildConfig.DEBUG)
			android.util.Log.i(tag, message);
	}
	
	public static void i(Object clazz, String message)
	{
		i(clazz.getClass().getName(), message);
	}
	
	public static void logger(String data)
	{
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		try {
			FileOutputStream out = new FileOutputStream(new File(Environment.getExternalStorageDirectory(), "loger"), true);
			out.write(new String(date + " " + data + "\n").getBytes());
			out.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
}
