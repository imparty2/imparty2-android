package com.manus.imparty2.core;

import android.os.Bundle;

public class GroupChat extends Chat {
	public String uid;
	@Override
	public Bundle toBundle() {
		// TODO Auto-generated method stub
		Bundle b = super.toBundle();
		b.putString("uid", uid);
		return b;
	}
	
	@Override
	public void fromBundle(Bundle input) {
		// TODO Auto-generated method stub
		super.fromBundle(input);
		uid = input.getString("uid");
	}
	
	public static GroupChat parse(Bundle b)
	{
		GroupChat _c = new GroupChat();
		_c.fromBundle(b);
		return _c;
	}

}
