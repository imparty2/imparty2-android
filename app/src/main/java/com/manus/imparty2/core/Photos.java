package com.manus.imparty2.core;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore.Images;
import android.util.Log;

public class Photos {

    private static final int MAX_IMAGE_DIMENSION = 600;
    private static final String TAG = "Photos";

    private static int rotationForImage(Context context, Uri uri) {
        if (uri.getScheme().equals("content")) {
            String[] projection = {Images.ImageColumns.ORIENTATION};
            Cursor c = context.getContentResolver().query(uri, projection,
                    null, null, null);
            if (c.moveToFirst()) {
                return c.getInt(0);
            }
        } else if (uri.getScheme().equals("file")) {
            try {
                ExifInterface exif = new ExifInterface(uri.getPath());
                int rotation = (int) exifOrientationToDegrees(exif
                        .getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL));
                return rotation;
            } catch (IOException e) {
                Log.e(TAG, "Error checking exif", e);
            }
        }
        return 0;
    }

    private static float exifOrientationToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    public static Bitmap getCorrectlyOrientedImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();

        int rotatedWidth, rotatedHeight;
        int orientation = Photos.rotationForImage(context, photoUri);

        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }

        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > MAX_IMAGE_DIMENSION || rotatedHeight > MAX_IMAGE_DIMENSION) {
            float widthRatio = ((float) rotatedWidth) / ((float) MAX_IMAGE_DIMENSION);
            float heightRatio = ((float) rotatedHeight) / ((float) MAX_IMAGE_DIMENSION);
            float maxRatio = Math.max(widthRatio, heightRatio);

            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();

        /*
         * if the orientation is not 0 (or -1, which means we don't know), we
         * have to do a rotation.
         */
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);

            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(),
                    srcBitmap.getHeight(), matrix, true);
        }

        return srcBitmap;
    }

    public static int getSampleSize(Bitmap bmp, int max_size)
    {
    	if(bmp.getWidth() > max_size || bmp.getHeight() > max_size)
    	{
    		
	    	float widthRatio = ((float) bmp.getWidth()) / ((float) max_size);
	        float heightRatio = ((float) bmp.getHeight()) / ((float) max_size);
	        return (int) Math.max(widthRatio, heightRatio);
    	}
    	return 0;
    }
    
    public static Bitmap getSampleSize(String file, int size)
    {
    	BitmapFactory.Options bounds = new BitmapFactory.Options();
    	bounds.inJustDecodeBounds = true;
    	BitmapFactory.decodeFile(file, bounds);
    	if (bounds.outWidth == -1) return null;
    	int width = bounds.outWidth;
    	int height = bounds.outHeight;
    	boolean withinBounds = width <= size && height <= size;
    	if (!withinBounds) {
    		float widthRatio = ((float) width) / ((float) size);
	        float heightRatio = ((float) height) / ((float) size);
	        float newWidth = Math.max(widthRatio, heightRatio);
    	    int sampleSize = Math.round(newWidth);
    	    BitmapFactory.Options resample = new BitmapFactory.Options();
    	    resample.inSampleSize = sampleSize;
    	    return BitmapFactory.decodeFile(file, resample);
    	}
    	return BitmapFactory.decodeFile(file);
    }
    
    public static Bitmap redimensionateBitmap(Bitmap in, int maxSize)
    {
    	int width = in.getWidth();
    	int height = in.getHeight();
    	boolean redim = width <= maxSize && height <= maxSize;
    	if(redim)
    	{
    		float widthRatio = ((float) maxSize) / ((float) width);
	        float heightRatio = ((float) maxSize) / ((float) height);

	        Matrix matrix = new Matrix();
	        // RESIZE THE BIT MAP
	        matrix.postScale(widthRatio, heightRatio);
	        Bitmap resizedBitmap = Bitmap.createBitmap(in, 0, 0, width, height, matrix, false);
	        in.recycle();
	        return resizedBitmap;
    	}
    	return in;
    }
    
    public static Bitmap applyGaussianFilter(Bitmap src) {
        Bitmap dest = Bitmap.createBitmap(
                src.getWidth(), src.getHeight(), src.getConfig());
        int KERNAL_WIDTH = 7;
        int KERNAL_HEIGHT = 7;

        int[][] knl = {
            {67, 2292, 19117, 38771, 19117, 2292, 67},
            {2292, 78633, 655965, 1330373, 655965, 78633, 2292},
            {19117, 655965, 5472157, 11098164, 5472157, 655965, 19117},
            {38771, 1330373, 11098164, 22508352, 11098164, 1330373, 38771},
            {19117, 655965, 5472157, 11098164, 5472157, 655965, 19117},
            {2292, 78633, 655965, 1330373, 655965, 78633, 2292},
            {67, 2292, 19117, 38771, 19117, 2292, 67}
        };
        int bmWidth = src.getWidth();
        int bmHeight = src.getHeight();
        int bmWidth_MINUS_6 = bmWidth - 6;
        int bmHeight_MINUS_6 = bmHeight - 6;
        int bmWidth_OFFSET_3 = 3;
        int bmHeight_OFFSET_3 = 3;

        for (int i = bmWidth_OFFSET_3; i <= bmWidth_MINUS_6; i++) {
            for (int j = bmHeight_OFFSET_3; j <= bmHeight_MINUS_6; j++) {

                //get the surround 7*7 pixel of current src[i][j] into a matrix subSrc[][]
                int[][] subSrc = new int[KERNAL_WIDTH][KERNAL_HEIGHT];
                for (int k = 0; k < KERNAL_WIDTH; k++) {
                    for (int l = 0; l < KERNAL_HEIGHT; l++) {
                        subSrc[k][l] = src.getPixel(i - bmWidth_OFFSET_3 + k, j - bmHeight_OFFSET_3 + l);
                    }
                }

                //subSum = subSrc[][] * knl[][]
                long subSumR = 0;
                long subSumG = 0;
                long subSumB = 0;

                for (int k = 0; k < KERNAL_WIDTH; k++) {
                    for (int l = 0; l < KERNAL_HEIGHT; l++) {
                        subSumR += (long) (Color.red(subSrc[k][l])) * (long) (knl[k][l]);
                        subSumG += (long) (Color.green(subSrc[k][l])) * (long) (knl[k][l]);
                        subSumB += (long) (Color.blue(subSrc[k][l])) * (long) (knl[k][l]);
                    }
                }

                subSumR = subSumR / 100000000;
                subSumG = subSumG / 100000000;
                subSumB = subSumB / 100000000;

                if (subSumR < 0) {
                    subSumR = 0;
                } else if (subSumR > 255) {
                    subSumR = 255;
                }

                if (subSumG < 0) {
                    subSumG = 0;
                } else if (subSumG > 255) {
                    subSumG = 255;
                }

                if (subSumB < 0) {
                    subSumB = 0;
                } else if (subSumB > 255) {
                    subSumB = 255;
                }

                dest.setPixel(i, j, Color.argb(
                        Color.alpha(src.getPixel(i, j)),
                        (int) subSumR,
                        (int) subSumG,
                        (int) subSumB));
            }
        }

        return dest;
    }
}
