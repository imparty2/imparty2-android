package com.manus.imparty2.core;

import com.manus.imparty2.Constants;
import com.manus.imparty2.R;

public class Provider {

	public static int getProviderIcon(String provider)
	{
		if(provider != null)
		{
			if(provider.equals(Constants.PROVIDER_GOOGLE))
				return R.drawable.ic_provider_google;
			if(provider.equals(Constants.PROVIDER_TWITTER))
				return R.drawable.ic_provider_twitter;
			if(provider.equals(Constants.PROVIDER_FACEBOOK))
				return R.drawable.ic_provider_facebook;
			if(provider.equals(Constants.PROVIDER_PHONEBOOK))
				return R.drawable.ic_provider_phonebook;
		}
		return R.drawable.ic_launcher;
	}
}
