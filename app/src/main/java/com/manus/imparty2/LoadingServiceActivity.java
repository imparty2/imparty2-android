package com.manus.imparty2;

import com.manus.imparty2.service.MessagingService;
import com.manus.imparty2.service.MessagingService.MessageReceiver;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class LoadingServiceActivity extends Activity implements MessageReceiver {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_loading_service);
		
		MessagingService.attachListenner(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.loading, menu);
		return true;
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		MessagingService.removeListenner(this);
	}

	@Override
	public boolean onMessage(Bundle data) {
		// TODO Auto-generated method stub
		if(data.getInt("key") == MessagingService.BUNDLE_KEY_SERVICE_BOOT)
		{
			finish();
		}
		return false;
	}

}
