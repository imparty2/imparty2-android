package com.manus.imparty2;

import org.json.JSONObject;

import com.manus.imparty2.client.API;
import com.manus.imparty2.login.FacebookLoginActivity;
import com.manus.imparty2.login.GoogleLoginActivity;
import com.manus.imparty2.login.PhoneNumberActivity;
import com.manus.imparty2.login.TwitterLoginActivity;
import com.manus.imparty2.service.UpdatesManager;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends Activity {

	public static int ACTIVITY_RESULT_LOGIN = 1;
	/**
	 * A dummy authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.
	 */
	private static final String[] DUMMY_CREDENTIALS = new String[]{"foo@example.com:hello", "bar@example.com:world"};

	/**
	 * The default email to populate the email field with.
	 */
	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

	// Values for email and password at the time of the login attempt.
	private String mEmail;
	private String mPassword;

	// UI references.
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);

		// Set up the login form.
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);
		findViewById(R.id.button1).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//if(Utils.device_have_sim(LoginActivity.this))
				//{
					startActivityForResult(new Intent(LoginActivity.this, PhoneNumberActivity.class), ACTIVITY_RESULT_LOGIN);
				//}
				//else
				//{
				//	startActivityForResult(new Intent(LoginActivity.this, GoogleLoginActivity.class), ACTIVITY_RESULT_LOGIN);
				//}
			}
		});
		UpdatesManager manager = new UpdatesManager(this);
		if (manager.is_update()) {
			manager.dispay_dialog();
		}
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == ACTIVITY_RESULT_LOGIN) {
			if (resultCode == RESULT_OK) {
				mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
				showProgress(true);
				ProviderLoginTask login = new ProviderLoginTask(data.getExtras());
				login.execute((Void) null);
			} else if (resultCode == RESULT_FIRST_USER + 1) {
				Builder builder = new Builder(this);
				builder.setMessage("Se a producido un error!");
				builder.create().show();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
				}
			});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
				}
			});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	public class ProviderLoginTask extends AsyncTask<Void, Void, Boolean> {

		String provider, user, hash;
		public ProviderLoginTask(Bundle result) {
			// TODO Auto-generated constructor stub
			provider = result.getString("provider");
			user = result.getString("username");
			hash = result.getString("hash");
		}
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			JSONObject obj = API.getToken(provider, user, hash);
			if (obj != null) {
				try {
					if (obj.getString("result").equals("ok")) {
						Intent i = new Intent();
						i.putExtra("uid", obj.getString("uid"));
						i.putExtra("token", obj.getString("token"));
						setResult(RESULT_OK, i);
						finish();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return false;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			//mAuthTask = null;
			showProgress(false);

			if (success) {
				finish();
			} else {
				Builder b = new Builder(LoginActivity.this);
				b.setMessage("Error al iniciar la sesion!");
				b.create().show();
			}
		}

		@Override
		protected void onCancelled() {
			//mAuthTask = null;
			showProgress(false);
		}
	}
}
