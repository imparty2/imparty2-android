package com.manus.imparty2.service;

import java.io.File;

import com.manus.imparty2.ConfigManager;
import com.manus.imparty2.ContactListActivity;
import com.manus.imparty2.R;
import com.manus.imparty2.Utils;
import com.manus.imparty2.client.API;
import com.manus.imparty2.client.HttpUtils;
import com.manus.imparty2.client.HttpUtils.HttpProgressListenner;
import com.manus.imparty2.client.UpdateInfo;
import com.manus.imparty2.push.PushMessages;

import android.app.AlertDialog.Builder;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

public class UpdatesManager {

	private static final String SWITCH_EVENT = "com.manus.imparty2.DOWNLOAD";
	private static final String CANCEL_EVENT = "com.manus.imparty2.CANCEL";

	Context context;
	public UpdatesManager(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	public boolean is_update() {
		try {
			String version = get_preferences().getString("version", "");
			String ignored_version = get_preferences().getString("ignore_version", "");
			if (!ignored_version.equals(version)) {

				if (!version.equals("") && Integer.parseInt(version) > get_current_version()) {

					return true;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}
	public void check_update() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				// Download updates info;

				UpdateInfo updates = API.get_update();
				if(updates == null) return;
				String cert = Utils.getAppCert(context);
				String current = updates.current.get(cert.toUpperCase());
				if (current != null) {
					int newVersion = Integer.parseInt(current);
					if (newVersion > get_current_version()) {
						String url = updates.versions.getJSONObject(current).getString("get");
						get_preferences().edit().putString("version", current).putString("url", url).commit();

						notify_update();
					}
				}
			}
		}).start();

		if (is_update()) {
			notify_update();
		} else {
			hide_update();
		}
	}

	private int get_current_version() {
		try {
			return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	private SharedPreferences get_preferences() {
		return context.getSharedPreferences("update", 0);
	}
	private void hide_update() {
		((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(102);

	}
	private void notify_update() {
		// DownloadButtonListener listenner = new DownloadButtonListener();
		// context.registerReceiver(listenner, new IntentFilter(SWITCH_EVENT));
		// Intent switchIntent = new Intent(SWITCH_EVENT);
		// PendingIntent contentIntent = PendingIntent.getBroadcast(context, 0,
		// switchIntent, 0);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent(context, ContactListActivity.class), 0);

		NotificationCompat.Builder b = new NotificationCompat.Builder(context);
		b.setContentTitle("IMParty Update");
		b.setContentText("Nueva actualizacion de IMParty!");
		b.addAction(R.drawable.ic_download, "Descargar", contentIntent);
		b.setSmallIcon(com.manus.imparty2.R.drawable.ic_launcher);
		b.setContentIntent(contentIntent);
		b.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
		Notification noti = b.build();
		((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(102, noti);

	}
	private void display_update_succeed(File file) {
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, Utils.InstallAPK(file.getAbsolutePath()), 0);

		NotificationCompat.Builder b = new NotificationCompat.Builder(context);
		b.setContentTitle("IMParty Update");
		b.setContentText("Nueva actualizacion de IMParty!");
		b.setSubText("Haz click aqui para actualizar.");
		b.setSmallIcon(com.manus.imparty2.R.drawable.ic_launcher);
		b.setContentIntent(contentIntent);
		b.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
		Notification noti = b.build();
		((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(101, noti);

	}

	public void download_update() {
		String version = get_preferences().getString("version", "");
		String url = get_preferences().getString("url", "");
		((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(102);

		File folder = new File(Environment.getExternalStorageDirectory(), "IMParty");
		folder.mkdirs();
		final File file = new File(folder, "IMParty-" + version + ".apk");
		PendingIntent progressIntent = PendingIntent.getActivity(context, 0, new Intent(), 0);

		final NotificationCompat.Builder progress = new NotificationCompat.Builder(context);

		progress.setProgress(100, 0, true);
		progress.setContentTitle("IMParty Update");
		progress.setContentText("Descargando...");
		progress.setSmallIcon(com.manus.imparty2.R.drawable.ic_launcher);
		progress.setContentIntent(progressIntent);
		HttpUtils.http_download(url, file.getAbsolutePath(), new HttpProgressListenner() {

			@Override
			public void onProgress(int p) {
				// TODO Auto-generated method stub
				progress.setProgress(100, p, false);
				((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(101, progress.build());

			}

			@Override
			public void onError(String message) {
				// TODO Auto-generated method stub
				notify_update();
			}

			@Override
			public void onComplete(String response) {
				// TODO Auto-generated method stub
				((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(101);

				context.startActivity(Utils.InstallAPK(file.getAbsolutePath()));

			}
		});
	}

	public void dispay_dialog() {
		Builder b = new Builder(context);
		b.setTitle("Actualizacion");
		b.setMessage("Hay una nueva version de IMParty. Descargar?");
		b.setPositiveButton(android.R.string.ok, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				download_update();
			}
		});
		b.setNegativeButton("Ignorar", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				get_preferences().edit().putString("ignore_version", get_preferences().getString("version", "")).commit();
				check_update();
			}
		});

		b.create().show();

	}

	public class DownloadButtonListener extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {

			String a = "";
			a.toCharArray();
			download_update();

		}

	}
}
