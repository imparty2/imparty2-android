package com.manus.imparty2.service;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.manus.imparty2.ConfigManager;
import com.manus.imparty2.Constants;
import com.manus.imparty2.ContactListActivity;
import com.manus.imparty2.GcmBroadcastReceiver;
import com.manus.imparty2.LoadingServiceActivity;
import com.manus.imparty2.Utils;
import com.manus.imparty2.client.API;
import com.manus.imparty2.client.API.APIHandler;
import com.manus.imparty2.client.Account;
import com.manus.imparty2.client.HttpUtils;
import com.manus.imparty2.client.ServerConfig;
import com.manus.imparty2.client.HttpUtils.HttpProgressListenner;
import com.manus.imparty2.client.Login;
import com.manus.imparty2.client.PaginatedContacts;
import com.manus.imparty2.client.Presence;
import com.manus.imparty2.core.Chat;
import com.manus.imparty2.core.Contact;
import com.manus.imparty2.core.GroupChat;
import com.manus.imparty2.core.ISO2Phone;
import com.manus.imparty2.core.MediaProvider;
import com.manus.imparty2.core.Message;
import com.manus.imparty2.core.Phones;
import com.manus.imparty2.core.Recent;
import com.manus.imparty2.database.AccountTable;
import com.manus.imparty2.database.ChatTable;
import com.manus.imparty2.database.ContactsTable;
import com.manus.imparty2.database.DatabaseManager;
import com.manus.imparty2.push.PushHelper;
import com.manus.imparty2.push.PushMessages;
import com.manus.push.Push;
import com.manus.push.Push.OnPush;

import android.app.IntentService;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.text.InputFilter.LengthFilter;
import android.text.format.DateFormat;
import android.util.Log;

public class MessagingService extends IntentService implements com.manus.imparty2.push.PushMessages.MessageReceiver, OnPush{

	public MessagingService() {
		super("MessagingService");
		MessagingService.instance = this;
		Log.d(TAG, "MessagingService construct");
		// TODO Auto-generated constructor stub
	}
	public static final int BUNDLE_KEY_LOGIN = 0x0001;
	public static final int BUNDLE_KEY_SERVICE_BOOT = 0x0010;
	public static final int BUNDLE_KEY_MERGE = 0x0020;
	public static final int BUNDLE_KEY_CONTACTS_UPDATE = 0x0021;
	public static final int BUNDLE_KEY_CHAT = 0x0030;
	public static final int BUNDLE_KEY_PRESENCE = 0x0031;

	public static final Bundle LOGIN_ERROR = new Bundle();
	static {
		LOGIN_ERROR.putInt("key", BUNDLE_KEY_LOGIN);
		LOGIN_ERROR.putBoolean("error", true);
	}
	public static final Bundle LOGIN_DENY = new Bundle();
	static {
		LOGIN_DENY.putInt("key", BUNDLE_KEY_LOGIN);
		LOGIN_DENY.putBoolean("error", true);
		LOGIN_DENY.putBoolean("deny", true);
	}
	public static final Bundle SERVICE_BOOTUP = new Bundle();
	static {
		SERVICE_BOOTUP.putInt("key", BUNDLE_KEY_SERVICE_BOOT);
	}
	public static final Bundle MERGE_FINISH = new Bundle();
	static {
		MERGE_FINISH.putInt("key", BUNDLE_KEY_MERGE);
		MERGE_FINISH.putBoolean("finish", true);
	}
	public static final Bundle MERGE_PROGRESS = new Bundle();
	private static final String TAG = "MessagingService";
	static {
		MERGE_PROGRESS.putInt("key", BUNDLE_KEY_MERGE);
		MERGE_PROGRESS.putBoolean("finish", false);
	}

	ContactsTable contactsTable = new ContactsTable();
	ChatTable chatsTable = new ChatTable();
	AccountTable accountsTable = new AccountTable();
	String uid, token;
	Push p;
	private static MessagingService instance;
	private long last_query_time = 0;
	private static int sessionCounter = 0;
	public Login loginInfo;
	public interface MessageReceiver {
		boolean onMessage(Bundle data);
	}
	public interface Executor<T> {
		public void result(T result);
	}
	private Handler handler = new Handler();
	private static List<MessageReceiver> handlers = new ArrayList<MessageReceiver>();
	private int time_offset;
	private boolean has_boot = false;
	private boolean do_login = false;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		instance = null;
		DatabaseManager.getInstance().close();
		// PushMessages.removeReceiver(this);
		Log.d(TAG, "onDestroy");
		// debug(this, "Service destroyed", "destroyed service at: " +
		// DateFormat.getTimeFormat(this).format(new Date()));

	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		// PushMessages.registerReceiver(this);

		Log.d(TAG, "onCreate");
		if (!ConfigManager.hasLogin(this)) {
			try {
				throw new Exception("No login");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		super.onCreate();
		if (ConfigManager.hasLogin(this)) {
			DatabaseManager.init();
			reinit();
			__sendMessage(SERVICE_BOOTUP);
			init_user();
			API.setAPIHandler(new APIHandler() {
				
				@Override
				public void request_login() {
					// TODO Auto-generated method stub
					last_query_time = 0;
				}
			});
		}
		UpdatesManager manager = new UpdatesManager(this);
		manager.check_update();

	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO do something useful
		// debug(this, "onStartCommand", "");
		processIntentMessage(intent);
		if (intent != null && intent.getBooleanExtra("sync", false)) {
			if (ConfigManager.hasLogin(this) && last_query_time != 0 ) {
				sync_recv();
			}
		}
		return Service.START_STICKY;
	}
	public static MessagingService getInstance() {
		return MessagingService.instance;
	}

	public static void attachListenner(MessageReceiver listenner) {
		handlers.add(listenner);
		if (getInstance() != null)
			getInstance().__sendMessage(SERVICE_BOOTUP);
	}

	public static void removeListenner(MessageReceiver listenner) {
		handlers.remove(listenner);
	}

	private void __sendMessage(final Bundle data) {
		__sendMessage(data, null);
	}
	private void __sendMessage(final Bundle data, final Executor<Boolean> listenner) {

		handler.post(new Runnable() {

			@Override
			public void run() {
				boolean result = false;
				for (MessageReceiver handler : handlers) {
					try {
						if (handler.onMessage(data)) {
							result = true;
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						Log.e("__sendMessage", "Sending: " + data.toString());
					}
				}
				if (listenner != null)
					listenner.result(result);
			}
		});
	}

	private void init_user() {
		uid = ConfigManager.getEmail(this);
		token = ConfigManager.getToken(this);
		new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (ensureLogin()) {
					send_messages_wrote();
				}
			}
		}).start();
	}
	public void register_push()
	{
		String proto = "";
		String reg = "";
		if(PushHelper.isGCMAvailable(this) && !PreferenceManager.getDefaultSharedPreferences(this).getBoolean("notifications_force_jpush", false))
		{
			proto = "gcm";
			int c = 0;
			while (c < 2) {
				reg = PushHelper.getRegistration(this);
				if (reg != null) {
					break;
				}
				c++;
			}
		}
		else
		{
			proto = "jpush";
			if(p == null || !p.isRunning())
			{
				p = new Push(Constants.JPUSH_SERVER);
				p.setPush(this);
				reg = get_push_reg();
				if(reg != null)
					p.connect(reg);
			}
			else
			{
				reg = get_push_reg();
			}
		}
		API.set_protocol(reg, proto, Utils.getDeviceID(this), Utils.getDeviceName());
		
	}
	
	private String get_push_reg()
	{
		String reg = PreferenceManager.getDefaultSharedPreferences(this).getString("push_id", "");
		if(reg.equals(""))
		{
			reg = p.register();
			if(reg != null)
				PreferenceManager.getDefaultSharedPreferences(this).edit().putString("push_id", reg).commit();
		}
		return reg;
	}
	
	public boolean refresh_login() {
		do_login = true;
		if ((loginInfo = API.login(uid, token)) == null) {
			__sendMessage(LOGIN_ERROR);
			do_login = false;
			return false;
		}
		if (loginInfo.deny) {
			__sendMessage(LOGIN_DENY);
			do_login = false;
			return false;
		}
		ServerConfig.save(this, loginInfo.config);
		register_push();
		time_offset = (int) ((System.currentTimeMillis() / 1000) - loginInfo.server_time);
		accountsTable.save_account(loginInfo.accounts);
		Bundle b = new Bundle();
		b.putInt("key", BUNDLE_KEY_LOGIN);
		b.putBoolean("result", true);
		__sendMessage(b);
		sync_recv();
		do_login = false;
		
		return true;
	}
	
	public boolean isLogin()
	{
		if(loginInfo != null && !loginInfo.deny){
			return true;
		}
		return false;
	}
	public static void reinit() {
		// TODO Auto-generated method stub
		getInstance().last_query_time = 0;
		getInstance().uid = "";
		getInstance().token = "";
	}

	public static String generateMID() {
		return sessionCounter + "-" + Utils.md5((sessionCounter++) + "@" + System.currentTimeMillis());
	}

	public boolean ensureLogin() {
		boolean force = true;
		if (do_login) {
			force = false;
		}
		int strikes = 0;
		while (do_login)
		{
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(do_login)
				strikes++;
			if(strikes == 20) break;
		}
		if (last_query_time == 0 || System.currentTimeMillis() - last_query_time > ServerConfig.getInt(this, "session_expiration") * 1000) {

			if (force) {
				if (!refresh_login())
					return false;
				last_query_time = System.currentTimeMillis();
			}
		}
		return true;
	}
	public void send_messages_wrote() {
		if (ensureLogin()) {
			List<Chat> list = chatsTable.select_messages("status = '" + Chat.STATUS_WRITED_BY_ME + "'");
			if (list.size() > 0)
				procressSend(API.sendMessages(list));
		}
	}
	public List<Account> getAccounts() {
		return accountsTable.list_accounts();
	}
	public List<Recent> getRecents() {
		List<Recent> ret = new ArrayList<Recent>();
		List<Chat> chats = chatsTable.list_recents();
		for (Chat chat : chats) {
			Recent _r = new Recent();
			if (chat.contact != null) {
				_r.contact = contactsTable.query(chat.contact);
				if (_r.contact == null) {
					_r.contact = new Contact();
					_r.contact.username = chat.contact;
					_r.contact.uid = chat.contact;
					_r.contact.display_name = chat.contact;
					_r.contact.avatar = "";
				}
				_r.chat = chat;
				if (chat.status == Chat.STATUS_RECV_FROM_CONTACT) {
					_r.count = chatsTable.count_unread_messages(chat.contact);
				}
				ret.add(_r);
			}
		}
		return ret;
	}
	public void sync_account(Account account, boolean flag) {
		accountsTable.sync_account(account, flag);
	}
	public boolean is_sync_account(Account account) {
		return accountsTable.is_sync(account);
	}

	public void update_chat(Chat item, int step, final int until) {
		final Bundle b = item.toBundle();
		b.putInt("key", BUNDLE_KEY_CHAT);
		b.putBoolean("refresh", true);
		Timer _timer = new Timer();
		TimerTask _task = new TimerTask() {
			long start = System.currentTimeMillis();
			@Override
			public void run() {
				// TODO Auto-generated method stub
				__sendMessage(b);
				if (System.currentTimeMillis() - start > until) {
					cancel();
				}
			}
		};
		_timer.schedule(_task, 0, step);

	}
	public void download_chat(Chat item) {
		
		Message m = item.getMessage();
		if(!HttpUtils.isDownload(m.file))
		{
			
			final Bundle b = new Bundle(item.toBundle());
			b.putInt("key", BUNDLE_KEY_CHAT);
			b.putInt("progress", -1);
			__sendMessage(b);
			// TODO Auto-generated method stub
			HttpUtils.http_download(m.file, MediaProvider.get_media_file(m).getAbsolutePath(), new HttpProgressListenner() {
	
				@Override
				public void onProgress(int progress) {
					// TODO Auto-generated method stub
					b.putInt("progress", progress);
					__sendMessage(b);
				}
	
				@Override
				public void onError(String message) {
					// TODO Auto-generated method stub
	
				}
	
				@Override
				public void onComplete(String response) {
					// TODO Auto-generated method stub
					b.putInt("progress", 100);
					__sendMessage(b);
	
				}
			});
		}
		else
		{
			HttpUtils.http_cancel_upload(m.file);
		}

	}
	public void send_image(final String contact, final String file) {
		final String data = Utils.get_image_preview_base64(file);
		send_media(contact, file, "image", data);
	}
	public void send_audio(final String contact, final String file, final int leng) {
		send_media(contact, file, "audio", leng + "");
	}
	public void send_file(String contact, File file) {
		send_media(contact, file.getAbsolutePath(), "file", file.getName());
	}

	public void send_video(String username, String file, int duration) {
		// TODO Auto-generated method stub

		String data = Utils.get_preview_video_image_base64(file);
		send_media(username, file, "video", duration + ":" + data);
	}
	public void send_media(final String contact, final String file, final String type, final String data) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				String _id = generateMID();
				JSONObject obj = new JSONObject();
				try {
					obj.put("t", type);
					obj.put("f", file);
					obj.put("d", data);
					obj.put("s", new File(file).length());
					String raw = obj.toString();
					final Chat c = chatsTable.insert(contact, raw, System.currentTimeMillis(), _id, Chat.STATUS_WRITED_BY_ME);
					final Bundle b = new Bundle();
					b.putInt("key", BUNDLE_KEY_CHAT);
					b.putAll(c.toBundle());
					__sendMessage(b);
					String media_server = ServerConfig.getConfig(MessagingService.this, "media_server", "");
					String md5 = Utils.md5_file(new File(file));
					HttpUtils.http_upload(file, media_server + "?md5=" + md5, new HttpProgressListenner() {
						boolean add = false;
						@Override
						public void onProgress(int progress) {
							// TODO Auto-generated method stub
							if (add)
								b.putInt("progress", progress);
							else {
								add = true;
								b.putInt("progress", -1);
							}
							__sendMessage(b);
						}

						@Override
						public void onError(String message) {
							// TODO Auto-generated method stub
							update_chat_status(b.getString("id"), Chat.STATUS_ERROR);

						}

						@Override
						public void onComplete(String response) {
							// TODO Auto-generated method stub
							JSONObject json;
							try {
								b.putInt("progress", 100);
								__sendMessage(b);
								json = new JSONObject(response);
								JSONObject obj = new JSONObject();
								obj.put("t", type);
								obj.put("f", json.getString("url"));
								obj.put("d", data);
								obj.put("s", new File(file).length());
								c.message = obj.toString();
								new Thread(new Runnable() {

									@Override
									public void run() {
										send_chat(c);
									}
								}).start();
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								update_chat_status(b.getString("id"), Chat.STATUS_ERROR);

							}
						}
					});
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}

	public void send_message(final String contact, final String message) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				send_chat(contact, message);
			}
		}).start();
	}

	private void send_chat(String contact, JSONObject message) {
		send_chat(contact, message.toString());
	}
	private void send_chat(String contact, String message) {

		String _id = generateMID();

		Chat c = chatsTable.insert(contact, message, System.currentTimeMillis(), _id, Chat.STATUS_WRITED_BY_ME);
		Bundle b = new Bundle();
		b.putInt("key", BUNDLE_KEY_CHAT);
		b.putAll(c.toBundle());
		__sendMessage(b);
		send_chat(c);
	}
	private void send_chat(Chat c) {
		if (ensureLogin()) {
			procressSend(API.sendMessages(c));
		}

	}

	public void send_deliver(String contact, String id) {
		if (ensureLogin())
			API.deliver(contact, id);
	}

	private void procressSend(JSONArray arr) {
		if (arr != null) {
			for (int i = 0; i < arr.length(); i++) {
				JSONObject obj;
				try {
					obj = arr.getJSONObject(i);
					if (obj.getBoolean("result")) {
						update_chat_status(obj.getString("id"), Chat.STATUS_SEND_BY_ME);
					} else {
						update_chat_status(obj.getString("id"), Chat.STATUS_ERROR);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void retry_message(String id) {

	}
	public void retry_message(Chat chat) {

	}

	private void update_chat_status(String id, int status) {
		Chat c = chatsTable.select(id);
		if (c == null)
			return;
		c.status = status;
		chatsTable.update_status(id, status);
		Bundle b = new Bundle();
		b.putInt("key", BUNDLE_KEY_CHAT);
		b.putAll(c.toBundle());
		__sendMessage(b);
	}

	public List<Contact> getAddressBook() {
		List<Account> accs = accountsTable.list_sync_accounts();
		Account acc = new Account();
		acc.username = "contacts";
		accs.add(acc);
		boolean joined = getSharedPreferences("imparty", 0).getBoolean("show_joined", true);
		return contactsTable.list(accs, joined);
	}
	public String getDefaultCountryCode() {
		TelephonyManager tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		return ISO2Phone.getPhone(tel.getNetworkCountryIso());
	}

	private boolean isMerge = false;
	public boolean mergeContacts() {
		if (!isMerge) {
			isMerge = true;

			new Thread(new Runnable() {

				@Override
				public void run() {
					if (ensureLogin()) {

						JSONArray array = new JSONArray();
						Uri uri = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
						String[] PROJECTION = new String[]{android.provider.ContactsContract.Contacts._ID, android.provider.ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.CommonDataKinds.Email.DATA};
						ContentResolver cr = MessagingService.this.getContentResolver();
						Cursor cur = cr.query(uri, PROJECTION, null, null, null);
						List<String> l = new ArrayList<String>();
						while (cur.moveToNext()) {
							array.put(cur.getString(2));
						}
						cur.close();
						uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
						PROJECTION = new String[]{android.provider.ContactsContract.Contacts._ID, android.provider.ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.DATA};
						cr = MessagingService.this.getContentResolver();
						cur = cr.query(uri, PROJECTION, null, null, null);
						while (cur.moveToNext()) {
							String email = cur.getString(2);
							email = email.replaceAll(" ", "");
							if (!email.contains("+") && !email.contains("@")) {
								email = getDefaultCountryCode() + email;
							}
							email = email.replace("+", "").replace("-", "");
							array.put(email);
						}
						cur.close();
						List<Contact> list = API.merge_contacts(array);

						contactsTable.drop_contacts("contacts");
						contactsTable.sync(list, "contacts");

						__sendMessage(MERGE_PROGRESS);
						List<Account> accounts = accountsTable.list_accounts();
						for (Account account : accounts) {
							contactsTable.drop_contacts(account.username, account.provider);

							if (!account.sync) {
								continue;
							}
							String page = "";
							while (page != null && !page.equals("0") && !page.equals("null")) {

								PaginatedContacts contacts = API.merge_account(account, page);
								if (contacts != null) {

									contactsTable.sync(contacts.contacts, account.username);
									__sendMessage(MERGE_PROGRESS);
									page = contacts.next_page;
								} else {
									page = null;
								}
							}

						}
					}
					__sendMessage(MERGE_FINISH);
					isMerge = false;

				}
			}).start();
			return true;
		}

		return false;
	}

	public Contact getContact(String username) {
		// TODO Auto-generated method stub
		return contactsTable.query(username);
	}

	public List<Chat> getChats(String username) {
		// TODO Auto-generated method stub
		return chatsTable.query(username);
	}

	private String decode(String message, String encoder) {
		if (encoder != null) {
			if (encoder.equals("url")) {
				message = URLDecoder.decode(message);
			} else if (encoder.equals("ex1")) {
				token.getBytes();
			}
		}
		return message;
	}
	@Override
	public void onMessage(Bundle data) {

		processBundleMessage(data);
	}
	private void processIntentMessage(Intent intent) {
		if (intent == null)
			return;
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (extras != null && !extras.isEmpty()) { // has effect of unparcelling
													// Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				// sendNotification("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
				// sendNotification("Deleted messages on server: " +
				// extras.toString());
				// If it's a regular GCM message, do some work.
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
				// This loop represents the service doing some work.
				processBundleMessage(extras);
				PushMessages.notify(this, extras);
			}
		}
		if (extras != null) {
			Log.d(TAG, "Received: " + extras.toString() + " Message type: " + messageType);
		} else {
			Log.d(TAG, "Received null message!");
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void processBundleMessage(final Bundle data) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				if (data.getString("type").equals("message")) {
					String message = decode(data.getString("message"), data.getString("enc"));
					if(chatsTable.insert(data.getString("contact"), message, apply_offset(Long.parseLong(data.getString("time"))), data.getString("_id"), Chat.STATUS_RECV_FROM_CONTACT, data.getString("display"), data.getString("uid")) == null)
					{
						return;
					}

					Bundle b = new Bundle(data);
					b.putString("message", message);
					b.putInt("key", BUNDLE_KEY_CHAT);
					b.putString("time", String.valueOf(apply_offset(Long.parseLong(b.getString("time")))));
					b.putString("status", String.valueOf(Chat.STATUS_RECV_FROM_CONTACT));
					__sendMessage(b, new Executor<Boolean>() {

						@Override
						public void result(Boolean result) {
							// TODO Auto-generated method stub
							if (!result) {
								generate_notification(true);
							}
						}
					});

					send_deliver(data.getString("contact"), data.getString("deliver"));

				} else if (data.getString("type").equals("deliver")) {
					update_chat_status(data.getString("_id"), Chat.STATUS_DELIVER_TO_CONTACT);
				} else if (data.getString("type").equals("presence")) {
					if (Utils.isGroup(data.getString("username"))) {
						request_presence(data.getString("username"));
					} else {

						Bundle b = new Bundle(data);
						b.putInt("key", BUNDLE_KEY_PRESENCE);
						__sendMessage(b);
						Presence _p = Presence.parse(b);
						contactsTable.update_info(data.getString("username"), _p.name, _p.avatar, _p.presence, _p.lastseen, _p.status);
					}

				} else if (data.getString("type").equals("group")) {
					contactsTable.insert(data.getString("id"), data.getString("id"), data.getString("title"), data.getString("avatar"));
				}
			}
		}).start();
	}

	public void request_presence(final String username) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				if (ensureLogin()) {

					Presence _p = API.get_presence(username);
					if (_p != null) {
						_p.lastseen = apply_offset(_p.lastseen);
						contactsTable.update_info(username, _p.name, _p.avatar, _p.presence, _p.lastseen, _p.status);
						Bundle b = new Bundle(_p.build());
						b.putInt("key", BUNDLE_KEY_PRESENCE);
						b.putString("username", username);
						__sendMessage(b);
					}
				}
			}
		}).start();
	}
	public void set_presence(final String presence) {
		set_presence(presence, "");
	}
	public void set_presence(final String presence, final String to) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (ensureLogin())
					API.set_presence(presence, to);
			}
		}).start();
	}

	public long apply_offset(long time) {
		return ((time + time_offset) * 1000);
	}

	public void read_chats(String username) {
		if (chatsTable.mark_read(username)) {
			generate_notification(false);
			Bundle b = new Bundle();
			b.putInt("key", BUNDLE_KEY_CHAT);
			__sendMessage(b);
		}
	}
	private long lastNotification = 0;
	private void generate_notification(boolean new_) {
		if(System.currentTimeMillis() - lastNotification < 1500) return;
		lastNotification = System.currentTimeMillis();
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, ContactListActivity.class), 0);

		int unread = chatsTable.getUnread();
		if (unread > 0) {

			int contacts = chatsTable.getUnreadUsers();
			List<Chat> chats = chatsTable.getUnreadChats();
			String msg = "";
			String last = "";
			for (Chat chat : chats) {
				if (!ConfigManager.isSilentUser(this, chat.contact)) {
					last = chat.display + ": " + chat.getPreviewMessage(this);
					msg += last + "\n";
				}
			}
			if (!msg.equals("")) {

				NotificationCompat.Builder b = new NotificationCompat.Builder(this);

				b.setContentTitle("IMParty");
				b.setContentText("Nuevos mensajes");
				b.setTicker(last);
				b.setSubText(unread + " mensajes sin leer de " + contacts + " usuarios");
				b.setSmallIcon(com.manus.imparty2.R.drawable.ic_notification);
				b.setContentIntent(contentIntent);
				b.setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
				if (new_) {
					b.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
				}
				Notification noti = b.build();
				((NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE)).notify(100, noti);

			}
		} else {
			((NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(100);

		}
	}

	public void clearContacts(Account acc) {
		// TODO Auto-generated method stub

	}

	public void delete_chats(String uid) {
		chatsTable.delete(uid);
	}

	public void delete_chat(String _id) {
		// TODO Auto-generated method stub
		chatsTable.delete_id(_id);

	}
	public Presence getPresence(String uid) {
		// TODO Auto-generated method stub
		Contact c = contactsTable.query(uid);
		Presence _p = new Presence();
		_p.avatar = c.avatar;
		_p.name = c.display_name;
		_p.presence = c.presence;
		_p.lastseen = c.lastseen;
		return _p;
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		Log.d(TAG, "handle intent!");
	}

	private void sync_recv() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				List<Chat> chats = API.sync_messages();
                boolean notify = false;
				for (Chat chat : chats) {
					chat.time = apply_offset(chat.time);
					if(chatsTable.insert_incoming(chat) != null)
                    {
                        notify = true;
                    }
				}
				API.deliver(chats);

				generate_notification(notify);
				Bundle b = new Bundle();
				b.putInt("key", BUNDLE_KEY_CHAT);
				__sendMessage(b);

			}
		}).start();
	}

	@Override
	public void onPush(String arg0) {
		com.manus.imparty2.core.Log.logger("onPush: " + arg0);
		Bundle b = new Bundle();
		try {
			JSONObject obj = new JSONObject(arg0);
			Iterator it = obj.keys();
			while(it.hasNext())
			{
				String key = (String) it.next();
				b.putString(key, obj.getString(key));
				
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		onMessage(b);
		PushMessages.notify(this, b);
		
	}

	@Override
	public void onLog(String data) {
		// TODO Auto-generated method stub
		Log.d("JPush", data);
		
	}

	public boolean isPhoneContact(String username) {
		// TODO Auto-generated method stub
		return false;
	}

	public void editProfileName(final String name)
	{
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(API.update_profile_name(name))
				{
					loginInfo.public_name = name;
					Bundle b = new Bundle();
					b.putInt("key", BUNDLE_KEY_LOGIN);
					b.putBoolean("result", true);
					__sendMessage(b);
					
				}
			}
		}).start();
	}
	public void editProfileStatus(final String staus)
	{
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(API.update_profile_status(staus))
				{
					loginInfo.status = staus;
					Bundle b = new Bundle();
					b.putInt("key", BUNDLE_KEY_LOGIN);
					b.putBoolean("result", true);
					__sendMessage(b);
					
				}
			}
		}).start();
	}
	public void editProfileAvatar(final String avatar)
	{
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(API.set_profile_image(avatar))
				{
					loginInfo.avatar = avatar;
					Bundle b = new Bundle();
					b.putInt("key", BUNDLE_KEY_LOGIN);
					b.putBoolean("result", true);
					__sendMessage(b);
					
				}
			}
		}).start();
	}
	

}
