package com.manus.imparty2;

import com.manus.imparty2.R.anim;
import com.manus.imparty2.R.layout;
import com.manus.imparty2.adapter.AccountFilterAdapter;
import com.manus.imparty2.adapter.ContactAdapter;
import com.manus.imparty2.core.Contact;
import com.manus.imparty2.service.MessagingService;
import com.manus.imparty2.service.MessagingService.MessageReceiver;

import android.os.Bundle;
import android.os.Messenger;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ActionMenuView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class PickContactActivity extends ActionBarActivity implements MessageReceiver, OnItemClickListener {

	private MenuItem refreshItem;
	ContactAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(MessagingService.getInstance() == null) 
		{
			finish();
			return;
		}
		setContentView(R.layout.activity_pick_contact);
		MessagingService.attachListenner(this);
		adapter = new ContactAdapter(this, R.layout.item_contact);
		setListAdapter(adapter);
		getListView().setOnItemClickListener(this);
	}

	private ListView getListView() {
		// TODO Auto-generated method stub
		return (ListView) findViewById(R.id.listView1);
	}

	private void setListAdapter(ContactAdapter adapter2) {
		// TODO Auto-generated method stub
		getListView().setAdapter(adapter2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pick_contact, menu);
		return true;
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
		if (item.getItemId() == R.id.action_refresh) {
			refreshItem = item;
			refresh();
			MessagingService.getInstance().mergeContacts();
			return true;
		}
		else if(item.getItemId() == R.id.action_accounts)
		{
			final View mainView = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_filter, null);
			((CheckBox) mainView.findViewById(R.id.checkBox1)).setChecked(!getSharedPreferences("imparty", 0).getBoolean("show_joined", true));
			ListView list = (ListView) mainView.findViewById(R.id.listView1);
			final AccountFilterAdapter adapter = new AccountFilterAdapter(this, R.layout.item_account_filter, MessagingService.getInstance().getAccounts());
			list.setAdapter(adapter);
			Builder b = new Builder(this);
			b.setView(mainView);
			b.setPositiveButton("OK", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					getSharedPreferences("imparty", 0).edit().putBoolean("show_joined", !((CheckBox) mainView.findViewById(R.id.checkBox1)).isChecked()).commit();
					PickContactActivity.this.adapter.set(MessagingService.getInstance().getAddressBook());
					PickContactActivity.this.adapter.notifyDataSetChanged();

				}
			});
			b.create().show();
		}

		return super.onOptionsItemSelected(item);
    }

    //
//	@Override
//	public boolean onMenuItemSelected(int featureId, MenuItem item) {
//
//	}

	private void refresh() {
		// TODO Auto-generated method stub
		/* Attach a rotating ImageView to the refresh item as an ActionView */
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ImageView iv = (ImageView) inflater.inflate(layout.refresh_action_view, null);

		Animation rotation = AnimationUtils.loadAnimation(this, anim.clockwise_refresh);
		rotation.setRepeatCount(Animation.INFINITE);
		iv.startAnimation(rotation);

        MenuItemCompat.setActionView(refreshItem, iv);
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		MessagingService.removeListenner(this);
	}

	@Override
	public boolean onMessage(Bundle data) {
		// TODO Auto-generated method stub
		if (data.getInt("key") == MessagingService.BUNDLE_KEY_SERVICE_BOOT) {
			adapter.set(MessagingService.getInstance().getAddressBook());
			adapter.notifyDataSetChanged();
			return true;
		} else if (data.getInt("key") == MessagingService.BUNDLE_KEY_MERGE) {
			adapter.set(MessagingService.getInstance().getAddressBook());
			adapter.notifyDataSetChanged();
			if(data.getBoolean("finish", false))
			{
				
				if (refreshItem != null && refreshItem.getActionView() != null) {

                    View av = MenuItemCompat.getActionView(refreshItem);
                    av.clearAnimation();
					MenuItemCompat.setActionView(refreshItem, null);
				}
				Toast.makeText(this, "Actualizacion finalizada!", Toast.LENGTH_SHORT).show();
			}
			return true;
		}
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		Contact contact = (Contact) getListAdapter().getItem(arg2);
		if(contact.joined == 1)
		{
			Intent result = new Intent();
			result.putExtra("uid", contact.uid);
			setResult(RESULT_OK, result);
			finish();
			
		}
	}

	private ListAdapter getListAdapter() {
		// TODO Auto-generated method stub
		return getListView().getAdapter();
	}

}
