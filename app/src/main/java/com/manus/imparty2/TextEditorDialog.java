package com.manus.imparty2;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class TextEditorDialog implements TextWatcher {

	public interface OnTextListenner
	{
		public void onText(String text);
	}
	TextView counter;
	EditText editor;
	int maxChars = -1;
	String message;
	AlertDialog d=null;
	private int defaultColor = 0;
	public void setMaxChars(int chars)
	{
		this.maxChars = chars;
	}
	
	public void setDefaultMessage(String message)
	{
		this.message = message;
	}
	void show(Context context, final OnTextListenner listenner)
	{
		View v = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.activity_text_editor, null);

		editor = (EditText) v.findViewById(R.id.input);
		counter = (TextView) v.findViewById(R.id.counter);
		editor.setText(message);
		
		Builder b = new Builder(context);
		b.setView(v);
		b.setPositiveButton(android.R.string.ok, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if(editor.getText().length() > 0)
				{
					if(maxChars == -1 || (maxChars > 0 && maxChars >= editor.getText().length()))
					{
						if(listenner != null)
						{
							listenner.onText(editor.getText().toString());
						}
					}
				}
			}
		});
		b.setNegativeButton(android.R.string.cancel, null);
		d = b.create();
		defaultColor = counter.getTextColors().getDefaultColor();
		d.show();
		editor.addTextChangedListener(this);
		onChange();
	}
	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		onChange();
	}
	
	void onChange()
	{
		if(maxChars > 0)
		{
			int n = maxChars - editor.getText().length();
			if(n < 0)
			{
				counter.setTextColor(0xffff0000);
				d.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
			}
			else
			{
				counter.setTextColor(defaultColor);
				d.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
			}
			counter.setText(String.valueOf(n));
		}
		else
		{
			counter.setText("");
		}
	}

}
