package com.manus.imparty2;

import com.manus.imparty2.client.API;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CreateGroupActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_group);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_group, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getItemId() == R.id.action_create)
		{
			final ProgressDialog d = new ProgressDialog(this);
			d.setMessage("...");
			d.show();
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					String title = ((EditText) findViewById(R.id.editText1)).getText().toString();
					final boolean result = (API.create_group(title, "test"));
					runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							d.cancel();
							if(!result)
							{
								Toast.makeText(CreateGroupActivity.this, "Error", 0).show();
							}
							else
							{
								finish();
							}
						}
					});
				}
			}).start();
		}
		return super.onOptionsItemSelected(item);
	}

}
