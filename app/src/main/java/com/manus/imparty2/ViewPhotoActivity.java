package com.manus.imparty2;

import com.manus.imparty2.core.ImageLoader;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

public class ViewPhotoActivity extends Activity {

	ImageLoader loader = new ImageLoader();
	String uid;
	String avatar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_photo);
		avatar = getIntent().getStringExtra("avatar");
		loader.setImage(avatar, (ImageView) findViewById(R.id.imageView1));
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		if(getIntent().getBooleanExtra("changeable", false))
		{
			getMenuInflater().inflate(R.menu.view_photo, menu);
		}
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getItemId() == R.id.action_edit)
		{
			Intent i = new Intent(this, ChangePictureActivity.class);
			i.putExtras(getIntent());
			startActivity(i);
		}
		return super.onOptionsItemSelected(item);
	}

}
