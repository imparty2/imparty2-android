package com.manus.imparty2;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.Base64;
import android.webkit.MimeTypeMap;

import com.manus.imparty2.client.Presence;
import com.manus.imparty2.core.ImageLoader.ImageCache;
import com.manus.imparty2.core.Photos;
import com.manus.imparty2.login.FacebookLoginActivity;
import com.manus.imparty2.login.GoogleLoginActivity;
import com.manus.imparty2.login.PhoneNumberActivity;
import com.manus.imparty2.login.TwitterLoginActivity;

public class Utils {

	private static final long K = 1024;
	private static final long M = K * K;
	private static final long G = M * K;
	private static final long T = G * K;

	public static String convertToStringRepresentation(final long value) {
		final long[] dividers = new long[]{T, G, M, K, 1};
		final String[] units = new String[]{"TB", "GB", "MB", "KB", "B"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		if (value < 1) {
			return "0 B"; //$NON-NLS-1$
		}
		String result = null;
		for (int i = 0; i < dividers.length; i++) {
			final long divider = dividers[i];
			if (value >= divider) {
				result = format(value, divider, units[i]);
				break;
			} else {
				// FIXME
			}
		}
		return result;
	}
	private static String format(final long value, final long divider, final String unit) {
		final double result = divider > 1 ? (double) value / (double) divider : (double) value;
		return new DecimalFormat("#,##0.#").format(result) + " " + unit; //$NON-NLS-1$ //$NON-NLS-2$
	}
	public static String md5_file(File file) {
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(file);
			byte[] buffer = new byte[1024];
			MessageDigest digest = MessageDigest.getInstance("MD5");
			int numRead = 0;
			while (numRead != -1) {
				numRead = inputStream.read(buffer);
				if (numRead > 0)
					digest.update(buffer, 0, numRead);
			}
			byte[] md5Bytes = digest.digest();
			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < md5Bytes.length; i++) {
				String hex = Integer.toHexString(0xFF & md5Bytes[i]);
				if (hex.length() == 1)
					hex = "0" + hex;
				hexString.append(hex);
			}
			// while (hexString.length() != 32)
			// hexString.insert(0, "0");
			return hexString.toString();
		} catch (Exception e) {
			return null;
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (Exception e) {
				}
			}
		}
	}
	public static String md5(String s) {
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1)
					hex = "0" + hex;
				hexString.append(hex);
			}
			// while (hexString.length() != 32)
			// hexString.insert(0, "0");
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static Intent build_messages_activity(Context context, String username) {
		Intent i = new Intent(context, MessagesActivity.class);
		i.putExtra("username", username);
		return i;
	}

	public static Intent InstallAPK(String file) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(new File(file)), "application/vnd.android.package-archive"); //$NON-NLS-1$
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		return intent;
	}

	public static String get_image_preview_base64(Bitmap bmp) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
		byte[] b = baos.toByteArray();
		String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
		if (imageEncoded.length() > 1024 * 2)
			return "";
		return imageEncoded;
	}

	public static String get_image_preview_base64(String file) {
		Bitmap bmp = Photos.getSampleSize(file, 32);
		return get_image_preview_base64(bmp);

	}

	public static Bitmap get_base64_image_preview(String base64) {
		byte[] decodedByte = Base64.decode(base64, 0);
		return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
	}

	public static Bitmap get_preview_video_image(String file) {
		Bitmap bmp = ImageCache.load(file);
		if (bmp != null)
			return bmp;
		MediaMetadataRetriever retriever = new MediaMetadataRetriever();
		try {
			// retriever.setMode(MediaMetadataRetriever.MODE_CAPTURE_FRAME_ONLY);
			retriever.setDataSource(file);
			bmp = retriever.getFrameAtTime();
			ImageCache.save(bmp, file);
			return bmp;
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
		} catch (RuntimeException ex) {
			ex.printStackTrace();
		} finally {
			try {
				retriever.release();
			} catch (RuntimeException ex) {
			}
		}
		return null;
	}
	public static String get_preview_video_image_base64(String file) {
		Bitmap b = get_preview_video_image(file);
		return get_image_preview_base64(Photos.redimensionateBitmap(b, 64));
	}

	public static String getElapsedTime(long time) {
		int sec = (int) (time / 1000);
		int mins = sec / 60;
		sec = sec % 60;
		return (mins > 9 ? mins : "0" + mins) + ":" + (sec > 9 ? sec : "0" + sec);
	}
	public static String getPresenceString(Context context, String presence) {
		if(presence == null) return "";
		if (presence.equals("online")) {
			return "En linea";
		} else if (presence.equals("writting")) {
			return "Escribiendo...";
		} else if (presence.equals("recording")) {
			return "Grabando nota de audio...";
		}
		return "";
	}

	public static String getPresenceString(Context context, Presence p) {
		String str = getPresenceString(context, p.presence);
		if (str.equals("")) {
			Date d = new Date(p.lastseen);
			String date = DateFormat.getDateFormat(context).format(d);
			if (date.equals(DateFormat.getDateFormat(context).format(System.currentTimeMillis()))) {
				date = "ult. vez hoy a las";
			}
			return (date + " " + DateFormat.getTimeFormat(context).format(new Date(p.lastseen)));
		}
		return str;
	}

	public static boolean isGroup(String username) {
		return username != null && username.contains("@g.imparty");
	}

	public static void show_add_account_dialog(final Activity context, final int request) {
		Builder b = new Builder(context);
		b.setTitle("Cuenta");
		b.setItems(R.array.providers, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if (which == 1) {
					context.startActivityForResult(new Intent(context, GoogleLoginActivity.class).putExtra("no_user", true), request);
				} else if (which == 2) {
					context.startActivityForResult(new Intent(context, TwitterLoginActivity.class).putExtra("no_user", true), request);
				} else if (which == 3) {
					context.startActivityForResult(new Intent(context, FacebookLoginActivity.class).putExtra("no_user", true), request);
				} else if (which == 4) {
					context.startActivityForResult(new Intent(context, PhoneNumberActivity.class).putExtra("no_user", true), request);
				}
			}
		});
		b.create().show();
	}
	public static boolean isInteractivePressence(String presence) {
		// TODO Auto-generated method stub
		return presence != null && (presence.equals("writting") || presence.equals("recording"));
	}

	public static HashMap<String, String> decode_url(String url) {
		if (url.contains("?")) {
			url = url.substring(url.indexOf("?") + 1);
		}
		HashMap<String, String> data = new HashMap<String, String>();
		if (!url.contains("&"))
			return data;
		String[] values = url.split("&");
		for (int i = 0; i < values.length; i++) {
			String key = values[i];
			if (key.indexOf("=") >= 0) {
				String val = URLDecoder.decode(key.substring(key.indexOf("=") + 1));
				key = key.substring(0, key.indexOf("="));
				data.put(key, val);
			} else {
				data.put(key, "");
			}
		}
		return data;
	}

	public static boolean device_have_sim(Context context) {
		TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		int simState = telMgr.getSimState();
		switch (simState) {
			case TelephonyManager.SIM_STATE_READY :
				return true;
		}
		return false;
	}
	public static String doFingerprint(byte[] certificateBytes, String algorithm) throws Exception {
		MessageDigest md = MessageDigest.getInstance(algorithm);
		md.update(certificateBytes);
		byte[] digest = md.digest();

		String toRet = "";
		for (int i = 0; i < digest.length; i++) {
			if (i != 0)
				toRet += ":";
			int b = digest[i] & 0xff;
			String hex = Integer.toHexString(b);
			if (hex.length() == 1)
				toRet += "0";
			toRet += hex;
		}
		return toRet;
	}

	public static String getAppCert(Context context) {
		String sign = "";

		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
			Signature sig = info.signatures[0];
			sign = Utils.doFingerprint(sig.toByteArray(), "MD5");
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sign;
	}

	public static String getDeviceID(Context context) {
		return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
	}

	public static String getDeviceName()
	{
		return capitalize(Build.MANUFACTURER) + " " + capitalize(Build.MODEL);
	}

	public static String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}
	public static String getMimeType(String url)
	{
	    String type = null;
	    String extension = MimeTypeMap.getFileExtensionFromUrl(url);
	    if (extension != null) {
	        MimeTypeMap mime = MimeTypeMap.getSingleton();
	        type = mime.getMimeTypeFromExtension(extension);
	    }
	    return type;
	}
}
