package com.manus.imparty2;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.manus.imparty2.TextEditorDialog.OnTextListenner;
import com.manus.imparty2.adapter.ContactAdapter;
import com.manus.imparty2.client.API;
import com.manus.imparty2.client.Presence;
import com.manus.imparty2.client.ServerConfig;
import com.manus.imparty2.core.Contact;
import com.manus.imparty2.core.ImageLoader;
import com.manus.imparty2.service.MessagingService;
import com.manus.imparty2.service.MessagingService.MessageReceiver;
import com.manus.imparty2.view.NonScrollableListView;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

public class ChatInfoActivity extends ActionBarActivity implements OnClickListener, MessageReceiver {
	ImageLoader loader = new ImageLoader();
	String userid;
	String avatar;
	String title;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat_info);

		// Show the Up button in the action bar.
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		userid = getIntent().getStringExtra("username");
		Contact c = MessagingService.getInstance().getContact(userid);
		if(c == null)
		{
			finish();
			Toast.makeText(this, "No sincronizado!", Toast.LENGTH_SHORT).show();
			return;
		}
		avatar = c.avatar;
		Presence p = MessagingService.getInstance().getPresence(userid);
		title = p.name; 
		setPresence(p);
		if (Utils.isGroup(userid)) {
			findViewById(R.id.group_controls).setVisibility(View.VISIBLE);
			((TextView) findViewById(R.id.textView2)).setText("Grupo");
			findViewById(R.id.status_layout).setVisibility(View.GONE);
		} else {
			findViewById(R.id.group_controls).setVisibility(View.GONE);
			findViewById(R.id.status_layout).setVisibility(View.VISIBLE);
			
			((TextView) findViewById(R.id.textView5)).setText(c.status);
			
		}
		((ImageView) findViewById(R.id.imageView1)).setOnClickListener(this);
		((Button) findViewById(R.id.button1)).setOnClickListener(this);
		MessagingService.attachListenner(this);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		MessagingService.removeListenner(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.chat_info, menu);
		if (Utils.isGroup(getIntent().getStringExtra("username"))) {
            getMenuInflater().inflate(R.menu.chat_info_group, menu);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
			case android.R.id.home :
				finish();
				return true;
			case R.id.action_edit :
				TextEditorDialog d = new TextEditorDialog();
				d.setMaxChars(32);
				d.setDefaultMessage(title);
				d.show(this, new OnTextListenner() {

					@Override
					public void onText(final String text) {
						new Thread(new Runnable() {
							
							@Override
							public void run() {
								API.update_group_status(userid, text);
							}
						}).start();
					}
				});
				return true;
			case R.id.action_add:
				startActivityForResult(new Intent(this, PickContactActivity.class), 1);
				
				return true;
		}
		return false;

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.button1) {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					API.remove_participant(userid, "");
				}
			}).start();
		} else if (v.getId() == R.id.imageView1) {
			Intent i = new Intent(this, ViewPhotoActivity.class);
			i.putExtra("avatar", avatar);
			i.putExtra("where", userid);
			i.putExtra("changeable", Utils.isGroup(userid));
			startActivity(i);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1 && resultCode == RESULT_OK) {
			final ProgressDialog d = new ProgressDialog(this);
			d.setMessage("...");
			d.show();
			new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					final boolean result = API.add_participant(userid, data.getStringExtra("uid"));

					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							d.cancel();
							if (!result) {
								Toast.makeText(ChatInfoActivity.this, "Error", Toast.LENGTH_SHORT).show();
							}
						}
					});
				}
			}).start();
		}
	}

	@Override
	public boolean onMessage(Bundle data) {
		// TODO Auto-generated method stub
		if (data.getInt("key") == MessagingService.BUNDLE_KEY_PRESENCE) {
			if (data.getString("username").equals(userid)) {
				setPresence(Presence.parse(data));
			}
		}
		return false;
	}
	public void setPresence(Presence p) {
		if (Utils.isGroup(userid)) {

			int count = 0;
			List<Contact> list = new ArrayList<Contact>();
			try {
				JSONArray arr = new JSONArray(p.presence);
				count = arr.length();
				for (int i = 0; i < arr.length(); i++) {
					JSONObject obj = arr.getJSONObject(i);
					boolean admin = obj.getBoolean("admin");
					Contact _c = new Contact();
					_c.avatar = obj.getString("avatar");
					_c.uid = obj.getString("uid");
					_c.display_name = obj.getString("name");
					_c.status = obj.optString("status", "Unset status");
					if(admin)
					{
						_c.other = "Admin";
					}
					list.add(_c);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ContactAdapter adapter = new ContactAdapter(this, R.layout.item_contact, list);
			((NonScrollableListView) findViewById(R.id.nonScrollableListView1)).setAdapter(adapter);
			((TextView) findViewById(R.id.textView4)).setText(count + " de " + ServerConfig.getInt(this, "max_group_participants"));

		} else {
			((TextView) findViewById(R.id.textView2)).setText(Utils.getPresenceString(this, p));
			((TextView) findViewById(R.id.textView5)).setText(p.status);
		}
		avatar = p.avatar;
		loader.setImage(avatar, (ImageView) findViewById(R.id.imageView1));
		((TextView) findViewById(R.id.textView1)).setText(p.name);
	}
}
