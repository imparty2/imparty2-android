package com.manus.imparty2.login;

import java.util.HashMap;

import com.manus.imparty2.Constants;
import com.manus.imparty2.R;
import com.manus.imparty2.R.layout;
import com.manus.imparty2.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class TwitterLoginActivity extends GenericLogin {


	@Override
	String getLoginURL() {
		// TODO Auto-generated method stub
		return Constants.LOGIN_TWITTER;
	}

	@Override
	void onLogin(HashMap<String, String> response) {
		// TODO Auto-generated method stub
		android.webkit.CookieManager.getInstance().removeSessionCookie();
	}


}
