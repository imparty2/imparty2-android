package com.manus.imparty2.login;

import java.net.URLDecoder;
import java.util.HashMap;

import com.manus.imparty2.Utils;
import com.manus.imparty2.R.anim;
import com.manus.imparty2.R.layout;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebIconDatabase;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

public abstract class GenericLogin extends ActionBarActivity {


	abstract String getLoginURL();
	abstract void onLogin(HashMap<String, String> response);
	private boolean loading = false;
	private WebView web;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		WebIconDatabase.getInstance().open(getDir("icons", MODE_PRIVATE).getPath());
		
		WebViewClient client = new WebViewClient() {
			
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                
                try {
                	loading = false;
                	invalidateOptionsMenu();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("GenericLogin", "URL: " + url);
                if(url.contains("flogin.php"))
                {
                    String response = url;
                    if(response.indexOf("?") >= 0)
                    {
                    	process_login(Utils.decode_url(response));
                    }
                    else
                    {
                    	showDialog(1);
                    }
                }
            }
            
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

            	loading = true;
            	getSupportActionBar().setIcon(new BitmapDrawable(web.getFavicon()));
            	getSupportActionBar().setSubtitle(url);
            	invalidateOptionsMenu();
            }
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            	// TODO Auto-generated method stub
            	handler.proceed();
            }
            
           

        };
		web = new WebView(this);
		web.setWebChromeClient(new WebChromeClient() 
        {
			@Override
			public void onReceivedIcon(WebView view, Bitmap icon) {
				// TODO Auto-generated method stub
				super.onReceivedIcon(view, icon);
				
				getSupportActionBar().setIcon(new BitmapDrawable(addWhiteBorder(icon, 5)));
            	
			}
        });
		setContentView(web);
		web.getSettings().setJavaScriptEnabled(true);
		web.setWebViewClient(client);
		if(getIntent().hasExtra("no_user"))
		{
			web.loadUrl(getLoginURL() + "?mode=no_user");
		}
		else
		{
			web.loadUrl(getLoginURL());
		}
	}
	private Bitmap addWhiteBorder(Bitmap bmp, int borderSize) {
	    Bitmap bmpWithBorder = Bitmap.createBitmap(bmp.getWidth() + borderSize * 2, bmp.getHeight() + borderSize * 2, bmp.getConfig());
	    Canvas canvas = new Canvas(bmpWithBorder);
	    canvas.drawColor(Color.WHITE);
	    canvas.drawBitmap(bmp, borderSize, borderSize, null);
	    return bmpWithBorder;
	}
	
    @Override
    protected Dialog onCreateDialog(int id) {
    	if(id == 0)
    	{
    		
	        ProgressDialog p = new ProgressDialog(this);
	        p.setIndeterminate(true);
	        p.setMessage("Cargando...");
	        return p;
    	}
    	else if(id == 1)
    	{
    		Builder b = new Builder(this);
    		b.setMessage("Se ha producido un error!");
    		b.setPositiveButton(android.R.string.ok, null);
    		return b.create();
    	}
    	
    	return super.onCreateDialog(id);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	// TODO Auto-generated method stub
    	if(loading)
    	{
    		
	    	MenuItem refreshItem = menu.add("Loading...");
            MenuItemCompat.setShowAsAction(refreshItem,MenuItem.SHOW_AS_ACTION_ALWAYS );
	    	ProgressBar pb = new ProgressBar(this);
	    	pb.setIndeterminate(true);
            MenuItemCompat.setActionView(refreshItem, pb);
    	}
		return true;
    }

    private void process_login(HashMap<String, String> data)
    {
    	if(data.get("result").equals("ok"))
    	{
        	onLogin(data);
	    	Intent result = new Intent();
	    	result.putExtra("provider", data.get("provider"));
	    	result.putExtra("username", data.get("username"));
	    	result.putExtra("hash", data.get("hash"));
	    	result.putExtra("expires", data.get("expires"));
	    	setResult(RESULT_OK, result);
	    	finish();
    	}
    	else
    	{
    		setResult(RESULT_FIRST_USER + 1);
    		finish();
    	}
    }
	
	
	
}
