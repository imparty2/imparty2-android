package com.manus.imparty2.login;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.manus.imparty2.R;
import com.manus.imparty2.Utils;
import com.manus.imparty2.client.API;
import com.manus.imparty2.client.SMSResponse;
import com.manus.imparty2.core.ISO2Phone;
import com.manus.imparty2.core.Phones;

public class PhoneNumberActivity extends Activity implements OnItemSelectedListener, OnClickListener {

	public static final String BROADCAST_SMS_VERIFICATION = "com.manus.imparty2.SMS_VERIFICATION";
	public static final boolean DEBUG = true;
	private boolean unregister = false;
	public PhoneNumberActivity() {
		// TODO Auto-generated constructor stub
	}

	public static final int MAX_TIME = (1000 * 60 * 2);

	BroadcastReceiver sms_verification = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			
			final String code = intent.getStringExtra("code");
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					verify(code);
				}
			});

		}
	};
	private void verify(final String code)
	{
		if(!PreferenceManager.getDefaultSharedPreferences(PhoneNumberActivity.this).getBoolean("registrated_phone", false))
		{
			final ProgressDialog d = new ProgressDialog(PhoneNumberActivity.this);
			d.setTitle("Mensaje recibido");
			d.setMessage("Se esta generando un codigo de autoidentificacion...");
			d.show();
			new Thread(new Runnable() {
	
				@Override
				public void run() {
					// TODO Auto-generated method stub
					String extras = PreferenceManager.getDefaultSharedPreferences(PhoneNumberActivity.this).getString("registration_extra", "");
	
					try {
						String res = API.verify_sms(code, new JSONObject(extras));
	
						JSONObject obj = new JSONObject(res);
						if (obj.getBoolean("result")) {
							HashMap<String, String> data = new HashMap<String, String>();
							data = Utils.decode_url(obj.getString("auth"));
							Intent result = new Intent();
							result.putExtra("provider", data.get("provider"));
							result.putExtra("username", data.get("username"));
							result.putExtra("hash", data.get("hash"));
							result.putExtra("expires", data.get("expires"));
							unregister = false;
							PreferenceManager.getDefaultSharedPreferences(PhoneNumberActivity.this).edit().putBoolean("registrated_phone", true).commit();
							unregisterReceiver(sms_verification);
							set_time_limit(0);
							
							PhoneNumberActivity.this.setResult(RESULT_OK, result);
							PhoneNumberActivity.this.finish();
	
							return;
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							d.hide();
							Builder b = new Builder(PhoneNumberActivity.this);
							b.setMessage("Se ha producido un error!");
							b.create().show();
						}
					});
	
				}
			}).start();
		}
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_phone_number);

		TelephonyManager tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		String prefix = ISO2Phone.getPhone(tel.getNetworkCountryIso());
		int index = Phones.findMCC(prefix);
		ArrayAdapter adapter = new ArrayAdapter(this, R.layout.spiner_item, Phones.PHONES_READDABLE);
		((Spinner) findViewById(R.id.Spinner1)).setAdapter(adapter);
		((Spinner) findViewById(R.id.Spinner1)).setSelection(index);
		((Spinner) findViewById(R.id.Spinner1)).setOnItemSelectedListener(this);
		((Button) findViewById(R.id.button1)).setOnClickListener(this);
		((Button) findViewById(R.id.button3)).setOnClickListener(this);
		display_receiver();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.phone_number, menu);
		return true;
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		((TextView) findViewById(R.id.textView4)).setText(Phones.PHONE_PREFIX[arg2]);

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.button1) {

			// TODO Auto-generated method stub
			final String cc = ((TextView) findViewById(R.id.textView4)).getText().toString();
			final String phone = ((EditText) findViewById(R.id.editText1)).getText().toString();
			Builder b = new Builder(this);
			b.setTitle("Enviar SMS");
			b.setMessage("Se enviara un SMS de confirmacion a: \n\n+" + cc + phone);
			b.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					PreferenceManager.getDefaultSharedPreferences(PhoneNumberActivity.this).edit().putBoolean("registrated_phone", false).commit();
					
					final ProgressDialog p = new ProgressDialog(PhoneNumberActivity.this);
					p.setMessage("...");
					p.show();

					new Thread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							final SMSResponse response = API.request_sms(cc, phone);
							runOnUiThread(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
									p.cancel();
									post_result(response);
								}
							});
						}
					}).start();
				}
			});
			b.setNegativeButton(android.R.string.cancel, null);
			b.create().show();
		}
		else if(v.getId() == R.id.button3)
		{
			String code = ((EditText) findViewById(R.id.editText2)).getText().toString();
			if(code.length() > 0)
			{
				verify(code);
			}
		}
	}

	void post_result(SMSResponse response) {
		if (response != null && response.result) {
			PreferenceManager.getDefaultSharedPreferences(this).edit().putString("registration_extra", response.extras.toString()).commit();
			set_time_limit(System.currentTimeMillis() + MAX_TIME);
			display_receiver();
		} else {
			Builder b = new Builder(this);
			b.setMessage("Se ha producido un problema al enviar el mensaje");
			b.setTitle("Validacion SMS");
			b.create().show();
		}
	}
	void setup_updater() {
		final TimerTask updater = new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (!updater()) {
					cancel();
				}

			}
		};
		new Timer().schedule(updater, 0, 1000);
	}
	boolean updater() {
		final long when = get_remaining_time();
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				long remaining = (when - System.currentTimeMillis());
				if (remaining >= 0) {

					((TextView) findViewById(R.id.textView5)).setText(Utils.getElapsedTime(remaining));
					((ProgressBar) findViewById(R.id.progressBar1)).setMax(MAX_TIME);
					((ProgressBar) findViewById(R.id.progressBar1)).setProgress((int) remaining);

				} else {
					set_time_limit(0);
					display_receiver();
				}
			}
		});
		return (when > 0 && when > System.currentTimeMillis());
	}
	void display_receiver() {
		if (get_remaining_time() > 0 && get_remaining_time() > System.currentTimeMillis()) {
			findViewById(R.id.send_sms).setVisibility(View.GONE);
			findViewById(R.id.whait_sms).setVisibility(View.VISIBLE);
			setup_updater();
			updater();
			unregister = true;
			registerReceiver(sms_verification, new IntentFilter(BROADCAST_SMS_VERIFICATION));
		} else {

			findViewById(R.id.send_sms).setVisibility(View.VISIBLE);
			findViewById(R.id.whait_sms).setVisibility(View.GONE);
		}
	}
	private long get_remaining_time() {
		return PreferenceManager.getDefaultSharedPreferences(this).getLong("registration_expires", 0);
	}
	private void set_time_limit(long time) {
		PreferenceManager.getDefaultSharedPreferences(this).edit().putLong("registration_expires", time).commit();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if(unregister)
		{
			unregisterReceiver(sms_verification);
		}
	}

}
