package com.manus.imparty2.login;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import com.manus.imparty2.client.API;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsMessage;
import android.util.Log;

public class SMSReceiver extends BroadcastReceiver {

	public SMSReceiver() {
		// TODO Auto-generated constructor stub
	}

	private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
	// android.provider.Telephony.SMS_RECEIVED
	private static final String TAG = "SMSBroadcastReceiver";
	@Override
	public void onReceive(final Context context, Intent intent) {
		// TODO Auto-generated method stub

		Log.i(TAG, "Intent recieved: " + intent.getAction());
		if (intent.getAction().equals(SMS_RECEIVED)) {
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				Object[] pdus = (Object[]) bundle.get("pdus");
				final SmsMessage[] messages = new SmsMessage[pdus.length];
				for (int i = 0; i < pdus.length; i++) {
					messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
				}
				String data = "";
				for (int i = 0; i < messages.length; i++) {
					data = messages[i].getMessageBody();

					if (PreferenceManager.getDefaultSharedPreferences(context).getLong("registration_expires", 0) > 0) {
						String number = "";
						Pattern p = Pattern.compile("\\d+\\W");
						Matcher m = p.matcher(data);
						while (m.find()) {
							number += m.group();
						}//86973786
						number = number.replaceAll("\\D", "");
						Intent intent2 = new Intent(PhoneNumberActivity.BROADCAST_SMS_VERIFICATION);
						intent2.putExtra("code", number);
						context.sendBroadcast(intent2);
						abortBroadcast();
						//test2hola
						
					}

				}

			}
		}
	}
}
