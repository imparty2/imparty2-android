package com.manus.imparty2.fragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Messenger;
import android.provider.MediaStore;
import android.provider.MediaStore.Video.Media;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.text.Editable;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.google.android.gms.internal.ab;
import com.google.android.gms.internal.ad;
import com.manus.imparty2.ChatInfoActivity;
import com.manus.imparty2.ConfigManager;
import com.manus.imparty2.FileBrowserActivity;
import com.manus.imparty2.R;
import com.manus.imparty2.Utils;
import com.manus.imparty2.adapter.GenericAdapter;
import com.manus.imparty2.adapter.GenericChatAdapter;
import com.manus.imparty2.adapter.MessageAdapter;
import com.manus.imparty2.client.Presence;
import com.manus.imparty2.core.Chat;
import com.manus.imparty2.core.Contact;
import com.manus.imparty2.core.ImageLoader;
import com.manus.imparty2.core.MediaProvider;
import com.manus.imparty2.core.Photos;
import com.manus.imparty2.core.Recent;
import com.manus.imparty2.fragment.ContactListFragment.ContactLoader;
import com.manus.imparty2.service.MessagingService;
import com.manus.imparty2.service.MessagingService.MessageReceiver;

public abstract class BaseMessagesFragment<T extends Chat> extends Fragment implements MessageReceiver, OnClickListener {
	public static final String ARG_ITEM_ID = "username";
	String username;

	GenericChatAdapter<T> adapter;
	EditText editor;
	ListView list;
	boolean front = false;
	boolean recording = false;
	boolean recordCancel = false;
	ImageButton send;
	Contact contact;
	private static final int ACTIVITY_REQUEST_PICK_PHOTO = 1;
	private static final int ACTIVITY_REQUEST_TAKE_PHOTO = 2;
	private static final int ACTIVITY_REQUEST_PICK_VIDEO = 10;
	private static final int ACTIVITY_REQUEST_TAKE_VIDEO = 11;
	private static final int ACTIVITY_REQUEST_PICK_FILE = 20;

	public BaseMessagesFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey("username")) {
			this.username = getArguments().getString(ARG_ITEM_ID);
		}

		setHasOptionsMenu(true);
	}

	protected abstract GenericChatAdapter<T> getAdapter();
	private GenericChatAdapter<T> getInternalAdapter() {
		if (adapter == null) {
			adapter = getAdapter();
		}
		return adapter;
	}
	private ActionMode mActionMode;

	public abstract List<T> getChats(String username);
	public abstract T parse(Bundle data);
	public abstract void update(GenericAdapter<T> adapter, Bundle newState);
	public abstract void presenceChanged(Presence presence);
	public abstract void sendMessage(String username, String message);

	private void playSound(int id) {
		MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(), id);
		mediaPlayer.start();
	}
	private void handleSendImage(Uri _uri) {
		Bitmap bmp;
		try {
			bmp = Photos.getCorrectlyOrientedImage(getActivity(), _uri);

			File f = MediaProvider.get_image_file();
			FileOutputStream out = new FileOutputStream(f);
			bmp.compress(CompressFormat.JPEG, 100, out);
			out.close();
			MessagingService.getInstance().send_image(username, f.getAbsolutePath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getActivity(), "No se ha podido preparar la foto", Toast.LENGTH_SHORT).show();
		}
	}

	private File takePhotoFile;
	private File takeVideoFile;
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {

			if (requestCode == ACTIVITY_REQUEST_PICK_PHOTO) {
				handleSendImage(data.getData());

			} else if (requestCode == ACTIVITY_REQUEST_TAKE_PHOTO) {
				if (takePhotoFile.exists()) {
					Bitmap bmp = Photos.getSampleSize(takePhotoFile.getAbsolutePath(), 600);

					File f = MediaProvider.get_image_file();
					try {
						FileOutputStream out = new FileOutputStream(f);
						bmp.compress(CompressFormat.JPEG, 100, out);
						out.close();
						takePhotoFile.delete();

						MessagingService.getInstance().send_image(username, f.getAbsolutePath());

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			} else if (requestCode == ACTIVITY_REQUEST_TAKE_VIDEO) {
				if (takeVideoFile.exists()) {

					MediaPlayer mp = MediaPlayer.create(getActivity(), Uri.fromFile(takeVideoFile));
					int duration = mp.getDuration();
					mp.release();

					MessagingService.getInstance().send_video(username, takeVideoFile.getAbsolutePath(), duration);

				}
			}
			else if(requestCode == ACTIVITY_REQUEST_PICK_FILE)
			{
				MessagingService.getInstance().send_file(username, new File(data.getStringExtra("file")));
			}
		}

	}
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

		getActivity().getMenuInflater().inflate(R.menu.messages, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if (item.getItemId() == R.id.action_info) {

			startActivity(new Intent(getActivity(), ChatInfoActivity.class).putExtras(getArguments()));
			// overridePendingTransition(R.anim.rotate_in, R.anim.rotate_out);
			return true;
		}

		if (item.getItemId() == R.id.share_from_galery) {

			Intent pickIntent = new Intent();
			pickIntent.setType("image/*");
			pickIntent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(pickIntent, ACTIVITY_REQUEST_PICK_PHOTO);
		}
		if (item.getItemId() == R.id.share_from_camera) {
			Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			takePhotoFile = MediaProvider.get_image_file();
			takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(takePhotoFile));
			startActivityForResult(takePictureIntent, ACTIVITY_REQUEST_TAKE_PHOTO);
		}
		if (item.getItemId() == R.id.share_video_camera) {
			Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
			takeVideoFile = MediaProvider.get_video_file();
			ContentValues value = new ContentValues();
			value.put(MediaStore.Video.Media.TITLE, "IMParty Video");
			value.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
			value.put(MediaStore.Video.Media.DATA, takeVideoFile.getAbsolutePath());

			Uri videoUri = getActivity().getContentResolver().insert(Media.EXTERNAL_CONTENT_URI, value);
			takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
			takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
			startActivityForResult(takeVideoIntent, ACTIVITY_REQUEST_TAKE_VIDEO);
		} else if (item.getItemId() == R.id.action_silent) {
			Builder b = new Builder(getActivity());
			final CharSequence ops[] = new CharSequence[]{"1 Hora", "8 Horas", "1 dia", "1 semana"};
			b.setItems(ops, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					int time[] = new int[]{60 * 60 * 1000, 8 * 60 * 60 * 1000, 24 * 60 * 60 * 1000, 7 * 24 * 60 * 60 * 1000};
					long expire = time[which] + System.currentTimeMillis();

					ConfigManager.silentUser(getActivity(), username, expire);
					String date = DateFormat.getDateFormat(getActivity()).format(new Date(expire));
					String stime = DateFormat.getTimeFormat(getActivity()).format(new Date(expire));
					Toast.makeText(getActivity(), "Silenciado hasta: " + date + " " + stime, Toast.LENGTH_SHORT).show();
				}
			});
			b.setTitle("Silenciar");
			b.create().show();
		}
		else if(item.getItemId() == R.id.action_file)
		{
			startActivityForResult(new Intent(getActivity(), FileBrowserActivity.class), ACTIVITY_REQUEST_PICK_FILE);
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		front = false;
	}
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		front = true;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		MessagingService.attachListenner(this);
		switchRecordSend();
		stopRecord();
		getActivity().findViewById(R.id.recod_layout).setVisibility(View.GONE);

		Intent intent = getActivity().getIntent();
		String action = intent.getAction();
		String type = intent.getType();

		if (Intent.ACTION_SEND.equals(action) && type != null) {
			if ("text/plain".equals(type)) {
				// handleSendText((Uri)
				// intent.getParcelableExtra(Intent.EXTRA_STREAM)); // Handle
				// text being sent
				String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
				sendMessage(username, sharedText);
			} else if (type.startsWith("image/")) {
				handleSendImage((Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM)); // Handle
																						// single
																						// image
																						// being
																						// sent
			}
		} else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
			if (type.startsWith("image/")) {
				ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
				if (imageUris != null) {
					for (Uri uri : imageUris) {
						handleSendImage(uri);
					}
				}
			}
		} else {
			// Handle other intents, such as being started from the home screen
		}
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		MessagingService.removeListenner(this);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_messages, container, false);

		adapter = getInternalAdapter();
		list = ((ListView) rootView.findViewById(R.id.listView1));
		list.setAdapter(adapter);
		list.setDividerHeight(0);
		list.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
		list.setStackFromBottom(true);

		list.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (mActionMode != null) {
					// if already in action mode - do nothing
					return false;
				}
				adapter.setChecked(arg2, true);
                ((ActionBarActivity) getActivity()).startSupportActionMode(new ActionModeCallback());
				mActionMode.invalidate();
				return true;
			}
		});
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (mActionMode != null) {
					// if action mode, toggle checked state of item
					adapter.toggleChecked(arg2);
					mActionMode.invalidate();
				}
			}
		});
		send = ((ImageButton) rootView.findViewById(R.id.imageButton1));
		send.setOnClickListener(this);
		editor = ((EditText) rootView.findViewById(R.id.message_edit));
		editor.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if (actionId == EditorInfo.IME_ACTION_SEND || (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
					sendMessage();
				}
				return false;
			}
		});
		editor.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				switchRecordSend();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (!Utils.isGroup(username)) {
					if (editor.getText().length() == 1)
						MessagingService.getInstance().set_presence("writting", username);
					else if (editor.getText().length() == 0)
						MessagingService.getInstance().set_presence("online", username);

				}

			}
		});
		send.setOnTouchListener(new OnTouchListener() {
			private static final int CANCEL_DISTANCE = 200;
			float startX;
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if (editor.getText().length() == 0) {
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						recordCancel = false;
						startRecord();
						startX = event.getX();
					} else if (event.getAction() == MotionEvent.ACTION_UP)
						stopRecord();
					else if (event.getAction() == MotionEvent.ACTION_MOVE) {
						TextPaint paint = new TextPaint(((TextView) getActivity().findViewById(R.id.info_audio)).getPaint());
						int tw = (int) paint.measureText("Desliza para Cancelar");
						int cancel = getActivity().findViewById(R.id.info_audio).getWidth() - tw;
						float d = startX - event.getX();
						if (d > cancel) {
							recordCancel = true;
							getActivity().findViewById(R.id.info_audio).setPadding(0, 0, (int) cancel, 0);
							((TextView) getActivity().findViewById(R.id.info_audio)).setText("Suelta para cancelar");
						} else {
							// float o = d / cancel;
							// int w =
							// getActivity().findViewById(R.id.info_audio).getWidth();
							// int r = (int) (w * o);
							((TextView) getActivity().findViewById(R.id.info_audio)).setText("Desliza para Cancelar");
							getActivity().findViewById(R.id.info_audio).setPadding(0, 0, (int) d, 0);

							recordCancel = false;
						}
					}
				}
				return false;
			}
		});
		return rootView;
	}

	private void switchRecordSend() {
		if (editor.getText().length() == 0) {
			send.setImageResource(R.drawable.ic_action_record);
		} else {
			send.setImageResource(R.drawable.ic_action_send);
		}
	}

    public void setParticipants(String[] avatars)
    {
        ImageLoader loader = new ImageLoader();
        LinearLayout layout = (LinearLayout) getActivity().findViewById(R.id.participants);
        layout.removeAllViews();
        for (int i = 0; i < avatars.length; i++) {
            ImageView image = new ImageView(getActivity());
            image.setBackgroundResource(R.drawable.shadow);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    100, 100);
            Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.rigth_to_left);
            animation.setStartOffset(i * 500);
            image.startAnimation(animation);

            layoutParams.setMargins(10, 10, 10, 10);
            loader.setImage(avatars[i], image);

            layout.addView(image, layoutParams);

        }
    }

	@Override
	public boolean onMessage(Bundle data) {
		// TODO Auto-generated method stub
		if (data.getInt("key") == MessagingService.BUNDLE_KEY_SERVICE_BOOT) {
			contact = MessagingService.getInstance().getContact(username);
			if (contact != null) {
                ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(contact.display_name);
			} else {
                ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(username);
			}

			// List<Chat> chats =
			// MessagingService.getInstance().getChats(username);
			getInternalAdapter().set(getChats(username));
			MessagingService.getInstance().read_chats(username);
			MessagingService.getInstance().request_presence(username);
			return true;
		} else if (data.getInt("key") == MessagingService.BUNDLE_KEY_CHAT && data.containsKey("contact")) {
			if (data.getBoolean("refresh", false)) {
				getInternalAdapter().notifyDataSetChanged();
				return true;
			}
			int message_status = Integer.parseInt(data.getString("status"));
			if (data.getString("contact").equals(username) && message_status != Chat.STATUS_DELIVER_TO_CONTACT && message_status != Chat.STATUS_SEND_BY_ME && !data.containsKey("progress")) {
				getInternalAdapter().add(parse(data));

				if (front) {
					MessagingService.getInstance().read_chats(username);
					return true;
				}
			} else if (data.getString("contact").equals(username) && data.containsKey("_id")) {
				update(getInternalAdapter(), data);
				return true;
			}

		} else if (data.getInt("key") == MessagingService.BUNDLE_KEY_PRESENCE) {
			if (data.getString("username").equals(username)) {
				Presence p = Presence.parse(data);
				presenceChanged(p);
			}
		}
		return false;
	}
	private final class ActionModeCallback implements ActionMode.Callback {

		// " selected" string resource to update ActionBar text
		private String selected = "Mensaje";

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			adapter.enterMultiMode();
			mActionMode = mode;
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			// remove previous items
			menu.clear();
			final int checked = adapter.getCheckedItemCount();
			// update title with number of checked items
			mode.setTitle(checked + " " + this.selected);
			getActivity().getMenuInflater().inflate(R.menu.messages_edit, menu);
			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
				case R.id.action_remove :

					for (Integer i : adapter.getCheckedItems()) {
						MessagingService.getInstance().delete_chat(adapter.getItem(i)._id);
					}

					getInternalAdapter().set(getChats(username));
					mActionMode.finish();
					return true;
				case R.id.action_copy:
					String text = "";
					for (Integer i : adapter.getCheckedItems()) {
						text += adapter.getItem(i).getDisplayMessage() + "\n";
					}
					int sdk = android.os.Build.VERSION.SDK_INT;
					if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
					    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
					    clipboard.setText(text);
					} else {
					    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE); 
					    android.content.ClipData clip = android.content.ClipData.newPlainText("IMParty",text);
					    clipboard.setPrimaryClip(clip);
					}
					mActionMode.finish();
					return true;
				default :
					return false;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			adapter.exitMultiMode();
			// don't forget to remove it, because we are assuming that if it's
			// not null we are in ActionMode
			mActionMode = null;
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (editor.getText().length() > 0) {
			sendMessage();
		}
	}
	private void sendMessage() {
		if (editor.getText().length() > 0) {
			sendMessage(username, editor.getText().toString());
		}
		editor.setText("");

	}

	private void startRecord() {
		if (!recording) {

			playSound(R.raw.record_start);
			recording = true;
			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					File output = MediaProvider.get_audio_file();
					// TODO Auto-generated method stub
					MediaRecorder mRecorder = new MediaRecorder();
					mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
					mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
					mRecorder.setOutputFile(output.getAbsolutePath());
					mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

					try {
						mRecorder.prepare();
						mRecorder.start();
						long start = System.currentTimeMillis();
						if (!Utils.isGroup(username)) {
							MessagingService.getInstance().set_presence("recording", username);
						}
						while (recording) {
							updateDuration(start);
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						mRecorder.stop();
						if (!Utils.isGroup(username)) {
							MessagingService.getInstance().set_presence("online", username);
						}
						if (!recordCancel) {
							if (System.currentTimeMillis() - start > 500) {

								MediaPlayer mPlayer = new MediaPlayer();
								try {
									mPlayer.setDataSource(output.getAbsolutePath());
									mPlayer.prepare();

									MessagingService.getInstance().send_audio(username, output.getAbsolutePath(), mPlayer.getDuration());

									mPlayer.release();
									return;
								} catch (IOException e) {
									showErrorMessage("prepare() failed");
								}
							} else {
								showErrorMessage("Deje pulsado para grabar");
							}
						}
					} catch (Exception e) {
						showErrorMessage("Error durning record");
					}

					playSound(R.raw.record_stop);
				}
			}).start();
		} else {
			// Something bad was happed!
		}
	}

	private void stopRecord() {
		// TODO Auto-generated method stub
		getActivity().findViewById(R.id.recod_layout).setVisibility(View.GONE);
		getActivity().findViewById(R.id.message_edit).setVisibility(View.VISIBLE);

		recording = false;
	}

	private void showErrorMessage(final String message) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
			}
		});
	}
	public void updateDuration(long start) {
		long duration = System.currentTimeMillis() - start;

		setDuration(Utils.getElapsedTime(duration));

	}
	public void setDuration(final String dur) {

		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (getActivity().findViewById(R.id.recod_layout) != null) {

					getActivity().findViewById(R.id.recod_layout).setVisibility(View.VISIBLE);
					getActivity().findViewById(R.id.message_edit).setVisibility(View.GONE);
					// TODO Auto-generated method stub
					((TextView) getActivity().findViewById(R.id.record_time)).setText(dur);

				}
			}
		});
	}
}
