package com.manus.imparty2.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Outline;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Messenger;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.manus.imparty2.ConfigManager;
import com.manus.imparty2.R;
import com.manus.imparty2.adapter.ContactAdapter;
import com.manus.imparty2.adapter.RecentsAdapter;
import com.manus.imparty2.core.Contact;
import com.manus.imparty2.core.Recent;
import com.manus.imparty2.dummy.DummyContent;
import com.manus.imparty2.service.MessagingService;
import com.manus.imparty2.service.MessagingService.MessageReceiver;

/**
 * A list fragment representing a list of Items. This fragment also supports
 * tablet devices by allowing list items to be given an 'activated' state upon
 * selection. This helps indicate which item is currently being viewed in a
 * {@link MessagesFragment}.
 * <p>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class ContactListFragment extends ListFragment implements MessageReceiver {

	List<Recent> recents;
	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * activated item position. Only used on tablets.
	 */
	private static final String STATE_ACTIVATED_POSITION = "activated_position";

	/**
	 * The fragment's current callback object, which is notified of list item
	 * clicks.
	 */
	private Callbacks mCallbacks = sDummyCallbacks;

	/**
	 * The current activated item position. Only used on tablets.
	 */
	private int mActivatedPosition = ListView.INVALID_POSITION;

	/**
	 * A callback interface that all activities containing this fragment must
	 * implement. This mechanism allows activities to be notified of item
	 * selections.
	 */
	public interface Callbacks {
		/**
		 * Callback for when an item has been selected.
		 */
		public void onItemSelected(String username);
	}

	/**
	 * A dummy implementation of the {@link Callbacks} interface that does
	 * nothing. Used only when this fragment is not attached to an activity.
	 */
	private static Callbacks sDummyCallbacks = new Callbacks() {

		@Override
		public void onItemSelected(String username) {
			// TODO Auto-generated method stub

		}

	};
	RecentsAdapter adapter;
	// if ActoinMode is null - assume we are in normal mode
	private ActionMode mActionMode;
	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ContactListFragment() {
	}

	@Override
	public void onPause() {
		super.onPause();
		if (this.mActionMode != null) {
			this.mActionMode.finish();
		}
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		if (recents == null) {
			recents = new ArrayList<Recent>();
			setListShown(false);
		} else {
			setListShown(true);
		}
		adapter = (new RecentsAdapter(getActivity(), R.layout.item_recent, recents));
		setListAdapter(adapter);
		MessagingService.attachListenner(this);
		getListView().setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (mActionMode != null) {
					// if already in action mode - do nothing
					return false;
				}
				adapter.setChecked(arg2, true);
                ((ActionBarActivity) getActivity()).startSupportActionMode(new ActionModeCallback());
				mActionMode.invalidate();
				return true;
			}
		});

//
//        View addButton = getActivity().findViewById(R.id.add_button);
//        ViewOutlineProvider viewOutlineProvider = new ViewOutlineProvider() {
//            @Override
//            public void getOutline(View view, Outline outline) {
//                // Or read size directly from the view's width/height
//                int diameter = getResources().getDimensionPixelSize(R.dimen.diameter);
//                outline.setOval(0, 0, diameter, diameter);
//            }
//        };
//
////        addButton.setOutline(outline);
//        addButton.setClipToOutline(true);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);

	}
	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		MessagingService.removeListenner(this);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		// Restore the previously serialized activated item position.
		if (savedInstanceState != null && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException("Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();

		// Reset the active callbacks interface to the dummy implementation.
		mCallbacks = sDummyCallbacks;
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position, long id) {
		super.onListItemClick(listView, view, position, id);
		if (mActionMode == null) {

			// Notify the active callbacks interface (the activity, if the
			// fragment is attached to one) that an item has been selected.
			Recent r = ((Recent) getListAdapter().getItem(position));
			Contact c = r.contact;
			mCallbacks.onItemSelected(c.uid);
		} else {
			// if action mode, toggle checked state of item
			adapter.toggleChecked(position);
			mActionMode.invalidate();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			// Serialize and persist the activated item position.
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
	}

	/**
	 * Turns on activate-on-click mode. When this mode is on, list items will be
	 * given the 'activated' state when touched.
	 */
	public void setActivateOnItemClick(boolean activateOnItemClick) {
		// When setting CHOICE_MODE_SINGLE, ListView will automatically
		// give items the 'activated' state when touched.
		getListView().setChoiceMode(activateOnItemClick ? ListView.CHOICE_MODE_SINGLE : ListView.CHOICE_MODE_NONE);
	}

	private void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			getListView().setItemChecked(mActivatedPosition, false);
		} else {
			getListView().setItemChecked(position, true);
		}

		mActivatedPosition = position;
	}

	public class ContactLoader extends AsyncTask<Void, Void, List<Recent>> {

		@Override
		protected List<Recent> doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return MessagingService.getInstance().getRecents();
		}

		@Override
		protected void onPostExecute(List<Recent> result) {
			// TODO Auto-generated method stub
			if (isVisible()) {

				if (result != null) {
					ContactListFragment.this.recents = result;
					((RecentsAdapter) getListAdapter()).set(result);
					setEmptyText("Comienza a chatear para ver las conversaciones");
				} else {
					setEmptyText("No se han podido cargar los chats recientes");
				}
				setListShown(true);
			}
			super.onPostExecute(result);
		}

	}
	boolean hasBoot = false;
	@Override
	public boolean onMessage(Bundle data) {
		// TODO Auto-generated method stub
		if (data.getInt("key") == MessagingService.BUNDLE_KEY_SERVICE_BOOT || data.getInt("key") == MessagingService.BUNDLE_KEY_LOGIN || data.getInt("key") == MessagingService.BUNDLE_KEY_CHAT || data.getInt("key") == MessagingService.BUNDLE_KEY_PRESENCE) {
			if (ConfigManager.hasLogin(getActivity())) {
				new ContactLoader().execute((Void) null);
				if (data.getInt("key") == MessagingService.BUNDLE_KEY_SERVICE_BOOT) {
					MessagingService.getInstance().set_presence("online");
				}
			}
		}
		return false;
	}
	private final class ActionModeCallback implements ActionMode.Callback {

		// " selected" string resource to update ActionBar text
		private String selected = " Chat";

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			adapter.enterMultiMode();
			mActionMode = mode;
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			// remove previous items
			menu.clear();
			final int checked = adapter.getCheckedItemCount();
			// update title with number of checked items
			mode.setTitle(checked + " " + this.selected);
            ((ActionBarActivity)getActivity()).getMenuInflater().inflate(R.menu.contacts_edit, menu);
			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
				case R.id.action_remove :

					for (Integer i : adapter.getCheckedItems()) {
						MessagingService.getInstance().delete_chats(adapter.getItem(i).contact.uid);
					}
					new ContactLoader().execute((Void) null);
					mActionMode.finish();
					return true;
				default :
					return false;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			adapter.exitMultiMode();
			// don't forget to remove it, because we are assuming that if it's
			// not null we are in ActionMode
			mActionMode = null;
		}

	}
}
