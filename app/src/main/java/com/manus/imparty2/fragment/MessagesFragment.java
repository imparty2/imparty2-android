package com.manus.imparty2.fragment;

import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Messenger;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


import com.manus.imparty2.ChatInfoActivity;
import com.manus.imparty2.MessagesActivity;
import com.manus.imparty2.ContactListActivity;
import com.manus.imparty2.R;
import com.manus.imparty2.Utils;
import com.manus.imparty2.R.id;
import com.manus.imparty2.R.layout;
import com.manus.imparty2.adapter.GenericAdapter;
import com.manus.imparty2.adapter.GenericChatAdapter;
import com.manus.imparty2.adapter.MessageAdapter;
import com.manus.imparty2.client.Presence;
import com.manus.imparty2.core.Chat;
import com.manus.imparty2.core.Contact;
import com.manus.imparty2.core.ImageLoader;
import com.manus.imparty2.dummy.DummyContent;
import com.manus.imparty2.fragment.ContactListFragment.ContactLoader;
import com.manus.imparty2.service.MessagingService;
import com.manus.imparty2.service.MessagingService.MessageReceiver;

/**
 * A fragment representing a single Contact detail screen. This fragment is
 * either contained in a {@link ContactListActivity} in two-pane mode (on
 * tablets) or a {@link MessagesActivity} on handsets.
 */
public class MessagesFragment extends BaseMessagesFragment<Chat> implements MessageReceiver, OnClickListener {

	@Override
	protected GenericChatAdapter<Chat> getAdapter() {
		// TODO Auto-generated method stub
		return new MessageAdapter(getActivity());
	}

	@Override
	public List<Chat> getChats(String username) {
		// TODO Auto-generated method stub
		return MessagingService.getInstance().getChats(username);
	}

	@Override
	public Chat parse(Bundle data) {
		// TODO Auto-generated method stub
		return Chat.unbundle(data);
	}

	@Override
	public void update(GenericAdapter<Chat> adapter, Bundle data) {
		// TODO Auto-generated method stub
		for(int i = adapter.getCount() - 1; i >= 0 ; i--)
		{
			if(adapter.getItem(i)._id.equals(data.getString("_id")))
			{
				if(data.containsKey("status"))
				{
					adapter.getItem(i).status = Integer.parseInt(data.getString("status"));
				}
				if(data.containsKey("progress"))
				{
					adapter.getItem(i).progress = data.getInt("progress");
				}
				adapter.notifyDataSetChanged();
				break;
			}
		}
		
	}

	@Override
	public void presenceChanged(Presence p) {
		// TODO Auto-generated method stub
		if(!contact.provider.equals("phonebook"))
		{
            ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(p.name);
		}
		ImageLoader loader = new ImageLoader();
		loader.setImageAB(p.avatar,((ActionBarActivity) getActivity()).getSupportActionBar());

		if(Utils.isGroup(username))
		{

			try {
				JSONArray arr = new JSONArray(p.presence);
                String[] avatars = new String[arr.length()];
				String parts = "";
				for(int i = 0; i < arr.length(); i++)
				{
					JSONObject obj = arr.getJSONObject(i);
					parts += (parts.equals("")?"":", ");
					Contact contact = MessagingService.getInstance().getContact(obj.getString("uid"));
					if(contact != null)
					{
						parts += contact.display_name;

                        avatars[i] = contact.avatar;
                    }
					else
					{
						parts += obj.getString("name");
                        avatars[i] = obj.getString("avatar");
					}

				}
                ((ActionBarActivity) getActivity()).getSupportActionBar().setSubtitle(parts);
				setParticipants(avatars);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
            ((ActionBarActivity) getActivity()).getSupportActionBar().setSubtitle(Utils.getPresenceString(getActivity(), p));

            setParticipants(new String[]{p.avatar});
		}
			
		
	}

	@Override
	public void sendMessage(String username, String message) {
		// TODO Auto-generated method stub
		MessagingService.getInstance().send_message(username, editor.getText().toString());
	}
	

	
	
}
