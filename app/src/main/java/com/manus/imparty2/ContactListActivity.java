package com.manus.imparty2;


import com.manus.imparty2.adapter.AccountAdapter;
import com.manus.imparty2.client.API;
import com.manus.imparty2.client.Account;
import com.manus.imparty2.client.Login;
import com.manus.imparty2.core.ImageLoader;
import com.manus.imparty2.fragment.MessagesFragment;
import com.manus.imparty2.fragment.ContactListFragment;
import com.manus.imparty2.login.FacebookLoginActivity;
import com.manus.imparty2.login.GoogleLoginActivity;
import com.manus.imparty2.login.PhoneNumberActivity;
import com.manus.imparty2.login.TwitterLoginActivity;
import com.manus.imparty2.push.PushHelper;
import com.manus.imparty2.service.MessagingService;
import com.manus.imparty2.service.MessagingService.MessageReceiver;
import com.manus.imparty2.service.UpdatesManager;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Messenger;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.style.LineHeightSpan.WithDensity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * An activity representing a list of Items. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link MessagesActivity} representing item details. On tablets, the activity
 * presents the list of items and item details side-by-side using two vertical
 * panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link ContactListFragment} and the item details (if present) is a
 * {@link MessagesFragment}.
 * <p>
 * This activity also implements the required
 * {@link ContactListFragment.Callbacks} interface to listen for item
 * selections.
 */
public class ContactListActivity extends ActionBarActivity implements ContactListFragment.Callbacks, MessageReceiver {

	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	private boolean mTwoPane;

	private DrawerLayout mDrawer;
	private View mDrawerLayout;

	private CustomActionBarDrawerToggle mDrawerToggle;

	private static final int ACTIVITY_RESULT_LOGIN = 1;
	private static final int ACTIVITY_RESULT_SETTINGS = 2;
	private static final int ACTIVITY_RESULT_ADD_ACCOUNT = 3;
	private static final int ACTIVITY_RESULT_PICK_USER = 4;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//if(PushHelper.checkPlayServices(this))
		{
			if (!ConfigManager.hasLogin(this)) {
				startActivityForResult(new Intent(this, LoginActivity.class), ACTIVITY_RESULT_LOGIN);
			} else {
				Intent i = new Intent(this, MessagingService.class);
				i.putExtra("sync", true);
				startService(i);
			}
			setContentView(R.layout.activity_contact_list);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setHomeButtonEnabled(true);
	
			mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
			mDrawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
			_initMenu();
			mDrawerToggle = new CustomActionBarDrawerToggle(this, mDrawer);
			mDrawerLayout = findViewById(R.id.drawer);
			mDrawer.setDrawerListener(mDrawerToggle);
			if (findViewById(R.id.contact_detail_container) != null) {
				// The detail container view will be present only in the
				// large-screen layouts (res/values-large and
				// res/values-sw600dp). If this view is present, then the
				// activity should be in two-pane mode.
				mTwoPane = true;
	
				// In two-pane mode, list items should be given the
				// 'activated' state when touched.
				((ContactListFragment) getSupportFragmentManager().findFragmentById(R.id.contact_list)).setActivateOnItemClick(true);
			}
			setDrawerLoading();
			MessagingService.attachListenner(this);
			UpdatesManager manager = new UpdatesManager(this);
			if(manager.is_update())
			{
				manager.dispay_dialog();
			}
		}

		// TODO: If exposing deep links into your app, handle intents here.
	}

	private void _initMenu() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		if (mDrawerToggle != null)
			mDrawerToggle.syncState();
	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(arg0, arg1, arg2);
		if (arg0 == ACTIVITY_RESULT_LOGIN) {
			if (arg1 == RESULT_OK) {
				ConfigManager.saveLogin(this, arg2.getStringExtra("uid"), arg2.getStringExtra("token"));
				startService(new Intent(this, MessagingService.class));
			} else {
				finish();
			}
		} else if (arg0 == ACTIVITY_RESULT_SETTINGS && arg1 == RESULT_OK) {
			if (arg2.hasExtra("logout")) {
				__logout();
			}
		} else if (arg0 == ACTIVITY_RESULT_ADD_ACCOUNT && arg1 == RESULT_OK) {
			final String provider = arg2.getStringExtra("provider");
			final String username = arg2.getStringExtra("username");
			final String hash = arg2.getStringExtra("hash");
			final ProgressDialog p = new ProgressDialog(this);
			p.setMessage("...");
			p.show();
			new Thread(new Runnable() {

				@Override
				public void run() {
					final boolean res = MessagingService.getInstance().ensureLogin() && API.add_account(provider, username, hash);
					if (res) {
						MessagingService.getInstance().refresh_login();
					}
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							p.cancel();
							if (!res) {
								Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_SHORT).show();
							}
						}
					});

				}
			}).start();
		} else if( arg0 == ACTIVITY_RESULT_PICK_USER && arg1 == RESULT_OK )
		{
			onItemSelected(arg2.getStringExtra("uid"));
		}
	}

	/**
	 * Callback method from {@link ContactListFragment.Callbacks} indicating
	 * that the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(String id) {
		if (mTwoPane) {
			// In two-pane mode, show the detail view in this activity by
			// adding or replacing the detail fragment using a
			// fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putString("username", id);
			MessagesFragment fragment = new MessagesFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction().replace(R.id.contact_detail_container, fragment).commit();

		} else {
			// In single-pane mode, simply start the detail activity
			// for the selected item ID.
			Intent detailIntent = new Intent(this, MessagesActivity.class);
			detailIntent.putExtra("username", id);
			startActivity(detailIntent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.main, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/*
		 * The action bar home/up should open or close the drawer.
		 * ActionBarDrawerToggle will take care of this.
		 */
		if (item.getItemId() == android.R.id.home) {
			if (mDrawer.isDrawerOpen(mDrawerLayout)) {
				mDrawer.closeDrawer(mDrawerLayout);
			} else {
				mDrawer.openDrawer(mDrawerLayout);
			}
			return true;
		}
		if (item.getItemId() == R.id.action_settings) {
			startActivityForResult(new Intent(this, SettingsActivity.class), ACTIVITY_RESULT_SETTINGS);
		}
		if (item.getItemId() == R.id.action_chat) {
			startActivityForResult(new Intent(this, PickContactActivity.class), ACTIVITY_RESULT_PICK_USER);
		}
		if(item.getItemId() == R.id.action_group)
		{
			startActivity(new Intent(this, CreateGroupActivity.class));
			
		}

		// Handle your other action bar items...
		return super.onOptionsItemSelected(item);
	}

	private class CustomActionBarDrawerToggle extends ActionBarDrawerToggle {

		public CustomActionBarDrawerToggle(Activity mActivity, DrawerLayout mDrawerLayout) {
			super(mActivity, mDrawerLayout, R.drawable.ic_navigation_drawer, R.string.app_name, R.string.app_name);
		}

		@Override
		public void onDrawerClosed(View view) {
			// getActionBar().setTitle(getString(mCurrentTitle));
			invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
		}

		@Override
		public void onDrawerOpened(View drawerView) {
			getSupportActionBar().setTitle(getString(R.string.app_name));
			invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
		}

	}

	void __logout() {
		ConfigManager.clearLogin(this);
		stopService(new Intent(this, MessagingService.class));
		startActivityForResult(new Intent(this, LoginActivity.class), ACTIVITY_RESULT_LOGIN);

	}

	@Override
	public boolean onMessage(Bundle data) {
		// TODO Auto-generated method stub
		if (data.getInt("key", 0) == MessagingService.BUNDLE_KEY_LOGIN || data.getInt("key", 0) == MessagingService.BUNDLE_KEY_SERVICE_BOOT) {
			if (MessagingService.getInstance().loginInfo != null) {
				if (MessagingService.getInstance().loginInfo.deny) {
					__logout();
				} else {
					setDrawerLogingInfo();
				}
			} else if (data.getInt("key", 0) != MessagingService.BUNDLE_KEY_SERVICE_BOOT) {
				setDrawerLogingError("No data connection");
			}
			return true;
		}
		return false;

	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		if(MessagingService.getInstance() != null)
			MessagingService.getInstance().set_presence("offline");
	}

	void setDrawerLogingError(String error) {
		findViewById(R.id.loading_info).setVisibility(View.GONE);
		findViewById(R.id.display_info).setVisibility(View.GONE);
		findViewById(R.id.display_error).setVisibility(View.VISIBLE);

		((TextView) findViewById(R.id.error)).setText(error);

	}
	void setDrawerLogingInfo() {
		findViewById(R.id.loading_info).setVisibility(View.GONE);
		findViewById(R.id.display_info).setVisibility(View.VISIBLE);
		findViewById(R.id.display_error).setVisibility(View.GONE);

		final Login login = MessagingService.getInstance().loginInfo;

		((TextView) mDrawerLayout.findViewById(R.id.username)).setText(login.public_name);
		((TextView) mDrawerLayout.findViewById(R.id.textView2)).setText(login.status);
		((ImageButton) mDrawerLayout.findViewById(R.id.add_account)).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Utils.show_add_account_dialog(ContactListActivity.this, ACTIVITY_RESULT_ADD_ACCOUNT);
				
			}
		});
		ListView list = ((ListView) mDrawerLayout.findViewById(R.id.accounts));
		list.setAdapter(new AccountAdapter(this, R.layout.item_account, login.accounts));
		ImageLoader loader = new ImageLoader();
		ImageView image = ((ImageView) mDrawerLayout.findViewById(R.id.imageView2));
		loader.setImage(login.avatar, image);
		image.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(ContactListActivity.this, ProfileActivity.class));
			}
		});
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				final Account acc = login.accounts.get(arg2);
				Builder b = new Builder(ContactListActivity.this);
				b.setTitle(acc.name);
				b.setItems(R.array.action_account, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						final ProgressDialog pd = new ProgressDialog(ContactListActivity.this);
						pd.setMessage("...");
						pd.show();

						if (which == 0) {

							new Thread(new Runnable() {

								@Override
								public void run() {
									runOnUiThread(new Runnable() {

										@Override
										public void run() {
											// TODO Auto-generated method stub
											pd.cancel();
										}
									});
									if (MessagingService.getInstance().ensureLogin() && API.set_profile_image(acc.avatar)) {
										MessagingService.getInstance().refresh_login();
									} else {
										runOnUiThread(new Runnable() {

											@Override
											public void run() {
												// TODO Auto-generated method
												// stub
												Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
											}
										});
									}
								}
							}).start();
						} else if (which == 1) {
							new Thread(new Runnable() {

								@Override
								public void run() {
									runOnUiThread(new Runnable() {

										@Override
										public void run() {
											// TODO Auto-generated method stub
											pd.cancel();
										}
									});
									if (MessagingService.getInstance().ensureLogin() && API.remove_account(acc.provider, acc.username)) {
										MessagingService.getInstance().refresh_login();
										MessagingService.getInstance().clearContacts(acc);
									} else {
										runOnUiThread(new Runnable() {

											@Override
											public void run() {
												// TODO Auto-generated method
												// stub
												Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
											}
										});
									}
								}
							}).start();
						}
					}
				});
				b.create().show();
			}
		});

	}
	void setDrawerLoading() {

		findViewById(R.id.loading_info).setVisibility(View.VISIBLE);
		findViewById(R.id.display_info).setVisibility(View.GONE);
		findViewById(R.id.display_error).setVisibility(View.GONE);
	}
}
