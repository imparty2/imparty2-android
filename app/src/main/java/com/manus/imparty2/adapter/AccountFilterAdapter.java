package com.manus.imparty2.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.manus.imparty2.R;
import com.manus.imparty2.client.Account;
import com.manus.imparty2.service.MessagingService;

public class AccountFilterAdapter extends AccountAdapter {

	public AccountFilterAdapter(Context context, int resource, List<Account> list) {
		super(context, resource, list);
	}
	
	List<Boolean> items = new ArrayList<Boolean>();
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = super.getView(position, convertView, parent);
		Account acc = getItem(position);
		((CheckBox)convertView.findViewById(R.id.acc_check)).setChecked(acc.sync);
		((CheckBox)convertView.findViewById(R.id.acc_check)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CheckBox cb = (CheckBox) v;
				Account acc = (Account) v.getTag();
				acc.sync = !acc.sync;
				MessagingService.getInstance().sync_account(acc, acc.sync);
				
			}
		});
		((CheckBox)convertView.findViewById(R.id.acc_check)).setTag(getItem(position));
		
		return convertView;
	}
	
	
	
	

}
