package com.manus.imparty2.adapter;

import java.util.HashSet;
import java.util.Set;

import com.manus.imparty2.R;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

public abstract class GenericChatAdapter<T> extends GenericAdapter<T> {

	public GenericChatAdapter(Context context) {
		super(context, R.layout.item_message);
		// TODO Auto-generated constructor stub
	}

	private HashSet<Integer> checkedItems = new HashSet<Integer>();
	// multi selection mode flag
	private boolean multiMode;

	protected abstract void getView(int position, ViewHolerBase holer);

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolerBase holer;
		if (convertView == null) {
			convertView = getInflater();
			holer = new ViewHolerBase();
			holer.recv_layout = ((LinearLayout) convertView.findViewById(R.id.message_recv));
			holer.send_layout = ((LinearLayout) convertView.findViewById(R.id.message_send));
			holer.info_layout = ((LinearLayout) convertView.findViewById(R.id.info_message));
			holer.group_layout = ((LinearLayout) convertView.findViewById(R.id.group));

			holer.send_time = ((TextView) convertView.findViewById(R.id.send_time));
			holer.recv_time = ((TextView) convertView.findViewById(R.id.recv_time));
			holer.info_text = ((TextView) convertView.findViewById(R.id.info_text));
			holer.display = ((TextView) convertView.findViewById(R.id.display));
			holer.state = ((ImageView) convertView.findViewById(R.id.imageView1));
			holer.retry = ((Button) convertView.findViewById(R.id.button1));
			View view = ((ViewStub) convertView.findViewById(R.id.media_recv)).inflate();
			holer.recv_media = new MediaHoler(view);
			holer.recv_media.text = ((TextView) convertView.findViewById(R.id.recv_message));

			view = ((ViewStub) convertView.findViewById(R.id.media_send)).inflate();
			holer.send_media = new MediaHoler(view);
			holer.send_media.text = ((TextView) convertView.findViewById(R.id.send_message));

			convertView.setTag(holer);
		} else {
			holer = (ViewHolerBase) convertView.getTag();
		}
		getView(position, holer);

		convertView.setBackgroundResource(R.drawable.selector_list_multimode);

		if (checkedItems.contains(Integer.valueOf(position))) {
			// if this item is checked - set checked state
			convertView.getBackground().setState(new int[]{android.R.attr.state_pressed});
		} else {
			// if this item is unchecked - set unchecked state (notice the
			// minus)
			convertView.getBackground().setState(new int[]{-android.R.attr.state_pressed});
		}

		return convertView;
	}
	public void enterMultiMode() {
		this.multiMode = true;
		this.notifyDataSetChanged();
	}

	public void exitMultiMode() {
		this.checkedItems.clear();
		this.multiMode = false;
		this.notifyDataSetChanged();
	}
	public void setChecked(int pos, boolean checked) {
		if (checked) {
			this.checkedItems.add(Integer.valueOf(pos));
		} else {
			this.checkedItems.remove(Integer.valueOf(pos));
		}
		if (this.multiMode) {
			this.notifyDataSetChanged();
		}
	}

	public boolean isChecked(int pos) {
		return this.checkedItems.contains(Integer.valueOf(pos));
	}

	public void toggleChecked(int pos) {
		final Integer v = Integer.valueOf(pos);
		if (this.checkedItems.contains(v)) {
			this.checkedItems.remove(v);
		} else {
			this.checkedItems.add(v);
		}
		this.notifyDataSetChanged();
	}

	public int getCheckedItemCount() {
		return this.checkedItems.size();
	}

	// we use this convinience method for rename thingie.
	// public LibraryItem getFirstCheckedItem()
	// {
	// for (Integer i : this.checkedItems) {
	// return this.items.get(i.intValue());
	// }
	// return null;
	// }

	public Set<Integer> getCheckedItems() {
		return this.checkedItems;
	}

	public class ViewHolerBase {
		LinearLayout send_layout;
		LinearLayout recv_layout;
		LinearLayout info_layout;
		LinearLayout group_layout;
		TextView info_text;
		TextView send_time;
		TextView recv_time;
		TextView display;
		ImageView state;
		Button retry;
		MediaHoler send_media;
		MediaHoler recv_media;
	}

	public class MediaHoler {
		public MediaHoler(View view) {
			media = view;
			// Images
			image_layout = view.findViewById(R.id.media_image);
			image = (ImageView) view.findViewById(R.id.media_image_view);
			download = (ImageButton) view.findViewById(R.id.download);
			image_progress = (ProgressBar) view.findViewById(R.id.progressBar1);
			
			//Audio
			audio_layout = view.findViewById(R.id.media_audio);
			audio_button = (ImageButton) view.findViewById(R.id.imageButton1);
			audio_download = (ProgressBar) view.findViewById(R.id.audio_download);
			audio_player = (SeekBar) view.findViewById(R.id.audio_player);
			audio_length = (TextView) view.findViewById(R.id.audio_length);
			audio_played = (TextView) view.findViewById(R.id.audio_progress);
			
			// Files
			
			file_layout = view.findViewById(R.id.media_file);
			file_open = (Button) view.findViewById(R.id.button1);
			file_download = (ImageButton) view.findViewById(R.id.ImageButton01);
			file_name = (TextView) view.findViewById(R.id.file_name);
			file_progress = (ProgressBar) view.findViewById(R.id.file_progress);
			
			
		}

		View media;
		
		//Images
		View image_layout;
		TextView text;
		ImageView image;
		ImageButton download;
		ProgressBar image_progress;
		
		//Audio
		View audio_layout;
		SeekBar audio_player;
		ProgressBar audio_download;
		ImageButton audio_button;
		TextView audio_length;
		TextView audio_played;
		
		//Files
		View file_layout;
		Button file_open;
		ImageButton file_download;
		TextView file_name;
		ProgressBar file_progress;
		
	}

}
