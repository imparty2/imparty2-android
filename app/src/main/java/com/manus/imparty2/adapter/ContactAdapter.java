package com.manus.imparty2.adapter;

import java.util.Date;
import java.util.List;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.manus.imparty2.Constants;
import com.manus.imparty2.R;
import com.manus.imparty2.core.Contact;
import com.manus.imparty2.core.ImageLoader;
import com.manus.imparty2.core.Provider;

public class ContactAdapter extends GenericAdapter<Contact> {

	public ContactAdapter(Context context, int resource, List<Contact> list) {
		super(context, resource, list);
		
		// TODO Auto-generated constructor stub
	}

	public ContactAdapter(Context context, int resource) {
		super(context, resource);
		// TODO Auto-generated constructor stub
	}

	ImageLoader loader = new ImageLoader();;
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView == null)
		{
			convertView = getInflater();
		}
		Contact c = getItem(position);
		ViewHoler holer;
		if(convertView.getTag() == null)
		{
			holer = new ViewHoler();
			holer.username = (TextView) convertView.findViewById(R.id.textView1);
			holer.second = (TextView) convertView.findViewById(R.id.textView2);
			holer.provider = (ImageView) convertView.findViewById(R.id.imageView2);
			holer.avatar = (ImageView) convertView.findViewById(R.id.imageView1);
			holer.other = (TextView) convertView.findViewById(R.id.textView3);
			convertView.setTag(holer);
		}
		else
		{
			holer = (ViewHoler) convertView.getTag();
		}
		holer.username.setText( c.display_name );
		holer.second.setText( c.username );
		if(c.username != null && !c.username.equals(""))
		{
			holer.provider.setImageResource(Provider.getProviderIcon(c.provider));
		}
		else
		{
			holer.provider.setImageBitmap(null);
			holer.second.setText( c.status );
		}
		loader.setImage(c.avatar, holer.avatar);
		holer.other.setText(c.other);
		if(c.joined == 0)
		{
			convertView.setBackgroundColor(0xFAa0a0a0);
		}
		else
		{
			convertView.setBackgroundResource(R.drawable.selector_list_multimode);
		}
		return convertView;
	}
	
	public class ViewHoler
	{
		public TextView username;
		public TextView second;
		public ImageView provider;
		public ImageView avatar;
		public TextView other;
	}


}
