package com.manus.imparty2.adapter;

import java.util.Date;

import com.manus.imparty2.R;
import com.manus.imparty2.core.Chat;
import com.manus.imparty2.core.GroupChat;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GroupAdapter extends GenericChatAdapter<GroupChat> {

	public GroupAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void getView(int position, com.manus.imparty2.adapter.GenericChatAdapter.ViewHolerBase holer) {
		// TODO Auto-generated method stub

		GroupChat c = getItem(position);
		if(c.isOutgoing())
		{
			holer.recv_layout.setVisibility(View.GONE);
			holer.send_layout.setVisibility(View.VISIBLE);
			//holer.send_message.setText(c.message);
			holer.send_time.setText(DateFormat.getTimeFormat(context).format(new Date(c.time)));
			if(c.status == Chat.STATUS_WRITED_BY_ME)
			{
				holer.state.setImageBitmap(null);
			}
			else if(c.status == Chat.STATUS_SEND_BY_ME)
			{
				holer.state.setImageResource(R.drawable.message_send);
			}
			else if(c.status == Chat.STATUS_DELIVER_TO_CONTACT)
			{
				holer.state.setImageResource(R.drawable.message_delivered);
			}
			
			if(c.status == Chat.STATUS_ERROR)
			{
				holer.retry.setVisibility(View.VISIBLE);
			}
			else
			{
				holer.retry.setVisibility(View.GONE);
			}
		}
		else
		{
			holer.group_layout.setVisibility(View.VISIBLE);
			holer.recv_layout.setVisibility(View.VISIBLE);
			holer.send_layout.setVisibility(View.GONE);
			//holer.recv_message.setText(c.message);
			holer.recv_time.setText(DateFormat.getTimeFormat(context).format(new Date(c.time)));
			holer.display.setText(c.display);
		}
	}

}
