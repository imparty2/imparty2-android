package com.manus.imparty2.adapter;

import java.io.File;
import java.util.List;

import com.manus.imparty2.R;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class FilesAdapter extends GenericAdapter<File> {
	File parent;
	public FilesAdapter(Context context, int resource, List<File> list, File parent) {
		super(context, resource, list);
		this.parent = parent;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		File file = getItem(position);
		if(convertView == null)
			convertView = getInflater();
		((TextView) convertView.findViewById(R.id.name)).setText(file.getName());
		
		if(file.isDirectory())
		{
			((ImageView) convertView.findViewById(R.id.icon)).setImageResource(R.drawable.ic_action_folder);
			
		}
		else
		{
			if(file.getName().endsWith(".apk"))
			{
				((ImageView) convertView.findViewById(R.id.icon)).setImageDrawable(getAppIcon(file));
			}
			else
			{
				((ImageView) convertView.findViewById(R.id.icon)).setImageResource(R.drawable.ic_menu_file);
				
			}
		}
		if(this.parent != null)
		{
			if(file.getAbsoluteFile().equals(this.parent.getAbsoluteFile()))
			{
				((ImageView) convertView.findViewById(R.id.icon)).setImageResource(R.drawable.ic_menu_up);
			}
		}
		return convertView;
	}
	
	
	public Drawable getAppIcon(File file)
	{
		PackageInfo info = context.getPackageManager().getPackageArchiveInfo(file.getAbsolutePath(), 0);
	
		return context.getPackageManager().getApplicationIcon(info.applicationInfo);
	}
	

}
