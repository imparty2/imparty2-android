package com.manus.imparty2.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.manus.imparty2.R;
import com.manus.imparty2.client.Account;
import com.manus.imparty2.core.ImageLoader;
import com.manus.imparty2.core.Provider;

public class AccountAdapter extends GenericAdapter<Account> {

	ImageLoader loader;
	public AccountAdapter(Context context, int resource, List<Account> list) {
		super(context, resource, list);
		loader = new ImageLoader();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView == null)
		{
			convertView = getInflater();
		}
		Account acc = getItem(position);
		((TextView) convertView.findViewById(R.id.textView1)).setText(acc.name);
		((TextView) convertView.findViewById(R.id.textView2)).setText(acc.username);
		((ImageView) convertView.findViewById(R.id.imageView2)).setImageResource(Provider.getProviderIcon(acc.provider));
		
		loader.setImage(acc.avatar, ((ImageView) convertView.findViewById(R.id.imageView1)));
		return convertView;
	}

}
