package com.manus.imparty2.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class GenericAdapter<T> extends BaseAdapter {
	List<Object> list = new ArrayList<Object>();
	Context context;
	int resource;
	public GenericAdapter(Context context, int resource, List<T> list) {
		// TODO Auto-generated constructor stub
		this.list = (List<Object>) list;
		this.context = context;
		this.resource = resource;
	}
	public GenericAdapter(Context context, int resource) {
		// TODO Auto-generated constructor stub
		this.list = new ArrayList<Object>();
		this.context = context;
		this.resource = resource;
	}

	public View getInflater()
	{
		return ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(resource, null);
	}

	public View getInflater(int resource)
	{
		return ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(resource, null);
	}

	public void clear() {
		// TODO Auto-generated method stub
		list.clear();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public T getItem(int position) {
		// TODO Auto-generated method stub
		return (T) list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public void add()
	{
		list.add(null);
		notifyDataSetChanged();
	}
	public void add(T item)
	{
		list.add(item);
		notifyDataSetChanged();
	}
	
	public void set(List<T> items)
	{
		list.clear();
		list.addAll(items);
		notifyDataSetChanged();
	}

	public void add(List<T> items)
	{
		list.addAll(items);
		notifyDataSetChanged();
	}

	public void add(int position, List<T> items)
	{
		list.addAll(position, items);
		notifyDataSetChanged();
	}
	
	public void removeLast()
	{
		list.remove(list.size() - 1);
		notifyDataSetChanged();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView == null)
		{
			convertView = getInflater();
		}
		return convertView;
	}
}
