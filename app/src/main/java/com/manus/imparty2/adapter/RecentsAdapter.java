package com.manus.imparty2.adapter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.manus.imparty2.ConfigManager;
import com.manus.imparty2.R;
import com.manus.imparty2.Utils;
import com.manus.imparty2.client.Presence;
import com.manus.imparty2.core.Chat;
import com.manus.imparty2.core.Contact;
import com.manus.imparty2.core.ImageLoader;
import com.manus.imparty2.core.Provider;
import com.manus.imparty2.core.Recent;

public class RecentsAdapter extends GenericAdapter<Recent> {

	SharedPreferences pref;
	public RecentsAdapter(Context context, int resource, List<Recent> list) {
		super(context, resource, list);
		// TODO Auto-generated constructor stub
	}
	ImageLoader loader = new ImageLoader();
	private HashSet<Integer> checkedItems = new HashSet<Integer>();
	// multi selection mode flag
	private boolean multiMode;
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = getInflater();
		}
		Recent c = getItem(position);
		ViewHoler holer;
		if (convertView.getTag() == null) {
			holer = new ViewHoler();
			holer.message_status = (LinearLayout) convertView.findViewById(R.id.messages_status);
			holer.pressence_status = (LinearLayout) convertView.findViewById(R.id.pressence_status);
			holer.username = (TextView) convertView.findViewById(R.id.textView1);
			holer.last_message = (TextView) convertView.findViewById(R.id.textView2);
			holer.pressence = (TextView) convertView.findViewById(R.id.pressence_message);
			holer.message_state = (ImageView) convertView.findViewById(R.id.imageView2);
			holer.avatar = (ImageView) convertView.findViewById(R.id.imageView1);
			holer.silent = (ImageView) convertView.findViewById(R.id.silent_image);
			holer.count = (TextView) convertView.findViewById(R.id.textView3);
			convertView.setTag(holer);
		} else {
			holer = (ViewHoler) convertView.getTag();
		}
		holer.username.setText(c.contact.display_name);
		if (c.chat.status == Chat.STATUS_RECV_FROM_CONTACT) {
			holer.username.setTypeface(null, Typeface.BOLD);
			holer.last_message.setTypeface(null, Typeface.BOLD);
			holer.count.setVisibility(View.VISIBLE);
			holer.count.setText(String.valueOf( c.count) );
		} else {
			holer.username.setTypeface(null, Typeface.NORMAL);
			holer.last_message.setTypeface(null, Typeface.NORMAL);
			holer.count.setVisibility(View.GONE);
		}
		holer.silent.setVisibility(ConfigManager.isSilentUser(context, c.contact.uid)?View.VISIBLE:View.GONE);
		
		
		if(Utils.isInteractivePressence(c.contact.presence))
		{
			holer.message_status.setVisibility(View.GONE);
			holer.pressence_status.setVisibility(View.VISIBLE);
			holer.pressence.setText(Utils.getPresenceString(context, c.contact.presence));
		}
		else
		{
			holer.message_status.setVisibility(View.VISIBLE);
			holer.pressence_status.setVisibility(View.GONE);
		}
		
		holer.last_message.setText(c.chat.getPreviewMessage(context));
		holer.message_state.setImageResource(Chat.getImageStatus(c.chat.status));
		loader.setImage(c.contact.avatar, holer.avatar);

		convertView.setBackgroundResource(R.drawable.selector_list_multimode);
		if (checkedItems.contains(Integer.valueOf(position))) {
			// if this item is checked - set checked state
			convertView.getBackground().setState(new int[]{android.R.attr.state_pressed});
		} else {
			// if this item is unchecked - set unchecked state (notice the
			// minus)
			convertView.getBackground().setState(new int[]{-android.R.attr.state_pressed});
		}

		return convertView;
	}
	public void enterMultiMode() {
		this.multiMode = true;
		this.notifyDataSetChanged();
	}

	public void exitMultiMode() {
		this.checkedItems.clear();
		this.multiMode = false;
		this.notifyDataSetChanged();
	}
	public void setChecked(int pos, boolean checked) {
		if (checked) {
			this.checkedItems.add(Integer.valueOf(pos));
		} else {
			this.checkedItems.remove(Integer.valueOf(pos));
		}
		if (this.multiMode) {
			this.notifyDataSetChanged();
		}
	}

	public boolean isChecked(int pos) {
		return this.checkedItems.contains(Integer.valueOf(pos));
	}

	public void toggleChecked(int pos) {
		final Integer v = Integer.valueOf(pos);
		if (this.checkedItems.contains(v)) {
			this.checkedItems.remove(v);
		} else {
			this.checkedItems.add(v);
		}
		this.notifyDataSetChanged();
	}

	public int getCheckedItemCount() {
		return this.checkedItems.size();
	}

	// we use this convinience method for rename thingie.
	// public LibraryItem getFirstCheckedItem()
	// {
	// for (Integer i : this.checkedItems) {
	// return this.items.get(i.intValue());
	// }
	// return null;
	// }

	public Set<Integer> getCheckedItems() {
		return this.checkedItems;
	}
	public class ViewHoler {
		public TextView username;
		public TextView pressence;
		public TextView last_message;
		public ImageView message_state;
		public ImageView avatar;
		public ImageView silent;
		public TextView count;
		
		LinearLayout message_status;
		LinearLayout pressence_status;
	}

}
