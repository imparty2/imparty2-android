package com.manus.imparty2.adapter;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.manus.imparty2.R;
import com.manus.imparty2.Utils;
import com.manus.imparty2.client.HttpUtils;
import com.manus.imparty2.client.HttpUtils.HttpProgressListenner;
import com.manus.imparty2.core.Chat;
import com.manus.imparty2.core.MediaProvider;
import com.manus.imparty2.core.Message;
import com.manus.imparty2.core.Photos;
import com.manus.imparty2.service.MessagingService;

public class MessageAdapter extends GenericChatAdapter<Chat> {

	MediaPlayer mPlayer = new MediaPlayer();

	public MessageAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void getView(int position, GenericChatAdapter.ViewHolerBase holer) {
		// TODO Auto-generated method stub
		Chat c = getItem(position);
		holer.group_layout.setVisibility(View.GONE);
		if (position != 0) {
			Date date1 = new Date(c.time);
			Date date2 = new Date(getItem(position - 1).time);
			java.text.DateFormat f = DateFormat.getDateFormat(context);
			if (!f.format(date1).equals(f.format(date2))) {
				holer.info_layout.setVisibility(View.VISIBLE);
				holer.info_text.setText(DateFormat.getDateFormat(context).format(date1));
			} else {
				holer.info_layout.setVisibility(View.GONE);
			}
		} else {
			Date date1 = new Date(c.time);
			holer.info_layout.setVisibility(View.VISIBLE);
			holer.info_text.setText(DateFormat.getDateFormat(context).format(date1));
		}
		if (c.uid != null && !c.uid.equals("")) {
			holer.group_layout.setVisibility(View.VISIBLE);
			holer.display.setText(c.display);
		} else {
			holer.group_layout.setVisibility(View.GONE);
		}
		if (c.isOutgoing()) {
			holer.recv_layout.setVisibility(View.GONE);
			holer.send_layout.setVisibility(View.VISIBLE);
			holer.send_time.setText(DateFormat.getTimeFormat(context).format(new Date(c.time)));
			if (c.status == Chat.STATUS_WRITED_BY_ME) {
				holer.state.setImageBitmap(null);
			} else if (c.status == Chat.STATUS_SEND_BY_ME) {
				holer.state.setImageResource(R.drawable.message_send);
			} else if (c.status == Chat.STATUS_DELIVER_TO_CONTACT) {
				holer.state.setImageResource(R.drawable.message_delivered);
			}

			if (c.status == Chat.STATUS_ERROR) {
				holer.retry.setVisibility(View.VISIBLE);
			} else {
				holer.retry.setVisibility(View.GONE);
			}
			setMessage(c, holer.send_media);
			// holer.send_message.setText(c.message);
		} else {
			holer.recv_layout.setVisibility(View.VISIBLE);
			holer.send_layout.setVisibility(View.GONE);
			holer.recv_time.setText(DateFormat.getTimeFormat(context).format(new Date(c.time)));

			setMessage(c, holer.recv_media);
		}

	}
	String playing_message = "";
	void setMessage(final Chat chat, com.manus.imparty2.adapter.GenericChatAdapter.MediaHoler holer) {
		Message message = chat.getMessage();
		if (message.type.equals("chat") || message.type.equals("group")) {
			holer.text.setText(chat.message);
			if(message.type.equals("group"))
			{
				holer.text.setText(Message.getGroupMessage(context, message));
			}
			holer.text.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			holer.media.setVisibility(View.GONE);
		} else {
			holer.text.setText(Utils.convertToStringRepresentation(message.size));
			
			if (message.type.equals("image")) {
				holer.text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_picture, 0, 0, 0);

				holer.media.setVisibility(View.VISIBLE);
				holer.image_layout.setVisibility(View.VISIBLE);
				holer.audio_layout.setVisibility(View.GONE);
				holer.file_layout.setVisibility(View.GONE);

				final File file = MediaProvider.get_media_file(message);
				Bitmap bmp = null;
				if (file.exists()) {
					Options ops = new Options();
					ops.inSampleSize = 4;
					holer.download.setVisibility(View.GONE);
					bmp = Photos.getSampleSize(file.getAbsolutePath(), 200);
					if (bmp != null) {

						holer.image_layout.setClickable(true);
						holer.image.setImageBitmap(bmp);
						holer.image_layout.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub

								Intent intent = new Intent();
								intent.setAction(android.content.Intent.ACTION_VIEW);
								intent.setDataAndType(Uri.fromFile(file), "image/*");
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								context.startActivity(intent);
							}
						});
					}
				}

				if (bmp == null) {
					holer.image.setImageBitmap(Utils.get_base64_image_preview(message.data));
					holer.download.setVisibility(View.VISIBLE);
					holer.download.setImageResource(R.drawable.ic_download);
					holer.download.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							MessagingService.getInstance().download_chat(chat);
						}
					});
				}
				if (chat.progress != 0 && chat.progress < 100) {
					holer.download.setImageResource(R.drawable.ic_cancel);
					holer.image_progress.setVisibility(View.VISIBLE);
					if (chat.progress == -1)
						holer.image_progress.setIndeterminate(true);
					else {
						holer.image_progress.setIndeterminate(false);
						holer.image_progress.setProgress(chat.progress);
					}
				} else {

					holer.image_progress.setVisibility(View.GONE);
				}

			} else if (message.type.equals("audio")) {
				String data = message.data;
				int leng = 0;
				try {
					leng = Integer.parseInt(data);
				} catch (Exception ex) {

				}
				holer.text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_record, 0, 0, 0);

				holer.media.setVisibility(View.VISIBLE);
				holer.image_layout.setVisibility(View.GONE);
				holer.audio_layout.setVisibility(View.VISIBLE);
				holer.file_layout.setVisibility(View.GONE);

				final File file = MediaProvider.get_media_file(message);
				holer.audio_played.setText(Utils.getElapsedTime(0));
				if (file.exists()) {
					holer.audio_player.setVisibility(View.VISIBLE);
					if (playing_message.equals(chat._id)) {
						holer.audio_button.setImageResource(R.drawable.ic_action_stop);
						holer.audio_player.setMax(mPlayer.getDuration());
						holer.audio_player.setProgress(mPlayer.getCurrentPosition());
						holer.audio_played.setText(Utils.getElapsedTime(mPlayer.getCurrentPosition()));
					} else {
						holer.audio_button.setImageResource(R.drawable.ic_action_play);
						holer.audio_player.setProgress(0);
					}
				} else {
					// Display download button
					holer.audio_player.setVisibility(View.VISIBLE);
					holer.audio_button.setImageResource(R.drawable.ic_download);
				}
				holer.audio_length.setText(Utils.getElapsedTime(leng));

				holer.audio_button.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (file.exists()) {
							if (chat._id.equals(playing_message)) {
								// stop
								mPlayer.stop();
								mPlayer.release();
								playing_message = "";
								MessagingService.getInstance().update_chat(chat, 100, -1);

							} else {
								if (!playing_message.equals("")) {
									mPlayer.stop();
									mPlayer.release();
								}
								try {
									mPlayer = new MediaPlayer();
									mPlayer.setDataSource(file.getAbsolutePath());
									mPlayer.prepare();
									mPlayer.start();
									mPlayer.setOnCompletionListener(new OnCompletionListener() {

										@Override
										public void onCompletion(MediaPlayer mp) {
											// TODO Auto-generated method stub
											mPlayer.release();
											playing_message = "";
											MessagingService.getInstance().update_chat(chat, 100, -1);

										}
									});
									playing_message = chat._id;
									MessagingService.getInstance().update_chat(chat, 500, mPlayer.getDuration() + 600);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									Toast.makeText(context, "Error!", 0).show();
									mPlayer.release();
								}

							}
						} else {
							MessagingService.getInstance().download_chat(chat);
						}
					}
				});

				if (chat.progress != 0 && chat.progress < 100) {
					holer.audio_button.setImageResource(R.drawable.ic_cancel);
					holer.audio_download.setVisibility(View.VISIBLE);
					if (chat.progress == -1)
						holer.audio_download.setIndeterminate(true);
					else {
						holer.audio_download.setIndeterminate(false);
						holer.audio_download.setProgress(chat.progress);
					}
				} else {
					holer.audio_download.setVisibility(View.GONE);
				}
			} else if (message.type.equals("video")) {
				String leng = message.data.substring(0, message.data.indexOf(":"));
				String image = message.data.substring(message.data.indexOf(":") + 1);
				holer.text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_video, 0, 0, 0);
				holer.media.setVisibility(View.VISIBLE);
				holer.image_layout.setClickable(true);
				holer.image_layout.setVisibility(View.VISIBLE);
				holer.audio_layout.setVisibility(View.GONE);
				holer.file_layout.setVisibility(View.GONE);
				holer.download.setVisibility(View.VISIBLE);
				final File file = MediaProvider.get_media_file(message);
				if (file.exists()) {
					holer.download.setImageResource(R.drawable.ic_action_play);
					holer.image.setImageBitmap(Utils.get_preview_video_image(file.getAbsolutePath()));

					holer.download.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = new Intent();
							intent.setAction(Intent.ACTION_VIEW);
							intent.setDataAndType(Uri.fromFile(file), "video/mp4");
							context.startActivity(intent);
						}
					});
				}
				else
				{
					holer.download.setImageResource(R.drawable.ic_download);
					holer.image.setImageBitmap(Utils.get_base64_image_preview(image));

					holer.download.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							MessagingService.getInstance().download_chat(chat);
						}
					});
				}
				if (chat.progress != 0 && chat.progress < 100) {
					holer.download.setImageResource(R.drawable.ic_cancel);
					holer.image_progress.setVisibility(View.VISIBLE);
					if (chat.progress == -1)
						holer.image_progress.setIndeterminate(true);
					else {
						holer.image_progress.setIndeterminate(false);
						holer.image_progress.setProgress(chat.progress);
					}
				} else {

					holer.image_progress.setVisibility(View.GONE);
				}
			}
			else if(message.type.equals("file"))
			{
				holer.media.setVisibility(View.VISIBLE);
				holer.image_layout.setVisibility(View.GONE);
				holer.audio_layout.setVisibility(View.GONE);
				holer.file_layout.setVisibility(View.VISIBLE);
				final File file = MediaProvider.get_media_file(message);
				holer.file_name.setText(file.getName());
				if(file.exists())
				{
					holer.file_download.setVisibility(View.GONE);
					holer.file_open.setVisibility(View.VISIBLE);
				}
				else
				{
					holer.file_download.setVisibility(View.VISIBLE);
					holer.file_open.setVisibility(View.GONE);
				}
				holer.file_download.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						MessagingService.getInstance().download_chat(chat);
						
					}
				});
				holer.file_open.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						Intent intent = new Intent();
						intent.setAction(android.content.Intent.ACTION_VIEW);
						intent.setDataAndType(Uri.fromFile(file), Utils.getMimeType(file.getAbsolutePath()));
						context.startActivity(intent);
					}
				});
				if (chat.progress != 0 && chat.progress < 100) {
					holer.file_download.setImageResource(R.drawable.ic_cancel);
					holer.file_download.setVisibility(View.VISIBLE);
					holer.file_open.setVisibility(View.GONE);
					holer.file_progress.setVisibility(View.VISIBLE);
					if (chat.progress == -1)
						holer.file_progress.setIndeterminate(true);
					else {
						holer.file_progress.setIndeterminate(false);
						holer.file_progress.setProgress(chat.progress);
					}
				} else {

					holer.file_progress.setVisibility(View.GONE);
				}
				
				
			}
		}
	}

}
