package com.manus.imparty2;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import android.app.ActivityManager;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

@ReportsCrashes(formKey = "",
		formUri = "http://pagenizer.tk/report/crash.php?app=HZwKkeymYfzStEX",
		mode = ReportingInteractionMode.SILENT
		)
public class Application extends android.app.Application {

	private static Application instance;
	
	public Application() {
		// TODO Auto-generated constructor stub
		instance = this;
		
	}
	public static Application getInstance()
	{
		return instance;
	}
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		ACRA.init(this);
		
		
		
	}
}
