package com.manus.imparty2;

public class Constants {

	public static final String PROVIDER_PHONEBOOK = "phonebook";
	public static final String PROVIDER_GOOGLE = "google";
	public static final String PROVIDER_TWITTER = "twitter";
	public static final String PROVIDER_FACEBOOK = "facebook";

	public static final String JPUSH_SERVER = "jpush.imparty.org";
	public static final String BASE_URL = "api.imparty.org";
//	public static final String BASE_URL = "192.168.1.139";
	//imparty/auth/
	public static final String DOCUMENT_ROOT = "";
	public static final String DOCUMENT_AUTH = DOCUMENT_ROOT + "auth/";
	
	public static final String LOGIN_TWITTER = "http://" + BASE_URL + "/" + DOCUMENT_AUTH + "twitter.php";
	public static final String LOGIN_GOOGLE = "http://" + BASE_URL + "/" + DOCUMENT_AUTH + "google.php";
	public static final String LOGIN_FACEBOOK = "http://" + BASE_URL + "/" + DOCUMENT_AUTH + "facebook.php";
	public static final String API_VERSION = "v1";
	public static final String API = "http://" + BASE_URL + "/" + DOCUMENT_ROOT + API_VERSION + "/";

	public static final String API_GET_TOKEN = API + "get_token.php";
	public static final String API_LOGIN = API + "login.php";
	public static final String API_SMS = API + "sms.php";
	public static final String API_SEND_MESSAGE = API + "send.php";
	public static final String API_RECV_MESSAGES = API + "recv.php";
	public static final String API_DELIVER = API + "deliver.php";;
	public static final String API_PROTOCOL = API + "protocol.php";
	public static final String API_ADD_ACCOUNT =  API + "add_account.php";
	public static final String API_REMOVE_ACCOUNT =  API + "remove_account.php";
	public static final String API_EDIT_PROFILE =  API + "edit_profile.php";
	public static final String API_PRESENCE = API + "get_user.php";
	public static final String API_SET_PRESENCE = API + "set_presence.php";
	public static final String API_CREATE_GROUP = API + "create_group.php";
	public static final String API_ADD_PARTICIPANT = API + "add_participant.php";
	public static final String API_REMOVE_PARTICIPANT = API + "remove_participant.php";
	public static final String API_SYNC =  API + "sync.php";
	public static final String GCM_SENDER_ID = "384873501677";
	public static final String UPLOAD_PROVIDER = "http://manushare.tk/upload.php?json";
	public static final String UPDATE_FILE = "http://" + BASE_URL + "/files/versions.json";
	
}
