package com.manus.imparty2.client;

import org.json.JSONException;
import org.json.JSONObject;

import com.manus.imparty2.client.HttpUtils.CustomPairs;

public class SMSResponse {

	public JSONObject extras;
	public boolean result;
	public static SMSResponse parse(JSONObject obj)
	{
		SMSResponse response = new SMSResponse();
		try {
			response.result = obj.getBoolean("result");
			response.extras = obj.optJSONObject("extras");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}
}
