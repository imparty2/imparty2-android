package com.manus.imparty2.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Login {

	public boolean deny = false;
	public String public_name;
	public String status;
	public String avatar;
	public List<Account> accounts = new ArrayList<Account>();
	public JSONArray config;
	public long server_time = 0;
	public void parseResponse(JSONObject json) throws JSONException
	{
		if(json.getString("result").equals("error"))
		{
			deny = json.getString("message").equals("credentials_error");
		}
		else
		{
			
			this.public_name = json.getString("name");
			this.avatar = json.getString("avatar");
			this.status = json.getString("status");
			this.server_time = json.getLong("time");
			JSONArray list = json.getJSONArray("accounts");
			for(int i = 0; i < list.length(); i++)
			{
				JSONObject acc = list.getJSONObject(i);
				Account _acc = new Account();
				_acc.provider = acc.getString("provider");
				_acc.username = acc.getString("username");
				_acc.name = acc.getString("name");
				_acc.avatar = acc.getString("avatar");
				accounts.add(_acc);
			}
			this.config = json.getJSONArray("config");
		}
	}
	
}
