package com.manus.imparty2.client;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle.Control;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.manus.imparty2.Constants;
import com.manus.imparty2.core.Chat;
import com.manus.imparty2.core.Contact;

public class API {

	public static interface APIHandler {
		public void request_login();
	}
	private static APIHandler handler;

	public static void setAPIHandler(APIHandler handler) {
		API.handler = handler;
	}
	private static boolean parse_response(String response) {
		if (response == null)
			return false;
		try {
			JSONObject obj = new JSONObject(response);
			if (obj.getString("result").equals("error")) {
				if (obj.getString("message").equals("no_login")) {
					if (handler != null) {
						handler.request_login();
						return false;
					}
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return true;

	}
	public static JSONObject getToken(String provider, String username, String hash) {
		String response = HttpUtils.http_get(Constants.API_GET_TOKEN + "?provider=" + provider + "&username=" + username + "&hash=" + hash);
		parse_response(response);
		if (response == null)
			return null;
		try {
			return new JSONObject(response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public static Login login(String uid, String token) {

		String data = HttpUtils.http_get(Constants.API_LOGIN + "?user=" + uid + "&token=" + token);
		if (data != null) {
			try {
				JSONObject json = new JSONObject(data);

				Login login = new Login();
				login.parseResponse(json);
				return login;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	public static boolean add_account(String provider, String username, String hash) {
		String response = HttpUtils.http_get(Constants.API_ADD_ACCOUNT + "?provider=" + provider + "&username=" + username + "&hash=" + hash);
		parse_response(response);
		if (response != null) {
			try {
				JSONObject json = new JSONObject(response);
				return (json.getString("result").equals("ok"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}
	public static boolean remove_account(String provider, String username) {
		String response = HttpUtils.http_get(Constants.API_REMOVE_ACCOUNT + "?provider=" + provider + "&username=" + username);
		parse_response(response);
		if (response != null) {
			try {
				JSONObject json = new JSONObject(response);
				return (json.getBoolean("result"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	public static boolean set_profile_image(String avatar) {

		String response = HttpUtils.http_get(Constants.API_EDIT_PROFILE + "?avatar=" + URLEncoder.encode(avatar));
		parse_response(response);
		if (response != null) {
			try {
				JSONObject json = new JSONObject(response);
				return (json.getString("result").equals("ok"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;

	}

	public static boolean set_protocol(String user, String protocol, String deviceId, String deviceName) {
		String response = HttpUtils.http_get(Constants.API_PROTOCOL + "?user=" + user + "&protocol=" + URLEncoder.encode(protocol) + "&device=" + URLEncoder.encode(deviceId) + "&deviceName=" + URLEncoder.encode(deviceName));
		parse_response(response);
		if (response != null) {
			try {
				JSONObject json = new JSONObject(response);
				return (json.getString("result").equals("ok"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}
	public static JSONArray sendMessages(Chat chat) {
		List<Chat> list = new ArrayList<Chat>();
		list.add(chat);
		return sendMessages(list);
	}
	public static JSONArray sendMessages(List<Chat> chats) {
		try {
			JSONArray arr = new JSONArray();

			for (Chat chat : chats) {
				JSONObject obj = new JSONObject();
				obj.put("_id", chat._id);
				obj.put("contact", chat.contact);
				obj.put("content", URLEncoder.encode(chat.message, "UTF8"));
				arr.put(obj);
			}

			String response = HttpUtils.http_json(Constants.API_SEND_MESSAGE, arr.toString());
			parse_response(response);
			if (response != null) {
				JSONObject json = new JSONObject(response);
				return json.getJSONArray("summary");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static List<Chat> sync_messages() {
		List<Chat> chats = new ArrayList<Chat>();
		String response = HttpUtils.http_get(Constants.API_RECV_MESSAGES);
		parse_response(response);
		if (response != null) {
			try {
				JSONObject json = new JSONObject(response);
				if (json.getString("result").equals("ok")) {
					JSONArray arr = json.getJSONArray("messages");
					for (int i = 0; i < arr.length(); i++) {
						chats.add(Chat.parse(arr.getJSONObject(i)));
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return chats;
	}

	public static boolean deliver(String contact, String id) {

		String response = HttpUtils.http_get(Constants.API_DELIVER + "?contact=" + URLEncoder.encode(contact) + "&id=" + id);
		parse_response(response);
		try {
			JSONObject json = new JSONObject(response);
			return json.getBoolean("result");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static boolean deliver(List<Chat> chats) {
		JSONArray arr = new JSONArray();
		for (Chat chat : chats) {
			JSONObject deliver = new JSONObject();
			try {
				deliver.put("contact", chat.contact);
				deliver.put("id", chat._id);
				arr.put(deliver);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String response = HttpUtils.http_json(Constants.API_DELIVER, arr.toString());
		parse_response(response);
		if (response == null)
			return false;
		try {
			boolean status = true;
			JSONArray json = new JSONArray(response);
			for (int i = 0; i < json.length(); i++) {
				if(!json.getBoolean(i))
				{
					status = false;
				}
			}
			
			return status;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static Presence get_presence(String username) {
		String response = HttpUtils.http_get(Constants.API_PRESENCE + "?username=" + URLEncoder.encode(username));
		parse_response(response);
		if (response != null) {
			try {
				return Presence.parse(new JSONObject(response));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public static boolean set_presence(String presence, String to) {
		String response = HttpUtils.http_get(Constants.API_SET_PRESENCE + "?presence=" + presence + "&to=" + URLEncoder.encode(to));
		parse_response(response);
		if (response != null) {
			try {
				JSONObject json = new JSONObject(response);
				return (json.getString("result").equals("ok"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}
	public static boolean create_group(String title, String avatar) {
		String response = HttpUtils.http_get(Constants.API_CREATE_GROUP + "?title=" + URLEncoder.encode(title) + "&avatar=" + URLEncoder.encode(avatar));
		parse_response(response);
		if (response != null) {
			try {
				JSONObject json = new JSONObject(response);
				return (json.getString("result").equals("ok"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return false;
	}
	public static PaginatedContacts merge_account(Account account, String next_page) {

		PaginatedContacts page = new PaginatedContacts();
		String data = HttpUtils.http_get(Constants.API_SYNC + "?account=" + account.username + "&provider=" + account.provider + "&next_page=" + URLEncoder.encode(next_page));
		parse_response(data);
		if (data == null)
			return null;
		try {
			JSONObject obj = new JSONObject(data);
			page.next_page = obj.getString("next_page");
			JSONArray arr = obj.getJSONArray("list");
			for (int i = 0; i < arr.length(); i++) {
				Contact _contact = new Contact();
				_contact.parse(arr.getJSONObject(i));
				page.contacts.add(_contact);
			}
			return page;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static List<Contact> merge_contacts(JSONArray array) {
		List<Contact> contacts = new ArrayList<Contact>();
		String data = HttpUtils.http_json(Constants.API_SYNC, array.toString());
		parse_response(data);
		JSONArray arr;
		try {
			arr = new JSONArray(data);

			for (int i = 0; i < arr.length(); i++) {
				Contact _contact = new Contact();
				_contact.parse(arr.getJSONObject(i));
				_contact.joined = 1;
				contacts.add(_contact);
			}
			return contacts;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static boolean add_participant(String gid, String uid) {
		// TODO Auto-generated method stub
		String response = HttpUtils.http_get(Constants.API_ADD_PARTICIPANT + "?gid=" + gid + "&uid=" + uid);
		parse_response(response);
		try {
			JSONObject json = new JSONObject(response);
			return (json.getString("result").equals("ok"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public static boolean remove_participant(String gid, String uid) {
		// TODO Auto-generated method stub
		String response = HttpUtils.http_get(Constants.API_REMOVE_PARTICIPANT + "?gid=" + gid + "&uid=" + uid);
		parse_response(response);
		try {
			JSONObject json = new JSONObject(response);
			return (json.getString("result").equals("ok"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static boolean update_group_image(String where, String url) {
		// TODO Auto-generated method stub
		String response = HttpUtils.http_get(Constants.API_EDIT_PROFILE + "?where=" + where + "&avatar=" + URLEncoder.encode(url));
		parse_response(response);
		try {
			JSONObject json = new JSONObject(response);
			return (json.getString("result").equals("ok"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static boolean update_group_status(String userid, String text) {
		// TODO Auto-generated method stub
		String response = HttpUtils.http_get(Constants.API_EDIT_PROFILE + "?where=" + userid + "&status=" + URLEncoder.encode(text));
		parse_response(response);
		try {
			JSONObject json = new JSONObject(response);
			return (json.getString("result").equals("ok"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public static boolean update_profile_status(String text) {
		// TODO Auto-generated method stub
		String response = HttpUtils.http_get(Constants.API_EDIT_PROFILE + "?status=" + URLEncoder.encode(text));
		parse_response(response);
		try {
			JSONObject json = new JSONObject(response);
			return (json.getString("result").equals("ok"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public static boolean update_profile_name(String text) {
		// TODO Auto-generated method stub
		String response = HttpUtils.http_get(Constants.API_EDIT_PROFILE + "?name=" + URLEncoder.encode(text));
		parse_response(response);
		try {
			JSONObject json = new JSONObject(response);
			return (json.getString("result").equals("ok"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static SMSResponse request_sms(String cc, String phone) {
		try {
			JSONObject json = new JSONObject();
			json.put("cc", cc);
			json.put("phone", phone);
			String response = HttpUtils.http_json(Constants.API_SMS + "?send", json.toString());
			parse_response(response);
			if (response != null)
				return SMSResponse.parse(new JSONObject(response));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static String verify_sms(String code, JSONObject extras) {
		try {
			JSONObject json = new JSONObject();
			json.put("code", code);
			json.put("extras", extras);
			String response = HttpUtils.http_json(Constants.API_SMS + "?validate", json.toString());
			parse_response(response);
			return response;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	public static UpdateInfo get_update() {
		try {

			String response = HttpUtils.http_get(Constants.UPDATE_FILE);
			if(response == null) return null;
			return UpdateInfo.build(new JSONObject(response));
		} catch (JSONException ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
