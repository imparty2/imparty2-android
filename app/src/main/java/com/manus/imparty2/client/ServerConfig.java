package com.manus.imparty2.client;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;

public class ServerConfig {
	
	public static final String SERVER_PREFERENCES = "server_prefs";

	public static String getConfig(Context context, String key, String def)
	{
		SharedPreferences prefs = context.getSharedPreferences(SERVER_PREFERENCES, 0);
		
		return prefs.getString(key.toLowerCase(), def);
	}
	
	public static String getString(Context context, String key)
	{
		return getConfig(context, key, "");
	}
	
	public static int getInt(Context context, String key)
	{
		return Integer.parseInt(getConfig(context, key, "0"));
	}
	
	public static void save(Context context, JSONArray response)
	{
		SharedPreferences prefs = context.getSharedPreferences(SERVER_PREFERENCES, 0);
		
		for(int i = 0; i < response.length(); i++)
		{
			try {
				JSONObject obj = response.getJSONObject(i);
				String key = obj.getString("name");
				String value = obj.getString("value");
				prefs.edit().putString(key, value).commit();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
