package com.manus.imparty2.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.CookieStore;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import com.manus.imparty2.BuildConfig;
import com.manus.imparty2.client.HttpUtils.CustomMultiPartEntity.ProgressListener;

import android.os.AsyncTask;
import android.util.Log;

public class HttpUtils {
	
	private static List<String> currentDownloads = new ArrayList<String>();
	
	public interface HttpProgressListenner {
		void onProgress(int progress);
		void onError(String message);
		void onComplete(String response);
	}

	public static class CustomPairs extends ArrayList<BasicNameValuePair> {

		public void add(String key, String value) {
			remove(key);
			this.add(new BasicNameValuePair(key, value));
		}
		public String get_value(int index) {
			return super.get(indexOf(index)).getValue();
		}
		public void remove(String key) {
			int index = indexOf(key);
			if (index > -1) {
				this.remove(index);
			}
		}

		public int indexOf(String key) {
			for (int i = 0; i < this.size(); i++) {
				BasicNameValuePair array_element = this.get(i);

				if (array_element.getName().equals(key))
					return i;
			}
			return -1;
		}
	}
	
	public static class MySSLSocketFactory extends SSLSocketFactory {
		SSLContext sslContext = SSLContext.getInstance("TLS");

		public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
			super(truststore);

			TrustManager tm = new X509TrustManager() {
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			};

			sslContext.init(null, new TrustManager[]{tm}, null);
		}

		@Override
		public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
			return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
		}

		@Override
		public Socket createSocket() throws IOException {
			return sslContext.getSocketFactory().createSocket();
		}
	}
	public static class CustomMultiPartEntity extends MultipartEntity {

		private final ProgressListener listener;

		public CustomMultiPartEntity(final ProgressListener listener) {
			super();
			this.listener = listener;
		}

		public CustomMultiPartEntity(final HttpMultipartMode mode, final ProgressListener listener) {
			super(mode);
			this.listener = listener;
		}

		public CustomMultiPartEntity(HttpMultipartMode mode, final String boundary, final Charset charset, final ProgressListener listener) {
			super(mode, boundary, charset);
			this.listener = listener;
		}

		@Override
		public void writeTo(final OutputStream outstream) throws IOException {
			super.writeTo(new CountingOutputStream(outstream, this.listener));
		}

		public static interface ProgressListener {
			void transferred(long num);
		}

		public static class CountingOutputStream extends FilterOutputStream {

			private final ProgressListener listener;
			private long transferred;

			public CountingOutputStream(final OutputStream out, final ProgressListener listener) {
				super(out);
				this.listener = listener;
				this.transferred = 0;
			}

			public void write(byte[] b, int off, int len) throws IOException {
				out.write(b, off, len);
				this.transferred += len;
				this.listener.transferred(this.transferred);
			}

			public void write(int b) throws IOException {
				out.write(b);
				this.transferred++;
				this.listener.transferred(this.transferred);
			}
		}
	}
	private static HttpClient client;
	private static HttpContext context;

	private static HttpClient createHttpClient() {
		// sets up parameters
		if (client == null) {
			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, "utf-8"); //$NON-NLS-1$

			params.setBooleanParameter("http.protocol.expect-continue", false); //$NON-NLS-1$
			// registers schemes for both http and https
			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80)); //$NON-NLS-1$
			KeyStore trustStore;
			try {
				trustStore = KeyStore.getInstance(KeyStore.getDefaultType());

				trustStore.load(null, null);
				SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
				sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

				registry.register(new Scheme("https", sf, 443)); //$NON-NLS-1$
				ThreadSafeClientConnManager manager = new ThreadSafeClientConnManager(params, registry);
				// Create a local instance of cookie store
				BasicCookieStore cookieStore = new BasicCookieStore();

				// Create local HTTP context
				context = new BasicHttpContext();
				// Bind custom cookie store to the local context
				context.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

				client = new DefaultHttpClient(manager, params);
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (KeyManagementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnrecoverableKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return client;
		// mHttpClient.getCookieStore().addCookie(new
		// BasicClientCookie("PHPSESSID", ResponseLoader.getSID()));

	}

	public static HttpResponse http_get_response(String query, Header[] headers) {
		try {
			HttpClient mHttpClient = createHttpClient();

			HttpGet get = new HttpGet(query);
			HttpResponse response = mHttpClient.execute(get, context);
			return response;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static String http_get(String query, Header[] headers) {

		HttpResponse response = http_get_response(query, headers);
		if (response != null) {

			try {
				String str = EntityUtils.toString(response.getEntity());
				if (BuildConfig.DEBUG) {
					Log.d("HTTP", query + "\n------ response\n" + str);
				}
				return str;
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public static String http_get(String query) {
		return http_get(query, null);
	}

	public static String http_post(String url, List<BasicNameValuePair> data) {
		HttpResponse response = http_post_response(url, data);
		if (response != null) {

			try {
				return EntityUtils.toString(response.getEntity());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public static HttpResponse http_post_response(String url, List<BasicNameValuePair> data) {
		HttpClient httpclient = createHttpClient();
		HttpPost post = new HttpPost(url);
		try {
			post.setEntity(new UrlEncodedFormEntity(data, HTTP.UTF_8));
			return httpclient.execute(post);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static boolean http_download(String surl, String file) {
		// TODO Auto-generated method stub

		try {
			InputStream input = null;
			OutputStream output = null;
			try {

				HttpClient httpclient = createHttpClient();
				HttpGet get = new HttpGet(surl);
				HttpResponse response = httpclient.execute(get, context);

				input = response.getEntity().getContent();
				output = new FileOutputStream(file);
				byte data[] = new byte[4096];
				int count;
				while ((count = input.read(data)) != -1) {
					// allow canceling with back button
					output.write(data, 0, count);
				}
				return true;
			} catch (Exception e) {
				// e.printStackTrace();
				return false;
			} finally {
				try {
					if (output != null)
						output.close();
					if (input != null)
						input.close();
				} catch (IOException ignored) {
				}
			}
		} finally {

		}
	}

	public static void http_download(final String uri, final String file, final HttpProgressListenner listenner) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				currentDownloads.add(uri);
				// TODO Auto-generated method stub

				InputStream input = null;
				OutputStream output = null;
				HttpURLConnection connection = null;
				try {
					URL url = new URL(uri);
					connection = (HttpURLConnection) url.openConnection();
					connection.connect();

					// expect HTTP 200 OK, so we don't mistakenly save error
					// report
					// instead of the file
					if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
						listenner.onError("Server returned HTTP " + connection.getResponseCode() + " " + connection.getResponseMessage());

					// this will be useful to display download percentage
					// might be -1: server did not report the length
					int fileLength = connection.getContentLength();

					// download the file
					input = connection.getInputStream();
					output = new FileOutputStream(file);

					byte data[] = new byte[4096];
					long total = 0;
					int count;
					while ((count = input.read(data)) != -1) {
						// allow canceling with back button

						total += count;
						// publishing the progress....
						if (fileLength > 0) // only if total length is known
							listenner.onProgress((int) (total * 100 / fileLength));
						output.write(data, 0, count);
						if(!isDownload(uri)) break;
					}
					listenner.onComplete("");
				} catch (Exception e) {
					listenner.onError(e.toString());
				} finally {
					try {
						if (output != null)
							output.close();
						if (input != null)
							input.close();
					} catch (IOException ignored) {
					}

					if (connection != null)
						connection.disconnect();

					currentDownloads.remove(uri);
				}
			}
		}).start();

	}
	
	public static boolean isDownload(String uri)
	{
		for (String url : currentDownloads) {
			if(url.equals(uri)) return true;
		}
		return false;
	}
	
	public static boolean http_cancel_upload(String url)
	{
		for(int i = 0; i < currentDownloads.size(); i++)
		{
			if(currentDownloads.get(i).equals(url))
			{
				currentDownloads.remove(i);
				return true;
			}
		}
		return false;
	}
	public static void http_upload(final String file, final String url, final HttpProgressListenner listenner) {
		new HttpUpload(url, listenner).execute(file);
	}

	public static class HttpUpload extends AsyncTask<String, Integer, String> {

		HttpProgressListenner listenner;
		String url;
		public HttpUpload(String url, HttpProgressListenner listenner) {
			// TODO Auto-generated constructor stub
			this.listenner = listenner;
			this.url = url;
		}
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String file = params[0];
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext httpContext = new BasicHttpContext();
			HttpPost httpPost = new HttpPost(url);

			try {
				final File mFile = new File(file);
				final long totalSize = mFile.length();
				CustomMultiPartEntity multipartContent = new CustomMultiPartEntity(new ProgressListener() {
					long last;
					@Override
					public void transferred(long num) {
						if (System.currentTimeMillis() - last > 1500 || last == 0) {
							publishProgress((int) ((float) (num) / (float) (totalSize) * 100));
							last = System.currentTimeMillis();
						}
					}
				});

				// We use FileBody to transfer an image
				multipartContent.addPart("file", new FileBody(mFile));

				// Send it
				httpPost.setEntity(multipartContent);
				HttpResponse response = httpClient.execute(httpPost, httpContext);
				String serverResponse = EntityUtils.toString(response.getEntity());

				return serverResponse;
			}

			catch (Exception e) {
				System.out.println(e);

			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			listenner.onProgress(values[0]);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if (result != null)
				listenner.onComplete(result);
			else
				listenner.onError("");
			super.onPostExecute(result);
		}

	}

	public static String http_json(String url, String json) {
		try {
			HttpClient httpclient = createHttpClient();
			// url with the post data
			HttpPost httpost = new HttpPost(url);

			// convert parameters into JSON object
			// passes the results to a string builder/entity
			StringEntity se = new StringEntity(json);
			se.setContentEncoding("UTF-8");
			se.setContentType("application/json");

			// sets the post request as the resulting string
			httpost.setEntity(se);
			// sets a request header so the page receving the request
			// will know what to do with it
			httpost.setHeader("Accept", "application/json");
			httpost.setHeader("Content-type", "application/json");

			// Handles what is returned from the page
			HttpResponse response;
			response = httpclient.execute(httpost, context);
			String str = EntityUtils.toString(response.getEntity());
			if (BuildConfig.DEBUG) {
				Log.d("HTTP", url + "\n" + json + "\n------ response\n" + str);
			}
			return str;
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
