package com.manus.imparty2.client;

import com.manus.imparty2.ConfigManager;
import com.manus.imparty2.push.PushHelper;
import com.manus.imparty2.push.PushMessages;
import com.manus.imparty2.service.MessagingService;
import com.manus.imparty2.service.UpdatesManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

public class NetworkChangeReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		UpdatesManager manager = new UpdatesManager(context);
		manager.check_update();
		if (!intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
            if(MessagingService.getInstance() != null)
            {
            	new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
		            	MessagingService.getInstance().send_messages_wrote();
						
		            	
					}
				}).start();
            }
        }
	}

}
