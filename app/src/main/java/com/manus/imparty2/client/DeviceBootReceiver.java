package com.manus.imparty2.client;

import com.manus.imparty2.ConfigManager;
import com.manus.imparty2.service.MessagingService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class DeviceBootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if(ConfigManager.hasLogin(context))
		{
			context.startService(new Intent(context, MessagingService.class));
		}
	}

}
