package com.manus.imparty2.client;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;

public class Presence {

	public String avatar;
	public String name;
	public String presence;
	public String status;
	public long lastseen;
	
	public Bundle build()
	{
		Bundle b = new Bundle();
		b.putString("avatar", avatar);
		b.putString("name", name);
		b.putString("presence", presence);
		b.putString("status", status);
		b.putString("lastseen", String.valueOf( lastseen / 1000 ));
		
		return b;
	}
	
	public static Presence parse(Bundle b)
	{
		Presence _p = new Presence();
		_p.avatar = b.getString("avatar");
		_p.name = b.getString("name");
		_p.presence = b.getString("presence");
		_p.status = b.getString("status");
		_p.lastseen = Long.parseLong( b.getString("lastseen") ) * 1000;
		
		return _p;
	}
	public static Presence parse(JSONObject json) throws JSONException
	{
		Presence _presence = new Presence();
		_presence.avatar = json.getString("avatar");
		_presence.presence = json.getString("presence");
		_presence.name = json.getString("name");
		_presence.lastseen = json.getLong("lastseen");
		_presence.status = json.getString("status");
		return _presence;
	}
}
