package com.manus.imparty2.client;

import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

public class UpdateInfo {

	public HashMap<String, String> current = new HashMap<String, String>();
	public JSONObject versions;
	
	public static UpdateInfo build(JSONObject obj)
	{
		UpdateInfo info = new UpdateInfo();
		JSONObject curr;
		try {
			curr = obj.getJSONObject("current");
			for (Iterator iterator = curr.keys(); iterator.hasNext();) {
				String type = (String) iterator.next();
				info.current.put(type, curr.getString(type));
			}
			
			info.versions = obj.getJSONObject("versions");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return info;
		
		
	}
}
