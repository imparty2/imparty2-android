package com.manus.imparty2.client;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;

public class NetworkInfoHelper {

	enum Type
	{
		WiFi,
		Movile,
		Unknown
	} 
	public static boolean isWifiConnection(Context context)
	{
		return getConnectionType(context) == Type.WiFi;
	}
	
	public static Type getConnectionType(Context context)
	{
		ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if(conMan == null) return Type.Unknown;
		if(conMan.getNetworkInfo(0) == null) return Type.Unknown;
		if(conMan.getNetworkInfo(1) == null) return Type.Unknown;
		State mobile = conMan.getNetworkInfo(0).getState();
		State wifi = conMan.getNetworkInfo(1).getState();
		
		if (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) {
		    return Type.Movile;
		} else if (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING) {
		    return Type.WiFi;
		}
		return Type.Unknown;
	}
}
