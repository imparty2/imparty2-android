package com.manus.imparty2.client;

import org.json.JSONException;
import org.json.JSONObject;

public class ManushareUploadProvider {

	public static String get_download_url(String response)
	{
		try {
			JSONObject obj = new JSONObject(response);
			return obj.getString("url");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
