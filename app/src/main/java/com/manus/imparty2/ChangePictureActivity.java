package com.manus.imparty2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import com.manus.imparty2.client.API;
import com.manus.imparty2.client.HttpUtils;
import com.manus.imparty2.client.ManushareUploadProvider;
import com.manus.imparty2.client.ServerConfig;
import com.manus.imparty2.client.HttpUtils.HttpProgressListenner;
import com.manus.imparty2.core.MediaProvider;
import com.manus.imparty2.core.Photos;
import com.manus.imparty2.service.MessagingService;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.view.Menu;
import android.widget.Toast;

public class ChangePictureActivity extends Activity {

	private static final int ACTIVITY_REQUEST_PICK_PHOTO = 1;
	private static final int ACTIVITY_REQUEST_CROP_PHOTO = 2;
	File f;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(android.R.style.Theme_Dialog);
		super.onCreate(savedInstanceState);
		
		f = MediaProvider.get_image_file();
		Intent pickIntent = new Intent();
		pickIntent.setType("image/*");
		pickIntent.putExtra("crop", "true");
		pickIntent.putExtra("aspectX", 1);
		pickIntent.putExtra("aspectY", 1);
		pickIntent.putExtra("outputX", 600);
		pickIntent.putExtra("outputY", 600);
		pickIntent.putExtra("output", Uri.fromFile(f));
		
		pickIntent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(pickIntent, ACTIVITY_REQUEST_PICK_PHOTO);
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if(resultCode == RESULT_OK)
		{
			
			if(requestCode == ACTIVITY_REQUEST_PICK_PHOTO)
			{
				Uri _uri = data.getData();
				Bitmap bmp;
				//try {
					//bmp = Photos.getCorrectlyOrientedImage(this, _uri);
	
					
					//FileOutputStream out = new FileOutputStream(f);
					//bmp.compress(CompressFormat.JPEG, 100, out);
					//out.close();
					final ProgressDialog d = new ProgressDialog(this);
					d.setTitle("Cambiando...");
					d.setIndeterminate(true);
					d.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
					d.show();
					String md5 = Utils.md5_file(f);
					HttpUtils.http_upload(f.getAbsolutePath(), ServerConfig.getConfig(this, "media_provider", "") + "?md5=" + md5, new HttpProgressListenner() {
						
						@Override
						public void onProgress(int progress) {
							// TODO Auto-generated method stub
							d.setIndeterminate(false);
							d.setProgress(progress);
						}
						
						@Override
						public void onError(String message) {
							// TODO Auto-generated method stub
							Toast.makeText(ChangePictureActivity.this, "Error", 0).show();
							d.cancel();
							finish();
						}
						
						@Override
						public void onComplete(String response) {
							// TODO Auto-generated method stub
							final String url = ManushareUploadProvider.get_download_url(response);
							if(url != null)
							{
								d.setIndeterminate(true);
								d.setProgressStyle(ProgressDialog.STYLE_SPINNER);
								
								new Thread(new Runnable() {
									
									@Override
									public void run() {
										// TODO Auto-generated method stub
										
										if(getIntent().hasExtra("where"))
										{
											API.update_group_image(getIntent().getStringExtra("where"), url);
										}
										else
										{
											API.set_profile_image(url);
										}
										
										runOnUiThread(new Runnable() {
											
											@Override
											public void run() {
												// TODO Auto-generated method stub
												d.cancel();
												finish();
											}
										});
									}
								}).start();
							}
							else
							{
								Toast.makeText(ChangePictureActivity.this, "Error", 0).show();
								
								finish();
								d.cancel();
							}
						}
					});
					
					//MessagingService.getInstance().send_image(username, f.getAbsolutePath());
				//} catch (IOException e) {
					// TODO Auto-generated catch block
				//	e.printStackTrace();
				//	Toast.makeText(this, "No se ha podido preparar la foto", 0).show();
				//}
			}
			else if(requestCode == ACTIVITY_REQUEST_CROP_PHOTO)
			{
				
				
			}

		}
		else
		{
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.change_picture, menu);
		return true;
	}

}
