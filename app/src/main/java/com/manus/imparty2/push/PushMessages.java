package com.manus.imparty2.push;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.manus.imparty2.ConfigManager;
import com.manus.imparty2.Constants;
import com.manus.imparty2.ContactListActivity;
import com.manus.imparty2.R;
import com.manus.imparty2.Utils;
import com.manus.imparty2.client.HttpUtils;
import com.manus.imparty2.client.HttpUtils.HttpProgressListenner;
import com.manus.imparty2.client.NetworkInfoHelper;
import com.manus.imparty2.core.Chat;
import com.manus.imparty2.service.UpdatesManager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class PushMessages {
	private static final String SWITCH_EVENT = "com.manus.imparty2.DOWNLOAD";

	public interface MessageReceiver
	{
		void onMessage(Bundle data);
	}
	
	

    private static final List<MessageReceiver> handlers = new ArrayList<MessageReceiver>();
    
    public static void registerReceiver(MessageReceiver handler)
    {
    	handlers.add(handler);
    }
    
    public static void removeReceiver(MessageReceiver handler)
    {
    	handlers.remove(handler);
    }
    
    public static void notify(Context context, Bundle obj)
    {
    	if(obj.getString("type").equals("update"))
    	{
    		do_update(context, obj.getString("url"), obj.getString("version"));
    	}
    	for (MessageReceiver handler : handlers) {
    		try
    		{
    			if(handler != null)
    				handler.onMessage(obj);
    		}
    		catch(Exception ex)
    		{
    			ex.printStackTrace();
    		}
		}
    	
    }
    
    public static void do_update(Context context, String url, String version)
    {
    	UpdatesManager manager = new UpdatesManager(context);
    	manager.check_update();
    }
    

}
