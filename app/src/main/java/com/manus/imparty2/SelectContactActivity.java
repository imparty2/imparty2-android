package com.manus.imparty2;

import com.manus.imparty2.adapter.ContactAdapter;
import com.manus.imparty2.adapter.RecentsAdapter;
import com.manus.imparty2.core.Recent;
import com.manus.imparty2.service.MessagingService;
import com.manus.imparty2.service.MessagingService.MessageReceiver;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class SelectContactActivity extends Activity implements MessageReceiver, OnItemClickListener {
	RecentsAdapter adapter;
	ListView list;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_contact);
		
		list = (ListView) findViewById(R.id.listView1);
		MessagingService.attachListenner(this);
		list.setOnItemClickListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.select_contact, menu);
		return true;
	}

	@Override
	public boolean onMessage(Bundle data) {
		// TODO Auto-generated method stub
		if(data.getInt("key") == MessagingService.BUNDLE_KEY_SERVICE_BOOT)
		{
			adapter = new RecentsAdapter(this, R.layout.item_recent, MessagingService.getInstance().getRecents());
			list.setAdapter(adapter);
		}
		
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		Recent r = adapter.getItem(arg2);
		
		Intent i = Utils.build_messages_activity(this, r.contact.uid);
		i.setAction(getIntent().getAction());
		i.setType(getIntent().getType());
		i.putExtras(getIntent());
		startActivity(i);
		finish();
		
	}

}
