package com.manus.imparty2;

import android.content.Context;
import android.support.v4.app.FragmentActivity;

public class ConfigManager {
	public static final String PREFERENCE_NAME = "imparty";
	public static final String EMAIL_KEY = "email";
	public static final String TOKEN_KEY = "token";
	public static boolean hasLogin(Context self)
	{
		String email = self.getSharedPreferences(PREFERENCE_NAME, 0).getString(EMAIL_KEY, "");
		String token = self.getSharedPreferences(PREFERENCE_NAME, 0).getString(TOKEN_KEY, "");
		
		return (!email.equals("") && !token.equals(""));
	}

	public static String getEmail(Context self)
	{
		return self.getSharedPreferences(PREFERENCE_NAME, 0).getString(EMAIL_KEY, "");
	}
	public static String getToken(Context self)
	{
		return self.getSharedPreferences(PREFERENCE_NAME, 0).getString(TOKEN_KEY, "");
	}
	
	public static void saveLogin(Context self, String email, String token)
	{
		self.getSharedPreferences(PREFERENCE_NAME, 0).edit()
		.putString(EMAIL_KEY, email)
		.putString(TOKEN_KEY, token)
		.commit();
	}

	public static void clearLogin(Context self) {
		// TODO Auto-generated method stub
		self.getSharedPreferences(PREFERENCE_NAME, 0).edit()
		.remove(EMAIL_KEY)
		.remove(TOKEN_KEY)
		.commit();
	}

	public static boolean isDownloadScheduler(Context self)
	{
		return self.getSharedPreferences(PREFERENCE_NAME, 0).getBoolean("download", false);
	}
	public static void setDownloadScheduler(Context self, boolean status)
	{
		self.getSharedPreferences(PREFERENCE_NAME, 0).edit()
		.putBoolean("download", status)
		.commit();
	}
	public static void setDownloadScheduler(Context self, boolean status, String url, String version)
	{
		self.getSharedPreferences(PREFERENCE_NAME, 0).edit()
		.putBoolean("download", status)
		.putString("url", url)
		.putString("version", version)
		.commit();
	}

	public static String getDownloadSchedulerUrl(Context self)
	{
		return self.getSharedPreferences(PREFERENCE_NAME, 0).getString("url", "");	
	}
	public static String getDownloadSchedulerVersion(Context self)
	{
		return self.getSharedPreferences(PREFERENCE_NAME, 0).getString("version", "");	
	}

	public static void silentUser(Context context, String username, long expire) {
		// TODO Auto-generated method stub
		
		context.getSharedPreferences(PREFERENCE_NAME, 0).edit()
		.putLong("block_" + username, expire)
		.commit();
	}
	
	public static boolean isSilentUser(Context self, String username)
	{
		long expires = self.getSharedPreferences(PREFERENCE_NAME, 0).getLong("block_"+username, 0);
		if(expires == 0) return false;
		return System.currentTimeMillis() < expires;
	}
}
